import sys
import os
import subprocess
import string
import datetime
import copy
import tarfile
import urllib
import shutil
from StringIO import StringIO

global EAS_HOME
global DJANGO_SETTINGS_DEPLOY_PATH
global EAS_COMMIT
global CONFIG_COMMIT
global BITBUCKET_USER
global BITBUCKET_PASSWORD

global settings

def usage():
    usage = ''
    usage += 'usage: %s <eas_home> <django_settings_deploy_path> <eas_commit> <config_commit> <bitbucket_user> <bitbucket_password>\n' % sys.argv[0]
    usage += 'description: Deploys the application to the web server.\n'
    print usage


def getEnvironment():
    global settings
    # Get the IP address of the host on which we are running.
    import socket
    hostIpAddress = socket.gethostbyname(socket.gethostname())
    #print 'host ip address detected for this machine is %s' % hostIpAddress

    # look for the hostIpAddress in the dictionary
    environment = settings.environments.getEnvironmentByIp(hostIpAddress)
    if environment is None:
        raise Exception('environment not found for %s' % hostIpAddress)
    #print 'using env: %s' % environment
    return environment


def init():

    global EAS_HOME
    global DJANGO_SETTINGS_DEPLOY_PATH
    global EAS_COMMIT
    global CONFIG_COMMIT
    global BITBUCKET_USER
    global BITBUCKET_PASSWORD

    if len(sys.argv) != 7:
        print 'need 7 input args - got %s' % str(len(sys.argv))
        usage()
        sys.exit(1)

    if not os.name == 'posix':
        print 'this script is for linux only'
        sys.exit(1)

    EAS_HOME = sys.argv[1]
    DJANGO_SETTINGS_DEPLOY_PATH = sys.argv[2]
    EAS_COMMIT = sys.argv[3]
    CONFIG_COMMIT = sys.argv[4]
    BITBUCKET_USER = sys.argv[5]
    BITBUCKET_PASSWORD = sys.argv[6]

    sys.path.append(EAS_HOME)


def initDjangoSettings():
    global settings
    from django.core.management import setup_environ
    import settings
    setup_environ(settings)


def getVersionControlSets(configRepoParms, easRepoParms, buildId):
    return \
        {
        'THIRDPARTY': {
                'PATH_MAPPINGS': [
                    ('extjs/extjs-4.1.0/ext-all-debug.js',                  'Media/ui_frameworks/extjs-4.1.0/ext-all-debug.js'),
                    ('extjs/extjs-4.1.0/ext-all.js',                        'Media/ui_frameworks/extjs-4.1.0/ext-all.js'),
                    ('extjs/extjs-4.1.0/resources/themes/images',           'Media/ui_frameworks/extjs-4.1.0/resources/themes/images'),
                    ('extjs/extjs-4.1.0/resources/css/ext-all.css',         'Media/ui_frameworks/extjs-4.1.0/resources/css/ext-all.css'),
                    ('extjs/extjs-4.1.0/examples/ux/CheckColumn.js',        'Media/ui_frameworks/extjs-4.1.0/examples/ux/CheckColumn.js'),
                    ('extjs/extjs-4.1.0/examples/ux/css/CheckHeader.css',   'Media/ui_frameworks/extjs-4.1.0/examples/ux/css/CheckHeader.css'),
                    ('extjs/extjs-4.1.0/examples/ux/grid',                  'Media/ui_frameworks/extjs-4.1.0/examples/ux/grid'),
                    ('OpenLayers/OpenLayers-2.12/theme',                    'Media/ui_frameworks/OpenLayers-2.12/theme'),
                    ('OpenLayers/OpenLayers-2.12/OpenLayers.js',            'Media/ui_frameworks/OpenLayers-2.12/OpenLayers.js'),
                    ('OpenLayers/OpenLayers-2.12/OpenLayers.debug.js',      'Media/ui_frameworks/OpenLayers-2.12/OpenLayers.debug.js'),
                    ('OpenLayers/OpenLayers-2.12/art',                      'Media/ui_frameworks/OpenLayers-2.12/art'),
                    ('OpenLayers/OpenLayers-2.12/img',                      'Media/ui_frameworks/OpenLayers-2.12/img'),
                ],
                'TEAM': 'sfgovdt',
                'PROJECT': 'thirdpartyx',
                'COMMIT': 'master',
                'USER': configRepoParms['USER'],
                'PASSWORD': configRepoParms['PASSWORD'],
                'COMMIT_KEY': 'CONFIG_COMMIT'
        },
        'CONFIG': {
                'PATH_MAPPINGS': [
                    ('web/settings_env/', 'settings_env/')
                ],
                'TEAM': configRepoParms['TEAM'],
                'PROJECT': configRepoParms['PROJECT'],
                'COMMIT': configRepoParms['COMMIT'],
                'USER': configRepoParms['USER'],
                'PASSWORD': configRepoParms['PASSWORD'],
                'COMMIT_KEY': 'CONFIG_COMMIT'
        },
        'EAS': {
                'PATH_MAPPINGS': [
                    ('web',                                     ''),
                    ('Media/js',                                'Media/js'),
                    ('Media/build_scripts.txt',                 'Media/build_scripts.txt'),
                    ('Media/ui_frameworks/FGI',                 'Media/ui_frameworks/FGI'),
                    ('Media/css',                               'Media/css'),
                    ('Media/css/MAD.css',                       'Media/css/MAD_'+buildId+'.css'),
                    ('Media/images',                            'Media/images')
                ],
                'TEAM': easRepoParms['TEAM'],
                'PROJECT': easRepoParms['PROJECT'],
                'COMMIT': easRepoParms['COMMIT'],
                'USER': easRepoParms['USER'],
                'PASSWORD': easRepoParms['PASSWORD'],
                'COMMIT_KEY': 'EAS_COMMIT'
            }
        }


def copyFileAndOverWrite(sourceFile, targetFile):
    if os.path.exists(targetFile):         
        os.remove(targetFile)
    targetDir = os.path.dirname(targetFile)
    if not os.path.exists(targetDir):
        os.makedirs(targetDir)
    shutil.copy(sourceFile,targetFile)


def hgExportSet(versionControlSet):
    print 'getting files from bitbucket'

    global EAS_HOME

    pathMappings = versionControlSet['PATH_MAPPINGS']
    url = 'https://%s:%s@bitbucket.org/%s/%s/get/%s.tar.gz' % (versionControlSet['USER'], versionControlSet['PASSWORD'], versionControlSet['TEAM'], versionControlSet['PROJECT'], versionControlSet['COMMIT'])
    tempFolder = os.path.join('/var','tmp')
    # tfile = tarfile.open(mode="r:gz",fileobj=StringIO(urllib.urlopen(url).read()))
    # tfile.extractall(tempFolder)
    # tfile.close()
    tempSrcFolder = None
    for dirname in os.listdir(tempFolder):
        expectedDirPrefix = '%s-%s' % (versionControlSet['TEAM'], versionControlSet['PROJECT'])
        if expectedDirPrefix in dirname:
            tempSrcFolder = os.path.join(tempFolder, dirname)
    if tempSrcFolder is None:
        raise Exception('there was a problem locating the downloaded repo archive')
    else:
        for pathMapping in pathMappings:
            sourcePath = os.path.join(tempSrcFolder, pathMapping[0])
            targetPath = os.path.join(EAS_HOME, pathMapping[1])
            if os.path.isfile(sourcePath):
                copyFileAndOverWrite(sourcePath, targetPath)                
            else:
                for item in os.listdir(sourcePath):
                    path = os.path.join(sourcePath,item)
                    if os.path.isfile(path):
                        copyFileAndOverWrite(path, os.path.join(targetPath,item))
                    else:
                        if os.path.exists(os.path.join(targetPath, item)):
                            shutil.rmtree(os.path.join(targetPath, item))
                        shutil.copytree(path, os.path.join(targetPath, item))

        # shutil.rmtree(os.path.join(tempSrcFolder))


def configureHtconf():

    global EAS_HOME
    global settings

    htconf_path = settings.HTCONF_FILENAME

    if not htconf_path:
        raise Exception('we need a path to set the htconf configurations')

    if not os.path.exists(htconf_path):
        raise IOError("htconf path not found: %s " % htconf_path)

    htconf_path = os.path.basename(htconf_path)
    htconf_path = os.path.join(EAS_HOME, htconf_path)

    print 'updating %s...' % htconf_path

    # Now set the Geoserver values in the htconf file.
    s = None
    try:
        f = open(htconf_path, 'r')
        s = f.read()
    except Exception, e:
        print e
        raise
    finally:
        f.close()

    try:
        f = open(htconf_path, 'w')
        s = s.replace('0.0.0.0', settings.environment['GEO_HOST'])
        f.write(s)
    except Exception, e:
        print e
        raise
    finally:
        f.close()


def minifyJavascript(buildList=None, outPath=None, outFileNamePrefix=None):
    from MAD.utils import jsbuild
    print 'minifying javascript'
    jsbuild.build_scripts(buildList=buildList, outPath=outPath, outFileNamePrefix=outFileNamePrefix)


def adjustDjangoSettings(FILE_PATH=None, versionControlSets={}, buildId=0):
    print 'adjusting django deploy settings'

    EAS_KEY = versionControlSets['EAS']['COMMIT_KEY']
    EAS_COMMIT = versionControlSets['EAS']['COMMIT']
    CONFIG_KEY = versionControlSets['CONFIG']['COMMIT_KEY']
    CONFIG_COMMIT = versionControlSets['CONFIG']['COMMIT']

    foundConfigCommitLine = False
    foundEasCommitLine = False
    foundBuildLine = False
    input=None
    output=None

    try:        
        inputFilename = FILE_PATH
        outputFilename = FILE_PATH + '.temp'
        
        input=open(inputFilename, 'r')
        output=open(outputFilename, 'w')           
        
        def getRevisedLine(inputString, value):
            temp = inputString.split('=')            
            return temp[0] + '="' + value + '"\n'

        for line in input.readlines():
            fields = line.split('=')
            if fields:
                key = fields[0].strip()
                if key == CONFIG_KEY:
                    line = getRevisedLine(line, CONFIG_COMMIT)
                    foundConfigCommitLine = True
                elif key == EAS_KEY:
                    line = getRevisedLine(line, EAS_COMMIT)
                    foundEasCommitLine = True
                elif key == 'BUILD_ID':
                    line = getRevisedLine(line, buildId)
                    foundBuildLine = True

            #sys.stdout.write(line) # debug
            output.writelines(line) 

    except Exception, e:
        print e
        raise

    finally:
        if input:
            input.close()
        input.close()
        if output:
            output.close()

        if not foundConfigCommitLine:
            raise Exception('%s was not found in the input file %s' % ( CONFIG_KEY, FILE_PATH ))
        if not foundEasCommitLine:
            raise Exception('%s was not found in the input file %s' % ( EAS_KEY, FILE_PATH ))
        if not foundBuildLine:
            raise Exception('BUILD_ID was not found in the input file %s' % FILE_PATH)

    try:
        os.remove(inputFilename)    
        os.rename(outputFilename, inputFilename)
    except Exception, e:       
        print e
        raise
    

def main():

    global EAS_HOME
    global DJANGO_SETTINGS_DEPLOY_PATH
    global EAS_COMMIT
    global CONFIG_COMMIT
    global BITBUCKET_USER
    global BITBUCKET_PASSWORD

    print 'deploying eas source'

    init()

    # Default REVISION to None not "HEAD". This way the script will retrieve the rev numbers and make them visible in the UI.
    configRepoParms = {'USER': BITBUCKET_USER, 'PASSWORD': BITBUCKET_PASSWORD, 'COMMIT': CONFIG_COMMIT, 'TEAM': 'sfgovdt', 'PROJECT': 'easconfigxsf'}
    easRepoParms = {'USER': BITBUCKET_USER, 'PASSWORD': BITBUCKET_PASSWORD, 'COMMIT': EAS_COMMIT, 'TEAM': 'sfgovdt', 'PROJECT': 'easx'}

    # example: g-branches_1.1.2A-r4065__j-branches_1.1.2A-r3519
    # gc:
    #   path: branches/1.1.2A
    #   revision: 4065
    # j:
    #   path: branches/1.1.2A
    #   revision: 3519
    # d = {'g': {'path': 'branches/1.1.2A', 'revision': '1234'}, 'j': {'path': 'branches/1.1.2A', 'revision': '5678'}}
    #   revision: 4065}
    buildId = 'eas-%s__config-%s' % ( easRepoParms['COMMIT'], configRepoParms['COMMIT'])
    versionControlSets = getVersionControlSets(configRepoParms, easRepoParms, buildId)

    # Now that we have the build id, we can export the code from bitbucket
    for versionControlSet in versionControlSets.values():
        hgExportSet(versionControlSet)

    initDjangoSettings()
    print 'configuring eas using the following environment'
    print settings.environment
    configureHtconf()
    adjustDjangoSettings(DJANGO_SETTINGS_DEPLOY_PATH, versionControlSets, buildId=buildId)
    minifyJavascript(buildList=EAS_HOME+'/Media/build_scripts.txt', outPath=EAS_HOME+'/Media', outFileNamePrefix='eas_'+buildId)


if __name__ == "__main__":
    main()
