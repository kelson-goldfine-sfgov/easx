#!/bin/bash

usage() {
    echo "Usage: $0 <EAS_COMMIT> <CONFIG_COMMIT> <BITBUCKET_USER>"
    exit
}

if [[ ! $# == 3 ]]
then
   usage
fi

EAS_COMMIT=$1
CONFIG_COMMIT=$2
BITBUCKET_USER=$3
stty -echo
read -p "BITBUCKET password: " BITBUCKET_PASSWORD; echo
stty echo

# curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/sfgovdt/easx/raw/$EAS_COMMIT/web/bin/deploy_eas.sh
# curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/sfgovdt/easx/raw/$EAS_COMMIT/web/bin/deploy_eas_source.py
# curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/sfgovdt/easx/raw/$EAS_COMMIT/web/bin/set_eas_mode.sh
# curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/sfgovdt/easx/raw/$EAS_COMMIT/web/bin/set_eas_env.sh
chmod u+x ./deploy_eas.sh
./deploy_eas.sh $EAS_COMMIT $CONFIG_COMMIT $BITBUCKET_USER $BITBUCKET_PASSWORD
