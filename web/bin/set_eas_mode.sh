
usage() {
    echo "Usage: $0 <LIVE|MAINT|SHOW_ENV|STANDBY_SF|STANDBY_DR>"
    echo "Run from /var/www/html."
    echo "LIVE will put the host into live mode."
    echo "MAINT will put the host into maintenance mode."
    echo "SHOW_ENV configures the host to show the versions of the system libraries."
    echo "STANDBY_SF will put the host into a standby mode and will return a page with a link to the DR site."
    echo "STANDBY_DR will put the host into a standby mode and will return a page with a link to the SF site."
    exit 1
}


validate_parms() {

    if [ -z "$TARGET_MODE" ]
    then
        echo "TARGET_MODE parm was null"
        usage
    fi

    if [ -z "$LIVE_HOME" ]
    then
        echo "LIVE_HOME parm was null"
        usage
    fi

    if [ -z "$MAINT_HOME" ]
    then
        echo "MAINT_HOME parm was null"
        usage
    fi

    if [ -z "$SYM_LINK" ]
    then
        echo "SYM_LINK parm was null"
        usage
    fi

    if [ "$TARGET_MODE" != "MAINT" ] && [ "$TARGET_MODE" != "LIVE" ] && [ "$TARGET_MODE" != "STANDBY_SF" ] && [ "$TARGET_MODE" != "STANDBY_DR" ] && [ "$TARGET_MODE" != "SHOW_ENV" ];
    then
        usage
    fi

    if [ "$TARGET_MODE" == "LIVE" ];
    then
        if [ ! -d "$LIVE_HOME" ]
        then
            echo "It appears that $LIVE_HOME does not exist. $LIVE_HOME must exist to set EAS to LIVE mode."
            usage
        fi
    fi

}

restart_web_server() {
    echo restarting web server
    /etc/init.d/httpd restart
    if [ $? -ne "0" ] ; then
        echo "***" encountered error restarting server "***"
        exit 1
    fi
}

set_live() {
    echo setting application mode to LIVE
    cp $LIVE_HOME/mad.htconf.live $LIVE_HOME/mad.htconf
    rm -f $SYM_LINK
    ln -s $LIVE_HOME $SYM_LINK
}

set_show_env() {
    echo setting application mode to SHOW_ENV
    cp $LIVE_HOME/mad.htconf.env $LIVE_HOME/mad.htconf
    rm -f $SYM_LINK
    ln -s $LIVE_HOME $SYM_LINK
}

set_mode() {
    echo setting web application mode to $TARGET_MODE

    if [ $TARGET_MODE == "MAINT" ]
    then
        MODE_MESSAGE="Sorry! EAS is temporarily unavailable. We should be back up shortly."
    elif [ $TARGET_MODE == "STANDBY_SF" ]
    then
        MODE_MESSAGE="EAS is currently running from the City's Disaster Recovery Data Center. To access the site, click <a href="http://10.255.8.116/">here</a>."
    elif [ $TARGET_MODE == "STANDBY_DR" ]
    then
        MODE_MESSAGE="EAS is currently running from the San Francisco Data Center. To access the site, click <a href="http://10.250.60.195/">here</a>."
    fi

    rm -rf $MAINT_HOME
    cp -R $LIVE_HOME $MAINT_HOME
    echo $MODE_MESSAGE > $MAINT_HOME/$MESSAGE_FILE
    cp $MAINT_HOME/mad.htconf.maint $MAINT_HOME/mad.htconf
    rm -f $SYM_LINK
    ln -s $MAINT_HOME $SYM_LINK
}


# MAIN
. ./set_eas_env.sh
LIVE_HOME=$EAS_HOME_LIVE
MAINT_HOME=$EAS_HOME_MAINT
SYM_LINK=$EAS_HOME_SYM_LINK
TARGET_MODE=$1
MESSAGE_FILE="mode_message.html"
MODE_MESSAGE=""

validate_parms

echo "running $0"
echo "LIVE_HOME:   $LIVE_HOME"
echo "MAINT_HOME:  $MAINT_HOME"
echo "TARGET_MODE: $TARGET_MODE"
echo "SYM_LINK:    $SYM_LINK"

if [ "$TARGET_MODE" == "LIVE" ];
then
    set_live
elif [ "$TARGET_MODE" == "SHOW_ENV" ];
then
    set_show_env
else
    set_mode
fi
restart_web_server
