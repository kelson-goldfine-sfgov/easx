
import random
import smtplib
from email.mime.text import MIMEText
from email.MIMEBase import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import Encoders

def generatePassword(choice, length):

   password = ""

   alphanum = ('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
   alpha = ('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
   alphacap = ('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
   all = ('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-_+=~`[]{}|\:;"\'<>,.?/')

   if str(choice).lower() == "alphanum":
      choice = alphanum

   elif str(choice).lower() == "alpha":
      choice = alpha

   elif str(choice).lower() == "alphacap":
      choice = alphacap

   elif str(choice).lower() == "all":
      choice = all

   else:
      raise Exception('unsupported password type\n')

   return password.join(random.sample(choice, int(length)))


def sendEmail(subject, text, attachFiles=[], recipientList=[]):
    if not recipientList:
        raise Exception('recipient list may not be empty')
    msg = MIMEMultipart()
    msg['Subject'] = subject
    msg.attach( MIMEText(text) )
    for attachFile in attachFiles:
        part = MIMEBase('application', "octet-stream")
        part.set_payload( open(attachFile,"rb").read() )
        Encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(attachFile))
        msg.attach(part)
    #smtpConnection = smtplib.SMTP('ompmta01.sfgov.org')
    smtpConnection = smtplib.SMTP('mercury.sfgov.org')
    smtpConnection.sendmail('noreply.sfgov.org', recipientList, msg.as_string())
    smtpConnection.quit()
