class AbstractException(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

class AddressIsRetiredWarning(AbstractException):
    pass

class ConcurrentUpdateWarning(AbstractException):
    pass

class ValidationWarning(AbstractException):
    pass
