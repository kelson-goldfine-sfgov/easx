from MAD.views.views import findAddressCandidates
import unittest
from MAD.utils.geojson_encode import *
from django.core.serializers.json import DjangoJSONEncoder

class ViewsFindAddressCandidatesTestCase(unittest.TestCase):

    def setUp(self):
        return

    def tearDown(self):
        return

    def getJsonFromDict(self, dict):
        return json.dumps(dict, cls=DjangoJSONEncoder, indent=4, ensure_ascii=False)

    def test_findAddressCandidates_01(self):
        responseDict = findAddressCandidates(None, '20 Sussex St', '94131')
        #print JSONEncoder(indent=4).encode(responseDict)
        self.assertTrue(len(responseDict['candidates']) > 0)
        self.assertTrue(responseDict['candidates'][0] is not None)
        candidate = responseDict['candidates'][0]
        self.assertTrue(candidate['score'] == 100)
        attributes = candidate['attributes']
        self.assertTrue(attributes['base_address_num'] == 20)
        self.assertTrue(attributes['base_address_num'] == 20)
        self.assertTrue(attributes['street_name'] == "SUSSEX")
        self.assertTrue(attributes['street_type'] == "ST")
        self.assertTrue(attributes['zipcode'] == "94131")

    def test_findAddressCandidates_02(self):
        responseDict = findAddressCandidates(None, '1000000 Sussex St', '94131')
        #print JSONEncoder(indent=4).encode(responseDict)
        self.assertTrue(len(responseDict['candidates']) == 0)

    def test_findAddressCandidates_03(self):
        for synonymousAddress in (
                    ('100 6th St', '94103'),
                    ('100 6TH ST', '94103'),
                    ('100 06TH St', '94103'),
                    ('100 sixth st', '94103'),
                    ('100 sixth St', '94103'),
                    ('100 Sixth St', '94103'),
                    ('100 SIXTH ST', '94103')
                ):
            addressString = synonymousAddress[0]
            zipCodeString = synonymousAddress[1]
            responseDict = findAddressCandidates(None, addressString, zipCodeString)
            #print JSONEncoder(indent=4).encode(responseDict)
            self.assertTrue(len(responseDict['candidates']) > 0)
            candidate = responseDict['candidates'][0]
            self.assertTrue(candidate['score'] == 100)

    def test_findAddressCandidates_04(self):
        # should have been BLVD
        responseDict = findAddressCandidates(None, '197 STANYAN ST', '94118')
        #print JSONEncoder(indent=4).encode(responseDict)
        candidate = responseDict['candidates'][0]
        self.assertTrue(candidate['score'] < 100)

    def test_findAddressCandidates_05(self):
        # should have been ST
        responseDict = findAddressCandidates(None, '207 STANYAN Blvd', '94118')
        #print JSONEncoder(indent=4).encode(responseDict)
        candidate = responseDict['candidates'][0]
        self.assertTrue(candidate['score'] < 100)

    def test_findAddressCandidates_06(self):
        for suffixVariation in (
                ('20 Sussex', '94131'),
                ('20 Sussex Strt', '94131'),
                ('20 Sussex Way', '94131'),
                ('20 Sussex Ave', '94131'),
                ('20 Sussex str', '94131'),
                ('20 Sussex Blvd', '94131')
            ):
            addressString = suffixVariation[0]
            zipCodeString = suffixVariation[1]
            responseDict = findAddressCandidates(None, addressString, zipCodeString)
            #print addressString
            #print JSONEncoder(indent=4).encode(responseDict)
            self.assertTrue(len(responseDict['candidates']) > 0)
            candidate = responseDict['candidates'][0]
            self.assertTrue(candidate['score'] <= 100)

    def test_findAddressCandidates_08(self):
        for nonExistentAddress in (
                ('10000000 Sussex St', '94131'),
                ('100 FOO BAR CAS BAH', '12345')
            ):
            addressString = nonExistentAddress[0]
            zipCodeString = nonExistentAddress[1]
            responseDict = findAddressCandidates(None, addressString, zipCodeString)
            #print JSONEncoder(indent=4).encode(responseDict)
            self.assertTrue(len(responseDict['candidates']) == 0)


    def test_findAddressCandidates_09(self):
        # TODO
        # We currently do not support post direction.
        # e.g '330 mission Bay Blvd South'
        # We are planning to add this enhancement in the very next release (1.1.8)
        # RE
        # https://sfgovdt.jira.com/wiki/display/MAD/Welcome#Welcome-milestonesroadmapRoadmap
        # http://code.google.com/p/eas/issues/detail?id=451
        for complexAddress in (
                ('20 1/2 Sussex St Unit 7', '94131', True),
                ('20 1/2 Sussex St', '94131', True),
                ('20 1/2 Sussex St Apt 7 (REAR)', '94131', False)
            ):
            addressString = complexAddress[0]
            zipCodeString = complexAddress[1]
            shouldFindCandidates = complexAddress[2]
            responseDict = findAddressCandidates(None, addressString, zipCodeString)
            #print addressString
            #print JSONEncoder(indent=4).encode(responseDict)
            if shouldFindCandidates:
                self.assertTrue(len(responseDict['candidates']) > 0)
            else:
                self.assertTrue(len(responseDict['candidates']) == 0)


    def test_findAddressCandidates_10(self):
        for levenshteinAddress in (
                ('20 Susssicks St ', '94131', False),
                ('20 Sussicks St ', '94131', False),
                ('20 Susssix St ', '94131', True),
                ('20 Susssex St ', '94131', True),
                ('20 Sussix St ', '94131', True)
            ):
            addressString = levenshteinAddress[0]
            zipCodeString = levenshteinAddress[1]
            shouldFindCandidates = levenshteinAddress[2]
            responseDict = findAddressCandidates(None, addressString, zipCodeString)
            #print addressString
            #print JSONEncoder(indent=4).encode(responseDict)
            if shouldFindCandidates:
                self.assertTrue(len(responseDict['candidates']) > 0)
            else:
                self.assertTrue(len(responseDict['candidates']) == 0)


    def test_findAddressCandidates_11(self):
        correctZipCandidate = findAddressCandidates(None, "20 sussex St", "94131")['candidates'][0]
        incorrectZipCandidate = findAddressCandidates(None, "20 sussex St", "12345")['candidates'][0]
        noZipCandidate = findAddressCandidates(None, "20 sussex St", "")['candidates'][0]
        self.assertTrue(correctZipCandidate['score'] > incorrectZipCandidate['score'])
        self.assertTrue(correctZipCandidate['score'] > noZipCandidate['score'])
        self.assertTrue(incorrectZipCandidate['score'] == noZipCandidate['score'])

    def test_findAddressCandidates_12(self):
        candidates = findAddressCandidates(None, "4376 A 24TH ST", "94114")['candidates']
        self.assertTrue(len(candidates) == 2)
        self.assertTrue(candidates[0]['score'] == 100)
        self.assertTrue(candidates[0]['attributes']['base_address_suffix'] == "A")
        self.assertTrue(candidates[1]['score'] < 100)
        self.assertTrue(candidates[1]['attributes']['base_address_suffix'] is None)

    def test_findAddressCandidates_13(self):
        candidates = findAddressCandidates(None, "4376 24TH ST", "94114")['candidates']
        self.assertTrue(len(candidates) == 2)
        self.assertTrue(candidates[0]['score'] == 100)
        self.assertTrue(candidates[1]['score'] == 100)
        zerothNumberSuffix = candidates[0]['attributes']['base_address_suffix']
        firstNumberSuffix  = candidates[1]['attributes']['base_address_suffix']
        if zerothNumberSuffix is None:
            self.assertTrue(firstNumberSuffix == "A")
        else:
            self.assertTrue(zerothNumberSuffix == "A")
            self.assertTrue(firstNumberSuffix is None)

    def test_findAddressCandidates_levenshtein_0_char_incorrect(self):

        responseDict = findAddressCandidates(None, "1013 LEAVENWORTH ST", "94109")
        #print JSONEncoder(indent=4).encode(responseDict)
        candidates = responseDict['candidates']
        self.assertTrue(len(candidates) == 1)
        self.assertTrue(candidates[0]['score'] == 100)

    def test_findAddressCandidates_levenshtein_1_char_incorrect(self):
        responseDict = findAddressCandidates(None, "1013 LEVENWORTH ST", "94109")
        #print JSONEncoder(indent=4).encode(responseDict)
        candidates = responseDict['candidates']
        self.assertTrue(len(candidates) == 1)
        self.assertTrue(candidates[0]['score'] == 98)

    def test_findAddressCandidates_levenshtein_2_char_incorrect(self):
        responseDict = findAddressCandidates(None, "1013 LEVANWORTH ST", "94109")
        #print JSONEncoder(indent=4).encode(responseDict)
        candidates = responseDict['candidates']
        self.assertTrue(len(candidates) == 1)
        self.assertTrue(candidates[0]['score'] == 96)

    def test_findAddressCandidates_levenshtein_3_char_incorrect(self):
        responseDict = findAddressCandidates(None, "1013 LEVANWERTH ST", "94109")
        #print JSONEncoder(indent=4).encode(responseDict)
        candidates = responseDict['candidates']
        self.assertTrue(len(candidates) == 1)
        self.assertTrue(candidates[0]['score'] == 94)

    def test_findAddressCandidates_levenshtein_match_fail(self):
        responseDict = findAddressCandidates(None, "1013 LEVANWERTTH ST", "94109")
        #print JSONEncoder(indent=4).encode(responseDict)
        candidates = responseDict['candidates']
        self.assertTrue(len(candidates) == 0)

    def test_findAddressCandidates_confirm_post_direction(self):
        responseDict = findAddressCandidates(None, "165 buena vista ave east", "94117")
        #print self.getJsonFromDict(responseDict)
        candidate = responseDict['candidates'][0]
        self.assertTrue(candidate['attributes']['street_post_direction'] == 'EAST')

