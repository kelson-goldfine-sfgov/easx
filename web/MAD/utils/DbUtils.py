import cStringIO
from collections import defaultdict

import logging

logger = logging.getLogger('EAS')

class DbUtils:
    pass

    #
    # The bulk operations will not commit unless you use manage the transaction manually or
    # unless some part of your save operation includes at least one model object save.
    #


    @staticmethod
    def getSequenceName(modelName):
        try:
            sequenceDictionary = {}
            sequenceDictionary['Addresses'] = 'addresses_address_id_seq'
            sequenceDictionary['CrAddresses'] = 'cr_addresses_cr_address_id_seq'
            sequenceDictionary['CrAddressXParcels'] = 'cr_address_x_parcels_id_seq'
            return sequenceDictionary[modelName]
        except:
            raise


    @staticmethod
    def getNextSequenceRange(connection, modelName, count=1):
        try:
            cursor = connection.cursor()
            sequenceName = DbUtils.getSequenceName(modelName)
            sql = "select _sfmad_get_next_sequence_range('%s', %d);" % (sequenceName, count)
            cursor.execute(sql)
            rowTuple = cursor.fetchone()
            minMaxString = rowTuple[0]
            # todo - make not so hacky
            left, right = minMaxString.split(',')
            min = int(left.lstrip('('))
            max = int(right.rstrip(')'))
            return (min, max)
        except:
            raise


    @staticmethod
    def executeBulkInsert(connection, modelList, setPrimaryKeys=False):

        def db_field_value(field, modelInstance, connection):
            base_value = field.get_db_prep_save(field.pre_save(modelInstance, add=True), connection=connection)
            return str(base_value) if base_value is not None else r'\N'
                # \N is the NULL value for copy_from

        if not modelList:
            return

        class_to_model_map = defaultdict(list)
        for model in modelList:
            class_to_model_map[model.__class__].append(model)

        cursor = connection.cursor().cursor
            # The cursor object that Django returns from connection.cursor() is a
            # Django-style cursor, not a psycopg2 cursor.  We need the latter to
            # use the copy_from method on it.

        for model_class, models_of_class in class_to_model_map.iteritems():

            fields = []
            for f in models_of_class[0]._meta.local_fields:
                if f.primary_key:
                    if setPrimaryKeys:
                        fields.append(f)
                else:
                    fields.append(f)

            columns = [ f.column for f in fields ]

            copy_block = cStringIO.StringIO()

            table_name = models_of_class[0]._meta.db_table

            for modelInstance in models_of_class:

                copy_block.write('\t'.join([ db_field_value(f, modelInstance, connection) for f in fields ]))
                copy_block.write('\r')

            copy_block.seek(0)
            cursor.copy_from(copy_block, table_name, columns=columns)


    @staticmethod
    def executeBulkUpdate(connection, modelList):

        logger.info("DbUtils.executeBulkUpdate()")

        def db_field_value(field, modelInstance, connection):
            base_value = field.get_db_prep_save(field.pre_save(modelInstance, add=False), connection=connection)
            return base_value
                # We preserve a None here so a proper NULL gets inserted into the database

        if not modelList:
            return

        # We allow heterogeneous lists of fields to update, but each iteration through can only
        # have one particular set of fields.  To support this, first, we separate the model
        # list into "buckets" of particular sets of fields to update.

        field_to_model_map = defaultdict(list)

        for model in modelList:
            try:
                field_key = frozenset(model.fieldsToUpdate)
            except AttributeError:
                raise Exception("Model " + str(model) + " does not have an attribute fieldsToUpdate.")

            field_to_model_map[field_key].append(model)

        cursor = connection.cursor().cursor
            # The cursor object that Django returns from connection.cursor() is a
            # Django-style cursor, not a psycopg2 cursor.  We need the latter to
            # use the executemany method on it.

        for fieldset, models_of_fieldset in field_to_model_map.iteritems():

            fields = []
            for f in models_of_fieldset[0]._meta.local_fields:
                if f.primary_key:
                    primary_key_field = f
                else:
                    if f.name in fieldset:
                        fields.append(f)

            fields = [f for f in fields if f.name in models_of_fieldset[0].fieldsToUpdate]

            statement = 'UPDATE ' + models_of_fieldset[0]._meta.db_table + ' SET '
            statement += ', '.join([ (r' %s=' % ( column, )) + r'%s' for column in [ f.column for f in fields ] ])
            statement += (r' WHERE %s=' % ( primary_key_field.column, ) ) + r'%s'
                # This generates an UPDATE statement of the form:
                # UPDATE <tablename> SET <fieldname>=%s, <fieldname>=%s... WHERE <primary_key_column>=%s

            parameters = []

            uniform_class = models_of_fieldset[0].__class__

            for modelInstance in models_of_fieldset:
                if not isinstance(modelInstance, uniform_class):
                    raise Exception("In executeBulkUpdate, attempt to update objects of non-uniform types.")

                values = [ db_field_value(f, modelInstance, connection) for f in fields ]
                values.append(db_field_value(primary_key_field, modelInstance, connection))

                parameters.append(tuple(values))

            cursor.executemany(statement, parameters)

