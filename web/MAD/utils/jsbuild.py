from settings import MEDIA_ROOT
import jsmin
from django.utils import simplejson as json
import gzip
import codecs
import fileinput
import os

# Takes all the files listed under media/js/build_scripts.txt and concatenates them into one file
# creates 3 files: a compiled file with all the scripts in 1 file
#                  a minimized file minified using code developed by Douglas Crockford
#				   a zipped file
def build_scripts(buildList=None, outPath=None, outFileNamePrefix=None):
    files = open(buildList, 'rU')
    js = ""
    for path in files:
        path = os.path.join(outPath, path.strip())
        piece = open(path, 'rU', 1).read()
        js = js + "\n" + piece

    f = open(os.path.join(outPath, outFileNamePrefix+'.js'), 'w')
    f.write(js)
    f.close()
    
    f = open(os.path.join(outPath, outFileNamePrefix+'-min.js'), 'w')
    f.write(jsmin.jsmin(js))
    f.close()
    
    f_in = open(os.path.join(outPath, outFileNamePrefix+'-min.js'), 'rb')
    f_out = gzip.open(os.path.join(outPath, outFileNamePrefix+'-min.js.gz'), 'wb')
    f_out.writelines(f_in)
    f_out.close()
    f_in.close()

