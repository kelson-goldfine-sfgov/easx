
# for now we do not really need anything as sophisticated as django caching
# from django.core.cache import cache

# todo - needs a complete rework - not cohesive etc etc. It was hacked together for a quick performance fix.

import logging
logger = logging.getLogger('EAS')

class CacheManager:
    
    def __init__(self, valid = True, message = '', object=None):

        from MAD.models.models import DUnitType, DAddressDisposition, DFloors

        self.cacheDict = {}

        unitTypes = {}
        for unitType in list(DUnitType.objects.all()):
            unitTypes[str(unitType.unit_type_id)] = unitType
        self.cacheDict["DUnitType"] = unitTypes

        dispositions = {}
        for disposition in list(DAddressDisposition.objects.all()):
            dispositions[str(disposition.disposition_code)] = disposition
        self.cacheDict["DAddressDisposition"] = dispositions

        floors = {}
        for floor in list(DFloors.objects.all()):
            floors[str(floor.floor_id)] = floor
        self.cacheDict["DFloors"] = floors


    def get(self, modelclassName, key):

        modelDict = None
        key = str(key)

        try:
            modelDict = self.cacheDict[modelclassName]
        except:
            # This will be a KeyError exception - we want to make that more intelligible.
            message = 'Oops! Looks like a programming error - we are not caching %s' % modelclassName
            logger.info(message)
            raise LookupError(message)

        try:
            returnValue = modelDict[key]
        except:
            # This will be a KeyError exception - we want to make that more intelligible.
            message = 'The value %s is not valid for %s.' % (str(key), modelclassName)
            logger.info(message)
            raise LookupError(message)

        return returnValue