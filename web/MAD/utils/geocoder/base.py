from operator import attrgetter
from parsing import parse, ParsingError
from MAD.models.models import VBaseAddressSearch, Streetnames
import logging

logger = logging.getLogger('EAS')

class Geocoder(object):


    @staticmethod
    def getSearchSteps(useType):
        if useType == 'BATCH':
            for streetNameUsage in ('EXACT', 'FUZZY'):
                score = 104
                for streetNameCategory in ('MAP', 'ALIAS'):
                    score -= 2
                    for suffixUsage in ('INCLUDE', 'NULL', 'EXCLUDE'):
                        score -= 2
                        yield {
                            'streetNameUsage': streetNameUsage,
                            'streetNameCategory': streetNameCategory,
                            'suffixUsage': suffixUsage,
                            'score': score
                        }
        elif useType == "INTERACTIVE":
            for streetNameUsage in ('EXACT', 'STARTS_WITH', 'FUZZY'):
                score = 104
                if streetNameUsage == 'STARTS_WITH':
                    score = 100
                for streetNameCategory in ('MAP', 'ALIAS'):
                    score -= 2
                    for suffixUsage in ('INCLUDE', 'NULL', 'EXCLUDE'):
                        score -= 2
                        yield {
                            'streetNameUsage': streetNameUsage,
                            'streetNameCategory': streetNameCategory,
                            'suffixUsage': suffixUsage,
                            'score': score
                        }
        else:
            raise Exception('unsupported parameter value %s' % useType)


    @staticmethod
    def refineForCategory(queryset, category):
        if category == 'MAP':
            queryset = queryset.filter(category = 'MAP')
        elif category == 'ALIAS':
            queryset = queryset.filter(category = 'ALIAS')
        else:
            raise Exception('unsupported streetNameCategory value: %s' % streetNameCategory)
        return queryset


    @staticmethod
    def addStreetSegments(streetSegmentsWithScore, streetNames, score):
        # give preference to higher scores
        for streetName in streetNames:
            if streetName.street_segment_id not in streetSegmentsWithScore:
                streetSegmentsWithScore[streetName.street_segment_id] = score


    @staticmethod
    def addScores(baseAddresses, streetSegmentsWithScores):
        for baseAddress in baseAddresses:
            baseAddress.score = streetSegmentsWithScores[baseAddress.street_segment_id]


    @staticmethod
    def adjustScoresForZipCode(baseAddresses, zipCodeString):
        for baseAddress in baseAddresses:
            if zipCodeString:
                if baseAddress.zipcode != zipCodeString:
                    baseAddress.score -= 2
            else:
                baseAddress.score -= 2


    @staticmethod
    def adjustScoresForInexactNumber(baseAddresses):
        for baseAddress in baseAddresses:
            baseAddress.score -= 50

    @staticmethod
    def adjustScoresForNumberSuffix(baseAddresses, parsedAddresses):
        parsedAddressNumberSuffix = None
        for parsedAddress in parsedAddresses:
            if parsedAddress['number_suffix'] is not None and parsedAddressNumberSuffix is None:
                parsedAddressNumberSuffix = parsedAddress['number_suffix']
                break

        if parsedAddressNumberSuffix is None:
            return

        from ebpub.geocoder.parser.parsing import Standardizer
        from ebpub.geocoder.parser.number_suffixes import number_suffixes
        numberSuffixStandardizer = Standardizer(number_suffixes)
        standardizedNumberSuffix = numberSuffixStandardizer(parsedAddressNumberSuffix).decode('utf-8')

        matchFound = False
        for baseAddress in baseAddresses:
            if baseAddress.base_address_suffix == standardizedNumberSuffix:
                matchFound = True
                break

        for baseAddress in baseAddresses:
            if baseAddress.base_address_suffix != standardizedNumberSuffix and matchFound:
                baseAddress.score -= 2


    @staticmethod
    def sortOnScore(baseAddresses):
        return sorted(baseAddresses, key=lambda baseAddress: baseAddress.score, reverse=True)

    @staticmethod
    def sortOnAddress(baseAddresses):
        return sorted(baseAddresses, key=attrgetter('base_address_num', 'base_address_suffix', 'street_name', 'street_type', 'street_category', 'jurisdiction'))


    @staticmethod
    def findStreetsByName(parsedAddresses=(), useType="INTERACTIVE"):
        streetSegmentsWithScore = {}
        for algorithmStep in Geocoder.getSearchSteps(useType):
            streetNameUsage = algorithmStep['streetNameUsage']
            streetNameCategory = algorithmStep['streetNameCategory']
            suffixUsage = algorithmStep['suffixUsage']
            score  = algorithmStep['score']
            # Traversed the parsed addresses in reverse order (from more complete to less complete).
            parsedAddresses.reverse()
            saveScore = score
            for parsedAddress in parsedAddresses:
                score = saveScore
                streetNames = Streetnames.objects.none()
                if streetNameUsage == 'EXACT':
                    streetNames = Streetnames.objects.filter(base_street_name = parsedAddress["street"])
                    streetNames = Geocoder.refineForCategory(streetNames, streetNameCategory)
                    if parsedAddress["post_dir"]:
                        streetNames = streetNames.filter(post_direction = parsedAddress["post_dir"])
                elif streetNameUsage == 'STARTS_WITH':
                    streetNames = Streetnames.objects.filter(base_street_name__startswith = parsedAddress["street"])
                    streetNames = Geocoder.refineForCategory(streetNames, streetNameCategory)
                elif streetNameUsage == 'FUZZY' and len(streetSegmentsWithScore) == 0:
                    # This will table scan.
                    for levDist in (1, 2, 3):
                        score -= 2
                        streetNames = Streetnames.objects.all().extra(select={"lev_dist" : "levenshtein(base_street_name, '%s')" % parsedAddress["street"]})
                        streetNames = streetNames.extra(where=["levenshtein(base_street_name, %s) <= %s"], params=[parsedAddress["street"], levDist])
                        streetNames = Geocoder.refineForCategory(streetNames, streetNameCategory)
                        if streetNames.count() > 0:
                            break
                if suffixUsage == "INCLUDE":
                    if not parsedAddress["suffix"]:
                        continue
                    streetNames = streetNames.filter(street_type = parsedAddress["suffix"])
                    if streetNames.count() > 0:
                        Geocoder.addStreetSegments(streetSegmentsWithScore, streetNames, score)
                elif suffixUsage == "EXCLUDE":
                    if streetNames.count() > 0:
                        Geocoder.addStreetSegments(streetSegmentsWithScore, streetNames, score)
                elif suffixUsage == "NULL":
                    streetNames = streetNames.filter(street_type__isnull = True)
                    if streetNames.count() > 0:
                        Geocoder.addStreetSegments(streetSegmentsWithScore, streetNames, score)
                else:
                    raise Exception('unsupported parameter value %s' % suffixUsage)
                #####
                #print streetNameUsage, streetNameCategory, suffixUsage, score
                #print parsedAddress
                #print 'length of streetSegmentsWithScore: %s' % len(streetSegmentsWithScore)
                #print '----------'
                #####

        return streetSegmentsWithScore


    @staticmethod
    def geocode(addressString=None, zipCodeString=None, useType=None, includeRetired=False):

        if useType not in ("INTERACTIVE", "BATCH"):
            raise Exception('unsupported parameter value %s' % useType)

        parsedAddresses = []
        from MAD.utils.geocoder.parsing import parse, ParsingError
        try:
            parsedAddresses = parse(addressString)
        except ParsingError, e:
            return ()

        streetSegmentsWithScores = Geocoder.findStreetsByName(parsedAddresses=parsedAddresses, useType=useType)
        if len(streetSegmentsWithScores) == 0:
            return ()

        addressNumber = parsedAddresses[0]['number']
        streetSegmentIds = streetSegmentsWithScores.keys()
        baseAddresses = VBaseAddressSearch.objects\
            .filter(street_segment_id__in = streetSegmentIds)\
            .filter(base_address_num = addressNumber)\
            .filter(street_category = 'MAP')

        if not includeRetired:
            baseAddresses = baseAddresses.filter(retire_tms__isnull = True)

        # If we did not find anything using the exact number, we look for nearby numbers.
        usedNearestNumber = False
        if len(baseAddresses) < 1 and useType == "INTERACTIVE":
            usedNearestNumber = True
            baseAddresses = VBaseAddressSearch.objects\
            .filter(street_segment_id__in = streetSegmentIds)\
            .filter(street_category = 'MAP')
            baseAddresses = baseAddresses.extra(select={"calc_sort_order" : "abs(base_address_num - %s)" % addressNumber}, order_by=["-street_category", "calc_sort_order", "base_address_num", "street_name"])

        if not includeRetired:
            baseAddresses = baseAddresses.filter(retire_tms__isnull = True)

        # This slice value has been empirically derived.
        # If you ask for 1000 Buena Vista Ave East, if this number is say 50, you will get mostly Buena Vista Ave West.
        # So the idea would be to get more than we'll use, sort on score, then trim back, the resort on address.
        baseAddresses = baseAddresses[0:100]

        Geocoder.addScores(baseAddresses, streetSegmentsWithScores)
        if usedNearestNumber:
            Geocoder.adjustScoresForInexactNumber(baseAddresses)
        if useType == "BATCH":
            Geocoder.adjustScoresForZipCode(baseAddresses, zipCodeString)
        Geocoder.adjustScoresForNumberSuffix(baseAddresses, parsedAddresses)
        baseAddresses = Geocoder.sortOnScore(baseAddresses)
        baseAddresses = baseAddresses[0:50]
        if useType == "INTERACTIVE":
            baseAddresses = Geocoder.sortOnAddress(baseAddresses)
        return baseAddresses
