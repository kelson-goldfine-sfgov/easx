from geojson_encode import *

# a small class to encapsulate ext compatible errors and return status
class GenericResponseObj(object):
    success = False
    status_code = -1
    message = ""
    errors = {} #a dict of names that coorespond to fields submitted and their error messages to be displayed in ExtJs
    returnObj = {}
    def __init__(self, m):
        self.message = m
        self.success = False
        self.status_code = -1
        self.errors = {} #a dict of names that coorespond to fields submitted and their error messages to be displayed in ExtJs
        self.returnObj = {}
    def __str__(self):
        ret = {}
        ret["returnObj"] = self.returnObj
        ret["errors"] = self.errors
        ret["message"] = self.message
        ret["status_code"] = self.status_code
        ret["success"] = self.success
        return geojson_encode(ret)