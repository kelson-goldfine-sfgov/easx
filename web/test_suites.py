import os
# ebpub requires that the django settings be loaded
# This allows us to use plain old python unit testing - and avoid some django overhead.
# Still not sure what the right mix is here.

os.environ['DJANGO_SETTINGS_MODULE']='settings'

import unittest

# To set up tests, lines such as this should be in settings_deploy.py
#     TEST_RUNNER = "test_suites.runAll"
# The tests are setup in
#     test_suites.py
# then use the standard django testing command line as follows:
#     > python manage.py test
#
# To refine the set of tests add this line
#TEST_RUNNER = "test_suites.runChangeRequestSuite"


def runSuite(suiteClass):
    testSuite = unittest.TestLoader().loadTestsFromTestCase(suiteClass)
    unittest.TextTestRunner(verbosity=2).run(testSuite)

def runAll(test_labels, verbosity=1, interactive=True, extra_tests=()):

    from MAD.models.tests.parcels import ParcelTestCase
    runSuite(ParcelTestCase)

    from MAD.models.tests.misc import AddressXParcelsTestCase, AddressBaseTestCase, CrAddressXParcelsTestCase, ClusteringTestCase
    runSuite(AddressXParcelsTestCase)
    runSuite(AddressBaseTestCase)
    runSuite(CrAddressXParcelsTestCase)
    runSuite(ClusteringTestCase)

    from MAD.models.tests.cr_change_requests import CrChangeRequestTestCase
    runSuite(CrChangeRequestTestCase)

    from MAD.models.tests.ModelUtils import ModelUtilsTestCase
    runSuite(ModelUtilsTestCase)

    from MAD.utils.DbUtilsTests import DbUtilsTests
    runSuite(DbUtilsTests)

    from MAD.views.tests.views__find_address_candidates import ViewsFindAddressCandidatesTestCase
    runSuite(ViewsFindAddressCandidatesTestCase)

    from MAD.utils.geocoder.tests import InteractiveGeocodingTestCase
    runSuite(InteractiveGeocodingTestCase)

