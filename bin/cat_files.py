import os
from optparse import OptionParser

def main():

    usage = "usage: %prog --f filename"

    parser = OptionParser(usage)
    parser.add_option("--filename", dest="filename", help="specify input file name")

    (options, args) = parser.parse_args()
    if options.filename == None:
        parser.error("Please specify an input file name.")

    if not os.path.exists(options.filename):
        print options.filename + ' does  not exist'
        return

    file = open(options.filename, 'r')
    for filename in file.readlines():
        
        # remove newline
        filename = filename.rstrip()
        
        if not os.path.exists(filename):
            print options.filename + ' does  not exist'
            return
        
        sqlFile = open(filename, 'r')
        
        for line in sqlFile.readlines():
            # remove newline
            line = line.rstrip()
            print line

if __name__ == "__main__":
    main()

