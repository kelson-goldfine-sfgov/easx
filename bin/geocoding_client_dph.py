
import urllib, httplib
import sys, os
import csv
import json
import codecs
from collections import OrderedDict

def usage():
    print """
usage:
    %s
    <host ip> <host port>
    <input file path> <output file path>
    <address column name> [zip code column name]
example command line:
    python geocoding_client_dph.py eas.sfgov.org 80 c:/temp/input.csv c:/temp/output.csv Address Zip
""" %  sys.argv[0]

def geocode(dict = None, addressStringKey = None, zipCodeKey = None, networkAddress = None):
    addressString = ''
    zipCodeString = ''

    try:
        addressString = dict[addressStringKey]
    except KeyError, e:
        raise

    try:
        zipCodeString = dict[zipCodeKey]
    except KeyError, e:
        pass

    httpConnection = httplib.HTTPConnection('%s:%s' % (networkAddress['host'], networkAddress['port']))
    try:
        encodedParameters = urllib.urlencode({
            'f' : 'json',
            'Address' : addressString,
            'Zip' : zipCodeString
        })
        httpConnection.request("GET", '/geocode/findAddressCandidates/?' + encodedParameters)
        response = httpConnection.getresponse()
        if response.status != 200:
            raise IOError('request failed - status: %s - message: %s' % (str (response.status), str(response.msg)))
        contentString = response.read()
        contentDict = json.loads(contentString)
    except Exception, e:
        raise
    finally:
        httpConnection.close()
    return contentDict


def getBaseAddressColumnNames():
    return [
        'match_address',
        'match_x',
        'match_y',
        'match_score',
        'match_base_address_num',
        'match_base_address_suffix',
        'match_street_name',
        'match_street_type',
        'match_zipcode'
    ]

def getUnitAddressColumnNames():
    return [
        'match_unit_num'
    ]

def getParcelAddressColumnNames():
    return [
        'match_map_blk_lot',
        'match_blk_lot',
        'match_unit_num',
        'match_date_map_add',
        'match_date_map_drop',
        'match_address_base_flg'
    ]


def getInputColumnNames(inputFileName):
    # e.g. businessID,date of violation,violation category,ViolationType,RegulatoryAuthority,Address,name
    inputFile = None
    inputColumnNames = None
    dictRow = None
    try:
        inputFile = open(inputFileName, 'rb')
        dictReader = csv.DictReader(inputFile)
        dictRow = dictReader.next()
        inputColumnNames = dictReader.fieldnames
    finally:
        try: inputFile.close()
        except Exception, e: pass
    return inputColumnNames

def getViewDictForBaseAddress(candidateAddress):
    columns = getBaseAddressColumnNames()
    base_address_suffix = candidateAddress['attributes']['base_address_suffix']
    if base_address_suffix is not None:
        base_address_suffix = base_address_suffix.encode('utf8')
    return OrderedDict([
        (columns[0], candidateAddress['address'].encode('utf8')),
        (columns[1], candidateAddress['location']['x']),
        (columns[2], candidateAddress['location']['y']),
        (columns[3], candidateAddress['score']),
        (columns[4], candidateAddress['attributes']['base_address_num']),
        (columns[5], base_address_suffix),
        (columns[6], candidateAddress['attributes']['street_name']),
        (columns[7], candidateAddress['attributes']['street_type']),
        (columns[8], candidateAddress['attributes']['zipcode'])
    ])

def getViewDictForParcel(parcelAddress):
    columns = getParcelAddressColumnNames()
    # TODO - reduce brittleness
    return OrderedDict([
        (columns[0], parcelAddress['map_blk_lot']),
        (columns[1], parcelAddress['blk_lot']),
        (columns[2], parcelAddress['unit_num']),
        (columns[3], parcelAddress['date_map_add']),
        (columns[4], parcelAddress['date_map_drop']),
        (columns[5], parcelAddress['address_base_flg'])
    ])

def getOutputDict(dicts=()):
    outputDict = OrderedDict()
    for dict in dicts:
        for key, value in dict.items():
            outputDict[key] = value
    return outputDict

def validateInit():

    if len(sys.argv) < 6:
        print 'need minimum of 5 input args - got %s' % str(len(sys.argv)-1)
        usage()
        sys.exit(1)

    hostIp = sys.argv[1]
    hostPort = sys.argv[2]
    inputFileName = sys.argv[3]
    outputFileName = sys.argv[4]
    addressColumnName = sys.argv[5]
    zipCodeColumnName = None
    if len(sys.argv) == 7:
        zipCodeColumnName = sys.argv[6]

    if not os.path.exists(inputFileName):
        print 'input file (%s) not found' % inputFileName
        usage()
        sys.exit(1)

    if not os.path.isfile(inputFileName):
        print 'specified input file (%s) is not a file' % inputFileName
        usage()
        sys.exit(1)

    if os.path.exists(outputFileName):
        if os.path.isfile(outputFileName):
            os.remove(outputFileName)

    inputColumnNames = getInputColumnNames(inputFileName)
    if addressColumnName not in inputColumnNames:
        print 'address column name (%s) not found in input file (%s)' % (addressColumnName, inputFileName)
        print 'try: %s' % str(inputColumnNames)
        sys.exit(1)
    if zipCodeColumnName and zipCodeColumnName not in inputColumnNames:
        print 'zip code column name (%s) not found in input file (%s)' % (zipCodeColumnName, inputFileName)
        print 'try: %s' % str(inputColumnNames)
        sys.exit(1)

    return inputFileName, outputFileName, addressColumnName, zipCodeColumnName, hostIp, hostPort


def getRowCountDict(rowCountColumnNames, inputRowNumber, candidateRowNumber, parcelRowNumber):
    return OrderedDict([
        (rowCountColumnNames[0], inputRowNumber),
        (rowCountColumnNames[1], candidateRowNumber),
        (rowCountColumnNames[2], parcelRowNumber)
    ])

def main():

    inputFileName, outputFileName, addressColumnName, zipCodeColumnName, hostIp, hostPort = validateInit()
    networkAddress = {'host': hostIp, 'port': hostPort}

    inputFile = open(inputFileName, 'rb')
    outputFile = open(outputFileName, 'wb')
    outputFile.write(codecs.BOM_UTF8)

    rowCountColumnNames = ["match_input_row", "match_candidate_row", "match_parcel_row"]
    inputColumnNames = getInputColumnNames(inputFileName)
    baseAddressColumnNames = getBaseAddressColumnNames()
    parcelAddressColumnNames = getParcelAddressColumnNames()
    # The order of the columns in allColumnNames dictates the order of the output columns.
    allColumnNames = inputColumnNames + rowCountColumnNames + baseAddressColumnNames + parcelAddressColumnNames
    try:
        reader = csv.DictReader(inputFile)
        writer = csv.DictWriter(outputFile, fieldnames=allColumnNames)
        # write the column header
        writer.writerow(dict( (n,n) for n in allColumnNames ))
        inputRowNumber = 0
        for inputRowDict in reader:
            inputRowNumber += 1
            geocodeResultsDict = geocode(dict = inputRowDict, addressStringKey = addressColumnName, zipCodeKey = zipCodeColumnName, networkAddress = networkAddress)
            if len(geocodeResultsDict['candidates']) > 0:
                candidateRowNumber = 0
                for candidateAddress in geocodeResultsDict['candidates']:
                    candidateRowNumber += 1
                    parcelRowNumber = 0
                    for parcelAddress in candidateAddress['attributes']['parcels']:
                        parcelRowNumber += 1
                        rowCountDict = getRowCountDict(rowCountColumnNames, inputRowNumber, candidateRowNumber, parcelRowNumber)
                        baseAddressViewDict = getViewDictForBaseAddress(candidateAddress)
                        parcelAddressViewDict = getViewDictForParcel(parcelAddress)
                        outputDict = getOutputDict(dicts = (inputRowDict, rowCountDict, baseAddressViewDict, parcelAddressViewDict))
                        writer.writerow(outputDict)
            else:
                outputDict = getOutputDict(dicts = (inputRowDict,))
                writer.writerow(outputDict)

            rightPad = " "
            if (inputRowNumber % 10) == 0:
                rightPad = "\n"
            sys.stdout.write("%s%s" % (inputRowNumber, rightPad))

    finally:
        try: inputFile.close()
        except Exception, e: pass
        try: outputFile.close()
        except Exception, e: pass


if __name__ == "__main__":
    main()

