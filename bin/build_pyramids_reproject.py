
# http://docs.geoserver.org/stable/en/user/tutorials/imagepyramid/imagepyramid.html

import os, glob, sys
from sys import argv
import subprocess
import osgeo.gdal
import osgeo.osr

GDAL_HOME = None
key = 'GDAL_HOME'
try:
    GDAL_HOME = os.environ[key]
except Exception:
    sys.stdout.write('missing system variable: %s\n' % key)
    sys.exit(-1)


def usage():
    print """python %s
""" % argv[0]
    sys.exit()



def retile(optFilePath=None, pyramidOutputPath=None, levels=None, epsg=None):
    import shutil
    if os.path.exists(pyramidOutputPath):
        shutil.rmtree(pyramidOutputPath)
    os.makedirs(pyramidOutputPath)

    # e.g. gdal_retile.py -v -r bilinear -levels 4 -ps 2048 2048 -co "TILED=YES" -co "COMPRESS=JPEG" -targetDir bmpyramid --optfile 'foo.txt'
    command = []
    command.append('python2.5')
    command.append('gdal_retile_with_alpha.py')
    command.append('-v')
    command.append('-r')
    command.append('bilinear')
    command.append('-co')
    command.append('ALPHA=YES')
    command.append('-levels')
    command.append(str(levels))
    command.append('-ps')
    command.append('2048')
    command.append('2048')
    command.append('-s_srs')
    command.append('EPSG:%s' % epsg)
    #command.append('-co')
    #command.append('TILED=YES')
    #command.append('-co')
    #command.append('BLOCKXSIZE=256')
    #command.append('-co')
    #command.append('BLOCKYSIZE=256')
    command.append('-targetDir')
    command.append(pyramidOutputPath)
    command.append('--optfile')
    command.append(optFilePath)
    print "executing command line: %s\n" % ' '.join(command)

    process = subprocess.Popen(command, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    print "command return code: " + str(process.returncode)
    if process.returncode != 0:
        sys.exit(-1)
    else:
        #print "command success"
        pass


def reproject(inputFilePath=None, outputFilePath=None, outputImageType=None, inputEpsg=None, outputEpsg=None):
    # e.g. gdalwarp -s_srs EPSG:2227 -t_srs \
    # "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext +no_defs" \
    #  -co "TILED=YES" -co "COMPRESS=JPEG" %s %s
    command = []
    command.append('gdalwarp')
    command.append('-s_srs')
    command.append('EPSG:%s' % inputEpsg)
    command.append('-t_srs')
    command.append('EPSG:%s' % outputEpsg)
    if outputImageType == 'jpg':
        command.append('-co')
        command.append('COMPRESS=JPEG')
    command.append('-dstalpha')
    command.append(inputFilePath)
    command.append(outputFilePath)
    print "executing command line: %s\n" % ' '.join(command)
    process = subprocess.Popen(command, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    if process.returncode != 0:
        print "***** command failure *****"
        sys.exit(-1)
    else:
        #print "command success"
        pass


def createGeoFiles(filePath):
    # mostly from http://gis.stackexchange.com/questions/9421/how-to-create-tfw-and-prj-files-for-a-bunch-of-geotiffs
    source = osgeo.gdal.Open(filePath)
    sourceTransform = source.GetGeoTransform()

    source_srs = osgeo.osr.SpatialReference()
    source_srs.ImportFromWkt(source.GetProjection())
    source_srs.MorphToESRI()
    source_wkt = source_srs.ExportToWkt()

    prjFilename = os.path.splitext(filePath)[0] + '.prj'
    prjFile = open(prjFilename, 'wt')
    prjFile.write(source_wkt)
    prjFile.close()

    source = None

    twfFilename = os.path.splitext(filePath)[0] + '.tfw'
    tfwFile = open(twfFilename, 'wt')
    tfwFile.write("%0.8f\n" % sourceTransform[1])
    tfwFile.write("%0.8f\n" % sourceTransform[2])
    tfwFile.write("%0.8f\n" % sourceTransform[4])
    tfwFile.write("%0.8f\n" % sourceTransform[5])
    tfwFile.write("%0.8f\n" % sourceTransform[0])
    tfwFile.write("%0.8f\n" % sourceTransform[3])
    tfwFile.close()

def downloadData(sftpAccess=None, downloadDir=None):
    pass
    # wget the shapefile
    # for each record in the shape file
    #   if fully within bbox
    #       wget the image
    # wget notes
    # make this section optional (parm or interrogate user)
    # do not hard code these SFTP credentials (parm or interrogate user)
    # for user intterogation see web/bin/deploy_source.py
    # tell user that the credentials are here - http://10.250.60.17/citypedia/index.php/SFGIS_FTP_Resource
    # here is the command line
    #  wget -North.* -r --no-directories --user=SFGISRO --password=YhmDA4xS https://sfftp.sfgov.org/Data/Imagery/Mosaic_2012/CASANF12-SID-6INCH
    # note that command line idiom is OK but this may be more understandable (depending)
    # commandList = " ".split("wget -North.* -r --no-directories --user=SFGISRO --password=YhmDA4xS https://sfftp.sfgov.org/Data/Imagery/Mosaic_2012/CASANF12-SID-6INCH")

def createRetileListFile(sourceDir, outputFilePath, fileSuffix):
    if os.path.exists(outputFilePath):
        os.remove(outputFilePath)
    inputFilePaths = glob.glob(sourceDir + "/*." + fileSuffix)
    outputFile = open(outputFilePath, 'wt')
    for inputFilePath in inputFilePaths:
        outputFile.write("%s\n" % inputFilePath)
    outputFile.close()

def go(imageSourceDir=None, tempDir=None, pyramidOutputDir=None, inputImageType='tif', outputImageType='tif', levels=7, inputEpsg=2227, outputEpsg=2227):

    # house keeping
    if not os.path.exists(imageSourceDir):
        raise OSError('source directory not found: %s' % imageSourceDir)
    import shutil
    if os.path.exists(tempDir):
        shutil.rmtree(tempDir)
    os.mkdir(tempDir)

    # reproject
    if inputEpsg != outputEpsg:
        inputFilePaths = glob.glob(imageSourceDir + "/*." + inputImageType)
        for inputFilePath in inputFilePaths:
            baseName = os.path.basename(inputFilePath)
            baseName = os.path.splitext(baseName)[0] + '.' + outputImageType
            outputFilePath = os.path.join(tempDir, baseName)
            reproject(inputFilePath=inputFilePath, outputFilePath=outputFilePath, outputImageType=outputImageType, inputEpsg=inputEpsg, outputEpsg=outputEpsg)
            createGeoFiles(outputFilePath)
        sourceDir = tempDir
    else:
        sourceDir = imageSourceDir

    # retile
    optFilePath = os.path.join(os.getcwd(), 'optfile.txt')
    createRetileListFile(sourceDir, optFilePath, outputImageType)
    retile(optFilePath=optFilePath, pyramidOutputPath=pyramidOutputDir, levels=levels, epsg=outputEpsg)

    print 'done!'


def main():

    go(
        imageSourceDir='/home/dev/ip/in',
        tempDir='/home/dev/ip/wk',
        pyramidOutputDir='/home/dev/ip/out',
        inputImageType='tif',
        outputImageType='tif',
        levels=3,
        inputEpsg=2227,
        outputEpsg=900913
    )


if __name__ == "__main__":
    main()

