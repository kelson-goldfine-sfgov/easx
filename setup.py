import sys
import os
import subprocess
import copy
import shutil
import urllib
import tarfile
from StringIO import StringIO


def usage():
    print 'usage: %s localTargetPath' % sys.argv[0]
    exit(1)


def initVersionControlParms(description, versionControlDict):
    import getpass
    message = '\nPlease enter values for %s' % description
    print message
    print '-' * len(message)
    for k,v in versionControlDict.iteritems():
        if k == 'PASSWORD':
            capturedValue = getpass.getpass()
        else:
            sys.stdout.write('%s (default %s):' % (k,v))
            capturedValue = raw_input()
        if capturedValue:
            versionControlDict[k] = capturedValue
    print '\n'


def bitbucketExport(targetPath, team, project, user, password):
    print 'retrieving libraries....'
    url = 'https://%s:%s@bitbucket.org/%s/%s/get/master.tar.gz' % (user, password, team, project)
    tfile = tarfile.open(mode="r:gz",fileobj=StringIO(urllib.urlopen(url).read()))
    tempFolder = os.path.join(targetPath,'temp')
    tfile.extractall(tempFolder)
    tempSrcFolder = None
    for dirname in os.listdir(tempFolder):
        expectedDirPrefix = '%s-%s' % (team, project)
        if expectedDirPrefix in dirname:
            tempSrcFolder = os.path.join(tempFolder, dirname)
    if tempSrcFolder is None:
        raise Exception('there was a problem locating the downloaded repo archive')
    else:
        print 'moving libraries....'
        shutil.move(os.path.join(tempSrcFolder, 'extjs/extjs-4.1.0/'), os.path.join(targetPath, 'Media/ui_frameworks/extjs-4.1.0/'))
        shutil.move(os.path.join(tempSrcFolder, 'OpenLayers/OpenLayers-2.12/'), os.path.join(targetPath, 'Media/ui_frameworks/OpenLayers-2.12/'))
        print 'removing temp dir...'
        shutil.rmtree(os.path.join(tempFolder))


def main():
    if len(sys.argv) < 2:
        usage()
    elif len(sys.argv) == 2:
        LOCAL_TARGET_PATH = sys.argv[1]
    else:
        usage()

    bitbucketParms = {'USER': None, 'PASSWORD': None, 'TEAM': 'sfgovdt', 'PROJECT': 'thirdpartyx'}
    initVersionControlParms('BITBUCKET', bitbucketParms)

    bitbucketExport(LOCAL_TARGET_PATH, bitbucketParms['TEAM'], bitbucketParms['PROJECT'], bitbucketParms['USER'], bitbucketParms['PASSWORD'])
    print 'done!'
    

if __name__ == "__main__":
    main()

