echo initializing geoserver deploy

EAS_BRANCH=master
export EAS_BRANCH

EAS_TEAM=sfgovdt
export EAS_TEAM

EAS_REPO=easx
export EAS_REPO

CONFIG_BRANCH=master
export CONFIG_BRANCH

CONFIG_TEAM=sfgovdt
export CONFIG_TEAM

CONFIG_REPO=easconfigxsf
export CONFIG_REPO

read -p "BitBucket usename: " BITBUCKET_USER; echo
export BITBUCKET_USER
stty -echo
read -p "BitBucket password: " BITBUCKET_PASSWORD; echo
export BITBUCKET_PASSWORD
stty echo

#curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/$EAS_TEAM/$EAS_REPO/raw/$EAS_BRANCH/geoserver/deploy_geoserver_env.sh
chmod 700 ./deploy_geoserver_env.sh

#curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/$EAS_TEAM/$EAS_REPO/raw/$EAS_BRANCH/geoserver/deploy_geoserver.py
chmod 600 ./deploy_geoserver.py

#curl -O https://$BITBUCKET_USER:$BITBUCKET_PASSWORD@bitbucket.org/$CONFIG_TEAM/$CONFIG_REPO/raw/$CONFIG_BRANCH/web/settings_env/live/environments.py
chmod 600 ./environments.py

. ./deploy_geoserver_env.sh
# use unbuffered for prettier stdout
$PYTHON_EXE_PATH -u deploy_geoserver.py
