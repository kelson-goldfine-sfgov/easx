import sys
import os
import subprocess
import time
import socket
import copy
import shutil
import urllib
import tarfile
from StringIO import StringIO
existingUmask = os.umask(066)
from environments import Environments
os.umask(existingUmask)


def printCountDown(seconds=9):
    for i in range(seconds, -1, -1):
        sys.stdout.write(str(i))
        time.sleep(1)
    sys.stdout.write('\n')


def before(fn):
    def wrapped():
        sys.stdout.write("\n")
        fn()
    return wrapped


def after(fn):
    def wrapped():
        fn()
        sys.stdout.write("\n")
        printCountDown()
    return wrapped


def executeSystemCommandString(commandString):
    commandList = commandString.split()
    executeSystemCommandList(commandList)


def executeSystemCommandList(commandList):
    print 'executing system command: %s' %  ' '.join(commandList)
    process = subprocess.Popen(commandList, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    process.wait()
    #print "command return code: " + str(process.returncode)
    if process.returncode != 0:
        print "**************"
        print "command failed"
        print "**************"
        printCountDown()
    else:
        #print "command success"
        pass


@before
@after
def archiveExistingDeploy():
    GEOSERVER_ARCHIVE_PATH = os.environ['GEOSERVER_ARCHIVE_PATH']
    if not os.path.exists(GEOSERVER_ARCHIVE_PATH):
        os.makedirs(GEOSERVER_ARCHIVE_PATH)
    targetFileName = 'geoserver_cfg_%s.tar' % time.strftime("%Y%m%d_%H%M%S")
    targetPath = os.path.join(GEOSERVER_ARCHIVE_PATH, targetFileName)
    sourcePath = os.environ['GEOSERVER_DATA_PATH']
    print 'archiving existing geoserver configuration'
    print '\tfrom %s' % sourcePath
    print '\tto %s' % targetPath
    time.sleep(5)
    executeSystemCommandString('tar -cf %s %s' % (targetPath, sourcePath))


@before
@after
def removeExistingConfigs():
    GEOSERVER_DATA_PATH = os.environ['GEOSERVER_DATA_PATH']
    print 'removing old configurations at %s' % GEOSERVER_DATA_PATH
    executeSystemCommandString('rm -rf %s' % GEOSERVER_DATA_PATH)


@before
@after
def setSecurity():
    GEOSERVER_DATA_PATH = os.environ['GEOSERVER_DATA_PATH']
    print 'setting permissions and ownership on %s' % GEOSERVER_DATA_PATH
    executeSystemCommandString('chown -R tomcat:geodev %s' % GEOSERVER_DATA_PATH)
    executeSystemCommandString('chmod -R ug+rwX %s' % GEOSERVER_DATA_PATH)
    executeSystemCommandString('chmod -R o-rwxX %s' % GEOSERVER_DATA_PATH)


@before
@after
def deployConfigs():
    print 'getting latest geoserver configuration from version control'
    time.sleep(3)
    url = 'https://%s:%s@bitbucket.org/%s/%s/get/master.tar.gz' % (os.environ['BITBUCKET_USER'], os.environ['BITBUCKET_PASSWORD'], os.environ['CONFIG_TEAM'], os.environ['CONFIG_REPO'])
    #tfile = tarfile.open(mode="r:gz",fileobj=StringIO(urllib.urlopen(url).read()))
    tempFolder = os.path.join('/var','tmp')
    #tfile.extractall(tempFolder)
    tempSrcFolder = None
    for dirname in os.listdir(tempFolder):
        expectedDirPrefix = '%s-%s' % (os.environ['CONFIG_TEAM'], os.environ['CONFIG_REPO'])
        if expectedDirPrefix in dirname:
            tempSrcFolder = os.path.join(tempFolder, dirname)
    if tempSrcFolder is None:
        raise Exception('there was a problem locating the downloaded repo archive')
    else:
        shutil.move(os.path.join(tempSrcFolder, 'geoserver/configuration/live'), os.environ['GEOSERVER_DATA_PATH'])    
        #shutil.rmtree(os.path.join(tempSrcFolder))


def getEnvironment():
    hostIpAddress = socket.gethostbyname(socket.gethostname())
    environments = Environments()
    environment = environments.getEnvironmentByIp(hostIpAddress)
    if environment is None:
        raise Exception('environment not found for %s' % hostIpAddress)
    return environment


@before
@after
def setLoggingLevel():
    environment = getEnvironment()
    from xml.etree.ElementTree import ElementTree
    loggingPath = os.path.join(os.environ['GEOSERVER_DATA_PATH'], 'logging.xml')
    tree = ElementTree()
    tree.parse(loggingPath)
    root = tree.getroot()
    node = root.find('level')
    print 'setting logging level to %s' % environment['GEOSERVER_LOGGING_LEVEL']
    node.text = environment['GEOSERVER_LOGGING_LEVEL']
    tree.write(loggingPath)


@before
@after
def setDatabaseConnections():

    print 'setting database connections'
    environment = getEnvironment()

    def transformNode(workspace, connectionParamsNode):
        for child in connectionParamsNode.getchildren():
            for key in child.keys():
                if child.get(key) == 'host':
                    host = environment['DB_HOST']
                    print 'setting db host to %s' % host
                    child.text = host
                if child.get(key) == 'port':
                    port = environment['DB_PORT']
                    print 'setting db port to %s' % port
                    child.text = port
                if child.get(key) == 'database':
                    databaseName = None
                    if workspace == 'SFMAPS':
                        databaseName = environment['SFMAPS_DB_NAME']
                    elif workspace == 'EAS':
                        databaseName = environment['EAS_DB_NAME']
                    print 'setting db name to %s' % databaseName
                    child.text = databaseName

    from xml.etree.ElementTree import ElementTree
    datastoreDict = {}
    datastoreDict['SFMAPS'] = os.path.join(os.environ['GEOSERVER_DATA_PATH'], 'workspaces', 'sfmaps', 'SFMAPS', 'datastore.xml')
    for workspace, datastorePath in datastoreDict.iteritems():
        print 'processing %s' % workspace
        tree = ElementTree()
        tree.parse(datastorePath)
        root = tree.getroot()
        connectionParamsNode = root.find('connectionParameters')
        transformNode(workspace, connectionParamsNode)
        tree.write(datastorePath)


@before
@after
def stopTomcat():
    print 'stopping tomcat...'
    executeSystemCommandString('/sbin/service tomcat stop')


@before
@after
def startTomcat():
    print 'starting tomcat...'
    executeSystemCommandString('/sbin/service tomcat start')


@before
@after
def checkSystemParameters():
    print 'using these system parameters'
    time.sleep(1)
    for key in ('TOMCAT_WEBAPPS_PATH', 'GEOSERVER_ARCHIVE_PATH', 'GEOSERVER_DATA_PATH', 'GWC_PATH', 'PYTHON_EXE_PATH', 'BITBUCKET_USER', 'CONFIG_TEAM', 'CONFIG_REPO', ):
        try:
            value = os.environ[key]
            print '\t%s: %s' % (key, value)
        except Exception, e:
            sys.stdout.write("%s\n" % e.message)
            sys.stdout.write('we might be missing a required system variable: %s\n' % key)
            sys.exit(-1)


def main():
    checkSystemParameters()
    stopTomcat()
    archiveExistingDeploy()
    removeExistingConfigs()
    deployConfigs()
    setSecurity()
    setLoggingLevel()
    setDatabaseConnections()
    startTomcat()
    print "\ndone!"


if __name__ == "__main__":
    main()

