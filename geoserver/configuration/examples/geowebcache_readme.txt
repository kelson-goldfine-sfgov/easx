This will hold important information about the geowebcache configuration

1.) 
The file geowebcache.xml must be in the same directory as the cache directory. The cache directory is defined in webapps/geoserver/WEB-INF/web.xml 
	<context-param>
	    <param-name>GEOWEBCACHE_CACHE_DIR</param-name>
	    <param-value>/mnt/fs/geowebcache_dir</param-value>
	 </context-param>