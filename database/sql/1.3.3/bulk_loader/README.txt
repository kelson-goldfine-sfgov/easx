Bulk loader currently comprised of sql files from the eas/database/sql/bulk_loader directory.

see job "1.1-avs_load" in this version of config_jobs.py: https://bitbucket.org/sfgovdt/sfeas_config/src/be5679c7732d/etl/src/config/live/config_jobs.py

Processing Outline for bulkloader:

1. Format and save input data: 
	C:\apps\eas_automation\app_data\data\bulkload_shapefile\bulkload.shp
	C:\apps\eas_automation\app_data\data\bulkload_data.csv

2. Execute job 'stage_bulkload_shapefile' and/or 'stage_bulkload_csv'
3. Init job 'bulkload'
4. Execute job 'bulkload'

	bulk_loader.load
		bulk_loader.process_address_base
			bulk_loader.find_address_base

			if address_extract record already has a geometery: #in other words _geocode == False 
				find_street_segment_by_geometry

			else:
				if service address:
					bulk_loader.geocode_street_address
						bulk_loader.find_street_segment_by_number

				else:
					bulk_loader.find_nearest_street_1
						bulk_loader.find_nearest_street_2
							bulk_loader.find_nearest_street_3  
			public._eas_insert_base_address

		bulk_loader.process_address_unit
			public._eas_find_equiv_unit_address
			public._eas_insert_unit_address
		bulk_loader.process_address_parcel_link

select public._eas_validate_addresses_after_etl('ALL');
vacuum analyze;

*Assumptions

	1. Input csv or shapefile must use unabbreviated street type as used in f_find_street_segment		
	2. Input shapefile bears the following schema:

	    field_name,data_type,length
	    OBJECTID,Integer,9
	    block,String,80
	    lot,String,80
	    unit,String,80
	    unit_sfx,String,80
	    st_num,Integer,4
	    st_num_sfx,String,20
	    st_name,String,32
	    st_suffix,String,10
	    st_type,String,4
	    source,String,32
	    st_prefix,String,10

	3. Input csv bears the following schema:

		field_name,data_type
		block,String
		lot,String
		unit,String
		unit_sfx,String
		street_number,String
		street_number_sfx,String
		street_name,String
		street_sfx,String
		street_type,String

