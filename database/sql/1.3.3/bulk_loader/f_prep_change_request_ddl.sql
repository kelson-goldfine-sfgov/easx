-- DROP FUNCTION IF EXISTS bulkloader.prep_change_request();

CREATE OR REPLACE FUNCTION bulkloader.prep_change_request()
  RETURNS text AS
$BODY$
DECLARE

BEGIN

    -- initialize the change request rows so we can do inserts (need these for FK constraints)
    INSERT INTO cr_change_requests(
                change_request_id, concurrency_id, requestor_user_id, requestor_comment, 
                reviewer_user_id, reviewer_comment, review_status, resolve_tms, 
                reviewer_last_update, "name", requestor_last_update)
        VALUES (nextval('cr_change_requests_change_request_id_seq'), 1, 1, 'bulk load cr change request', 
                1, 'bulk load cr change request', 1, now(), 
                now(), 'bulk load cr change request', now());

    INSERT INTO change_requests(
                change_request_id, cr_change_request_id, requestor_user_id, requestor_comment, 
                reviewer_user_id, reviewer_comment, create_tms)
        VALUES (nextval('change_requests_change_request_id_seq'), currval('cr_change_requests_change_request_id_seq'), 1, 'bulk load change request', 
                1, 'bulk load change request', now());

    UPDATE bulkloader.metadata set value = currval('change_requests_change_request_id_seq') WHERE key = 'change_request_id';

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION bulkloader.prep_change_request() OWNER TO postgres;
