
-- DROP FUNCTION IF EXISTS bulkloader.init_streets_nearest();

CREATE OR REPLACE FUNCTION bulkloader.init_streets_nearest()
  RETURNS text AS
$BODY$
DECLARE

BEGIN


    delete from bulkloader.streets_nearest;

    insert into bulkloader.streets_nearest (parcel_id, street_segment_id, distance)
    select ppts.parcel_id, ss.street_segment_id, st_distance(ppts.geometry, ss.geometry)
    from bulkloader.parcel_points ppts
    inner join public.street_segments ss on st_dwithin(ppts.geometry, ss.geometry, 500);

    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION bulkloader.init_streets_nearest() OWNER TO postgres;
