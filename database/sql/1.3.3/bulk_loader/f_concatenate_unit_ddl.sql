
DROP FUNCTION IF EXISTS bulkloader.concatenate_unit(
    _src_unit character varying(10),
    _src_unit_sfx character varying(10)
);


CREATE OR REPLACE FUNCTION bulkloader.concatenate_unit(
    _src_unit character varying(10),
    _src_unit_sfx character varying(10)
)
  RETURNS text AS
$BODY$
DECLARE

    _eas_unit_num character varying(21);

BEGIN

    _src_unit = coalesce(_src_unit, '');
    _src_unit = trim(both ' ' from _src_unit);

    _src_unit_sfx = coalesce(_src_unit_sfx, '');
    _src_unit_sfx = trim(both ' ' from _src_unit_sfx);

    _eas_unit_num = _src_unit || ' ' || _src_unit_sfx;
    _eas_unit_num = trim(both ' ' from _eas_unit_num);
    _eas_unit_num = nullif(_eas_unit_num, '');

    return _eas_unit_num;

END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION bulkloader.concatenate_unit(
    _src_unit character varying(10),
    _src_unit_sfx character varying(10)
) OWNER TO postgres;
