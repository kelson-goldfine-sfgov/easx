--isolate base addresses in a view
CREATE OR REPLACE VIEW vw_addrs_base AS
SELECT
    a.unq_adds_id,
    a.addr_number,
    a.addr_unit,
    a.cnn_final,
    a.addr_zip_spatial
FROM unq_adds a
WHERE 1=1
AND a.geometry IS NOT NULL
AND a.addr_unit = '-1'
AND a.cnn_final is not null;

--isolate unit addresses in a view
CREATE OR REPLACE VIEW vw_addrs_unit AS
SELECT
    a.unq_adds_id,
    a.addr_number,
    a.addr_unit,
    a.cnn_final,
    a.addr_zip_spatial
FROM vw_unq_adds_to_process a
WHERE 1=1
AND a.addr_unit != '-1'
AND a.cnn_final is not null;

--***identifies the unq_add records that are a unit of a base address***
--in some cases, a unit address was being assigned to multiple base addresses.
--Code below forces assigning each unit address to only one base address.
--It chooses the candidate base address with the lowest unq_adds_id value
--becuase that is the one most likely to be in EAS already.
DROP TABLE base_unit;

CREATE TABLE base_unit as
--first create base_unit relationships where everything joins perfectly (including zipcode)
SELECT
    min(a.unq_adds_id) as base_unq_add_id,
    b.unq_adds_id as unit_unq_add_id
FROM vw_addrs_base a
LEFT JOIN
    vw_addrs_unit b on 
            a.addr_number = b.addr_number
    and     a.cnn_final = b.cnn_final
    and     a.addr_zip_spatial = b.addr_zip_spatial
WHERE 1=1 
AND a.unq_adds_id is not null
AND b.unq_adds_id is not null 
GROUP BY b.unq_adds_id;

--second, pick up stragglers where the zipcode does not match but addr_number and cnn do match.  
--Result of zipcode boundaries changing between ETL zero and successive ETL's
insert into base_unit
    select *
    FROM (
        SELECT min(a.unq_adds_id) as base_unq_add_id, b.unq_adds_id as unit_unq_add_id
        FROM vw_addrs_base a
        LEFT JOIN
            vw_addrs_unit b on a.addr_number = b.addr_number
            AND a.cnn_final = b.cnn_final
        WHERE 1=1
        AND a.unq_adds_id is not null
        AND b.unq_adds_id is not null
        GROUP BY b.unq_adds_id) foo
WHERE (base_unq_add_id, unit_unq_add_id) not in (select base_unq_add_id, unit_unq_add_id from base_unit);

commit;
-- This result of this query should be no records if base_unit is populated correctly.
--select a.* ,
--b.*
--from base_unit a, (select 
--count(*) as cnt,
--unit_unq_add_id
--from base_unit 
--group by unit_unq_add_id
--having count(*) > 1
--order by count(*) desc) b
--where
--a.unit_unq_add_id = b.unit_unq_add_id


--update all unq_adds records that are units of a base address
UPDATE vw_unq_adds_to_process a
SET subaddr_to = (
    SELECT min(b.base_unq_add_id)
    FROM base_unit b
    WHERE a.unq_adds_id = b.unit_unq_add_id
)
WHERE subaddr_to is null;
commit;    


--where subaddr_to is null, then it must be the base address itself
UPDATE vw_unq_adds_to_process
set subaddr_to = unq_adds_id
where 1=1
AND subaddr_to is null
AND CNN_FINAL is not null;
  
commit;  
