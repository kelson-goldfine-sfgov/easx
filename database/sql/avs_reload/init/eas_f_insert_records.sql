
-- DROP FUNCTION IF EXISTS avs_reload.insert_records();

CREATE OR REPLACE FUNCTION avs_reload.insert_records()
  RETURNS text AS
$BODY$
DECLARE

    -- The Big Picture
    -- loop through the base addresses
    --    if dupe, get PK of dup
    --    else get PK and insert
    --    insert units using PK (separate function)
    --
    -- This entire procedure should be treated as a single transaction by the calling process.
    --
    -- When processing the source tables:
    --      avs_reload.addr_inserts
    --      avs_reload.addr_retires
    --      avs_reload.axp_inserts
    --      avs_reload.axp_retires
    -- we use a field called "exception_text" to...
    --      exclude rows from insertion and from further processing
    --      record exceptions for that row for subsequent reporting
    -- and we use a field called "success_text" to...
    --      simplify reporting
    --      help to assert that things are working properly
    --
    -- todo - add uniqueness constraints to unq_adds_id in EAS itself (we rows have non-unique values [nulls])
    -- todo - should we do much maintenance here, it would be very nice to follow a naming convention...even for something as mundane as block - lot; I mean really.

    _address_base_id    integer;
    _address_base_ids   integer[];
    _address_id         integer;
    _unit_address_ids   integer[];
    _addr_number        integer;
    _addr_unit          varchar(10);
    _addr_streetname    character varying(100);
    _addr_streetsuffix  character varying(14);
    _addr_zip_spatial   character varying(14);
    _zone_id            double precision;
    _zipcode            character varying(10);
    _geometry           geometry;
    _unq_adds_id        int;
    _unq_adds_id_special    int;
    _subaddr_to_id      integer;
    _street_segment_id  integer;
    _matched_streetname character varying(100);
    _matched_streetsuffix  character varying(14);
    _current_tms        timestamp without time zone = now();
    _block_lot          character varying(10);
    _new_ids            integer[];

BEGIN

    ----- base address logic in one place
    update avs_reload.addr_inserts ai
    set addr_base_flg = false;

    update avs_reload.addr_inserts ai
    set addr_base_flg = true
    where addr_unit = '-1';
    -----


    -----
    declare cursorBaseAddress cursor for
        select 
            ai.addr_number::int,
            nullif(ai.addr_unit, '-1'),
            ai.addr_streetname, 
            nullif(ai.addr_streetsuffix, '[NULL]'), -- you gotta love this
            st_transform(ai.geometry, 2227),
            ai.unq_adds_id::int,
            ai.zone_id::int,
            ss.street_segment_id,
            sn.base_street_name, 
            st.unabbreviated
        from avs_reload.addr_inserts as ai
        left outer join street_segments as ss on ss.seg_cnn::int = ai.cnn_final::int
        left outer join streetnames as sn on (sn.street_segment_id = ss.street_segment_id and sn.category = 'MAP')
        left outer join d_street_type st on sn.street_type = st.abbreviated
        where 1=1
        and ai.addr_base_flg = true
        and ai.exception_text is null
        and ai.success_text is null;
    begin
        open cursorBaseAddress;
        loop
            fetch cursorBaseAddress into _addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, _geometry, _unq_adds_id, _zone_id, _street_segment_id, _matched_streetname, _matched_streetsuffix;
            if not found then
                exit;
            end if;
            BEGIN

                -- If we matched a street segment, use its name and suffix because that will be more accurate than what might have come from AVS.
                if _street_segment_id is not null then
                    _addr_streetname = _matched_streetname;
                    _addr_streetsuffix = _matched_streetsuffix;
                end if;
               
                -- If an equivelent record already exists, we will attach everything else to that.
                -- In this case the addr_insert row does not need a either street segment reference or a geometry...because we will already have that in the existing record.
                -- If no equivelent record already exists, we will insert a new one.
                -- In this case the addr_insert row must have a valid street segment reference and a geometry.
                _address_base_id = null;
                select into _address_base_ids public._eas_find_equiv_base_address_smart(_addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, null);

                if array_upper(_address_base_ids, 1) is null then
                    -- no equivalent found - the nominal path
                    select into _new_ids public._eas_insert_base_address( null::text, _addr_number::int, _addr_unit::text, _zone_id::int, _geometry, _street_segment_id::int, _current_tms::timestamp without time zone, _unq_adds_id::int);
                    _address_base_id = _new_ids[1];
                    _address_id = _new_ids[2];
                    perform avs_reload.insert_axps(_unq_adds_id, _address_id, _current_tms, 'INSERT', null);
                    update avs_reload.addr_inserts set success_text = 'inserted base address' where unq_adds_id = _unq_adds_id;
                else
                    -- equivalent found
                    if array_upper(_address_base_ids, 1) > 1 then
                        raise exception 'multiple equivalent base address records already exist';
                    end if;
                    _address_base_id = _address_base_ids[1];
                    select address_id into _address_id from addresses where address_base_id = _address_base_id and address_base_flg = true;
                    update avs_reload.addr_inserts set success_text = 'equivalent base address' where unq_adds_id = _unq_adds_id;
                    perform avs_reload.insert_axps(_unq_adds_id, _address_id, _current_tms, 'INSERT', null);
                end if;

                if _address_base_id is null then
                    raise exception 'programming error - no base address found or created';
                end if;

                -- The base-unit relationship is represented by the fields unq_adds_id and subaddr_to.
                -- A sub_addr_to means "is a subaddress to..." and points to the parents unq_adds_id value.
                _subaddr_to_id = _unq_adds_id;
                perform avs_reload.insert_unit_addresses(_address_base_id, _subaddr_to_id, _current_tms, 'INSERT', null);

            EXCEPTION
                when others then
                    update avs_reload.addr_inserts set exception_text = substring(SQLERRM from 1 for 256) where unq_adds_id = _unq_adds_id;
                    -- cascade exceptions to unit addresses and AXPs
                    perform avs_reload.insert_unit_addresses(_address_base_id, _subaddr_to_id, _current_tms, 'EXCEPT', 'parent row insert failed');

            END;
        end loop;
        close cursorBaseAddress;
    end;
    -----


    ----- special unit address inserts 
    --- These have base address rows in EAS but not in the addr_inserts table.
    /*
    declare cursorBaseAddressEas cursor for
        select distinct
            ab.address_base_id, 
            ai1.subaddr_to
        from avs_reload.addr_inserts ai1
        join public.address_base ab on ab.unq_adds_id = ai1.subaddr_to::int
        where 1=1
        and ai1.addr_base_flg = false
        and ai1.exception_text is null
        and ai1.success_text is null
        and ai1.subaddr_to::int not in (
            select distinct ai2.unq_adds_id::int
            from avs_reload.addr_inserts ai2
        );
    begin
        open cursorBaseAddressEas;
        loop
            fetch cursorBaseAddressEas into _address_base_id, _subaddr_to_id;
            if not found then
                exit;
            end if;
            BEGIN

                perform avs_reload.insert_unit_addresses(_address_base_id, _subaddr_to_id, _current_tms, 'INSERT', null);

            EXCEPTION

                when others then
                    perform avs_reload.insert_unit_addresses(_address_base_id, _subaddr_to_id, _current_tms, 'EXCEPT', 'unable to insert unit address');

            END;
        end loop;
        close cursorBaseAddressEas;
    end;
    */
    -----


    ----- more special unit address inserts 
    --- This set of rows
    --    cannot be linked to base address rows in EAS with the primary key scheme
    --    do not have a parent base address row in addr_inserts table
    -- xxx
    declare cursorEquivalentAddress cursor for
        select 
            ai.addr_number::int,
            nullif(ai.addr_unit, '-1'),
            ai.addr_streetname, 
            nullif(ai.addr_streetsuffix, '[NULL]'), -- you gotta love this
            st_transform(ai.geometry, 2227),
            ai.unq_adds_id::int,
            ai.zone_id::int,
            ss.street_segment_id,
            sn.base_street_name, 
            st.unabbreviated
        from avs_reload.addr_inserts as ai
        left outer join street_segments as ss on ss.seg_cnn::int = ai.cnn_final::int
        left outer join streetnames as sn on (sn.street_segment_id = ss.street_segment_id and sn.category = 'MAP')
        left outer join d_street_type st on sn.street_type = st.abbreviated
        where 1=1
        and ai.addr_base_flg = false
        and ai.exception_text is null
        and ai.success_text is null;
    begin
        open cursorEquivalentAddress;
        loop
            fetch cursorEquivalentAddress into _addr_number, _addr_unit, _addr_streetname, _addr_streetsuffix, _geometry, _unq_adds_id, _zone_id, _street_segment_id, _matched_streetname, _matched_streetsuffix;
            if not found then
                exit;
            end if;
            BEGIN

                _address_base_id = null;

                -- If we matched a street segment, use its name and suffix because that will be more accurate than what might have come from AVS.
                if _street_segment_id is not null then
                    _addr_streetname = _matched_streetname;
                    _addr_streetsuffix = _matched_streetsuffix;
                end if;

                select into _address_base_ids public._eas_find_equiv_base_address_smart(_addr_number, null, _addr_streetname,  _addr_streetsuffix, null);

                if array_upper(_address_base_ids, 1) is null then
                    -- no equivalent base address found
                    -- Derive a base address from the unit address.
                    select into _new_ids public._eas_insert_base_address( null::text, _addr_number::int, null, _zone_id::int, _geometry, _street_segment_id::int, _current_tms::timestamp without time zone, null);
                    _address_base_id = _new_ids[1];
                    _address_id = _new_ids[2];
                    -- There should be no AXPs for the base address since we had no base address.
                else
                    -- equivalent base address found
                    if array_upper(_address_base_ids, 1) > 1 then
                        raise exception 'multiple equivalent parent base address records exist';
                    end if;
                    _address_base_id = _address_base_ids[1];
                end if;

                if _address_base_id is null then
                    raise exception 'programming error - no base address found or created';
                end if;

                perform avs_reload.insert_unit_address(_unq_adds_id, _addr_unit, _address_base_id, _current_tms, 'INSERT', null::text);

            EXCEPTION
                when others then
                    perform avs_reload.insert_unit_address(_unq_adds_id, _addr_unit, _address_base_id, _current_tms, 'EXCEPT', substring(SQLERRM from 1 for 256));

            END;
        end loop;
        close cursorEquivalentAddress;
    end;
    -----

    ----- At this point we have lots of rows in axp_inserts that have no base or unit level parent row in addr_inserts.
    -- However, these rows may have a parent row already in EAS. Here we try to insert these under the best parent.
    -- We join the streetnames table because if the street name is not in this table there will be no way we can find an equivalent address.
    -- e.g. "0 pier"

    update avs_reload.axp_inserts axpi
    set exception_text = 'unable to insert - no matching street name'
    where not exists (
        select 1
        from public.streetnames sn
        where sn.base_street_name = axpi.addr_streetname 
    )
    and axpi.exception_text is null
    and axpi.success_text is null;    

    declare cursorAxpInsertStragglers cursor for
        select distinct
            axpi.addr_number::int,
            nullif(axpi.addr_unit, '-1') as addr_unit,
            axpi.addr_streetname, 
            axpi.unq_adds_id::int,
            axpi.blklot
        from avs_reload.axp_inserts as axpi
        inner join streetnames as sn on (sn.base_street_name = axpi.addr_streetname)
        where 1=1
        and axpi.exception_text is null
        and axpi.success_text is null;    
    begin
        open cursorAxpInsertStragglers;
        loop
            fetch cursorAxpInsertStragglers into _addr_number, _addr_unit, _addr_streetname, _unq_adds_id, _block_lot;
            if not found then
                exit;
            end if;
            BEGIN

                -- get the base address info
                select into _address_base_ids public._eas_find_equiv_base_address_smart(_addr_number, _addr_unit, _addr_streetname, null::text, null::text);
                if array_upper(_address_base_ids, 1) is null then
                    -- no equivalent found
                    update avs_reload.axp_inserts set exception_text = 'unable to insert address parcel link - no parent base address found' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
                    continue;
                else
                    -- equivalent found
                    if array_upper(_address_base_ids, 1) > 1 then
                        update avs_reload.axp_inserts set exception_text = 'unable to insert address parcel link - multiple parent base addresses found' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
                        continue;
                    end if;
                    _address_base_id = _address_base_ids[1];
                end if;

                if _addr_unit is null then
                    -- AXP should be attached to base address.
                    select address_id into _address_id from addresses where address_base_id = _address_base_id and address_base_flg = true;
                else
                    -- AXP should be attached to unit address.
                    select into _unit_address_ids public._eas_find_equiv_unit_address(_address_base_id::int, _addr_unit::text);
                    if array_upper(_unit_address_ids, 1) is null then
                        -- no equivalent records found
                        update avs_reload.axp_inserts set exception_text = 'unable to insert address parcel link - no parent unit address found' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
                        continue;
                    else
                        -- equivalent found
                        if array_upper(_unit_address_ids, 1) > 1 then
                            update avs_reload.axp_inserts set exception_text = 'unable to insert address parcel link - multiple parent unit addresses found' where unq_adds_id = _unq_adds_id and blklot = _block_lot;
                            continue;
                        end if;
                        _address_id = _unit_address_ids[1];
                    end if;
                end if;

                perform avs_reload.insert_axp(_unq_adds_id, _block_lot, _address_id, _current_tms, 'INSERT', null);

            EXCEPTION
                when others then
                    perform avs_reload.insert_axp(_unq_adds_id, _block_lot, _address_id, _current_tms, 'EXCEPT', 'parent row insert failed');

            END;
        end loop;
        close cursorAxpInsertStragglers;
    end;
    -----


    return 'success!';


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs_reload.insert_records() OWNER TO postgres;
