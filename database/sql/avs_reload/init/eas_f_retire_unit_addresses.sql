-- Function: avs_reload.retire_unit_addresses()

DROP FUNCTION IF EXISTS avs_reload.retire_unit_addresses(
    _address_base_id int,
    _current_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION avs_reload.retire_unit_addresses(
    _address_base_id int,
    _current_tms timestamp without time zone
)
RETURNS void AS
$BODY$
DECLARE 
    _address_id integer;
BEGIN


    declare cursorUnitAddress cursor for
        select a.address_id
        from public.addresses a
        where 1=1
        and a.address_base_id = _address_base_id;
    begin

        open cursorUnitAddress;
        loop
            fetch cursorUnitAddress into _address_id;
            if not found then
                exit;
            end if;

                perform avs_reload.retire_unit_address(_address_id, _current_tms);

        end loop;
        close cursorUnitAddress;

    end;


END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs_reload.retire_unit_addresses(
    _address_base_id int,
    _current_tms timestamp without time zone
)
OWNER TO postgres;
