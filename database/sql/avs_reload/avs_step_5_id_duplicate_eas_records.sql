


select a.address_base_id, b.*,  c.addresses_count, _sfmad_distance_to_segment(a.address_base_id) as dist_to_segment
FROM
  address_base a, 
	(select 
	  count(*) as count,
	  base_address_prefix ,
	  base_address_num ,
	  base_address_suffix,
	  retire_tms ,
	  zone_id ,
	  a.street_segment_id,
	  b.full_street_name
	FROM address_base a, streetnames b
	WHERE 1=1
	  AND a.street_segment_id = b.street_segment_id
	  AND b.category = 'MAP'
	GROUP BY   
	  base_address_prefix ,
	  base_address_num ,
	  base_address_suffix ,
	  retire_tms ,
	  zone_id ,
	  a.street_segment_id,
	  b.full_street_name
	HAVING count(*) > 1 
	) b,
	(select 
	count(*) as addresses_count,
	address_base_id
	from addresses
	group by address_base_id) c
	
WHERE 1=1
  AND coalesce(a.base_address_prefix, '-1') = coalesce(b.base_address_prefix, '-1')
  AND a.base_address_num = b.base_address_num
  AND coalesce(a.base_address_suffix, '-1') = coalesce(b.base_address_suffix, '-1')
  AND a.zone_id = b.zone_id
  AND a.street_segment_id = b.street_segment_id 
  AND a.address_base_id = c.address_base_id
order by count, base_address_num, full_street_name, addresses_count desc, dist_to_segment