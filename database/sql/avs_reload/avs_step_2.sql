
-- step 2: source data pre-processing and standardization


----- BEGIN - metadata and spatial index
update user_sdo_geom_metadata
set diminfo =
  mdsys.sdo_dim_array(
  mdsys.sdo_dim_element('x', -2147483648,2147483647,0.00005),
  mdsys.sdo_dim_element('y', -2147483648,2147483647,0.00005))
where table_name in ('TBL_STREETS_ALL', 'PARCELS_PT', 'PARCELS_POLY');
commit;

-- select count(*) from tbl_streets_all;
-- select count(*) from tbl_streets_all where geometry is null;

drop index TBL_STREETS_ALL_SIDX;
drop index TBL_STREETS_ALL_IDX1;
drop index TBL_STREETS_ALL_IDX2;

create index TBL_STREETS_ALL_SIDX on TBL_STREETS_ALL (geometry) indextype is mdsys.spatial_index;
create index TBL_STREETS_ALL_IDX1 on TBL_STREETS_ALL (streetname);
create index TBL_STREETS_ALL_IDX2 on TBL_STREETS_ALL (streettype);

----- END - metadata and spatial index


----- BEGIN - fill zip_spatial information for parcels_pt
-- select count(*) from parcels_pt;
-- select count(*) from parcels_poly;
drop index zones_sidx;
create index zones_sidx on zones(geometry) indextype is mdsys.spatial_index;


-- Row count is something like 869 becuase most of these rows are set in the FMW.
update parcels_pt a
set zip_spatial = (
  select z.z_zipcode 
  from zones z
  where SDO_NN(z.geometry, a.geometry, 'sdo_num_res=1') = 'TRUE'
)
WHERE zip_spatial is null;
COMMIT;
----- END - fill zip_spatial information for parcels_pt


----- BEGIN - clean up unit for src_dbiavs2

-- src_dbiavs2.id is stable

-- select count(*), unit
-- from src_dbiavs2
-- group by unit
-- order by unit --count(*);
--
-- select count(*) from src_dbiavs2;

-- I am no longer sure what the problem is here - commenting it out for now.
-- known misc issue - 1 row of bad data
-- select * from src_dbiavs2 where id between 465340 and 465360;
-- delete from src_dbiavs2 where id = 465349;
-- commit;


-- select count(*) from src_dbiavs2 where unit = '0';
-- n=52868
update src_dbiavs2
set unit = null
where unit = '0';
commit;
----- BEGIN - clean up unit


----- BEGIN - clean up end date
-- select count(*), end_date
-- from src_dbiavs2
-- where 1=1
-- group by end_date
-- order by end_date;
--
-- select distinct end_date from src_dbiavs2;
-- 
-- select count(*)
-- from src_dbiavs2
-- where end_date is null;
--
-- select trim(both ' ' from end_date)
-- from src_dbiavs2
-- where rownum < 10
-- and trim(both ' ' from end_date) = '';
--takes about a 1.5 minutes
update src_dbiavs2
set end_date = trim(both ' ' from end_date)
where end_date is not null;
commit;
----- END - clean up end date


-- select * from st_type_mapping;
-- select streettype, count(*) from tbl_streets_all group by streettype;
update tbl_streets_all a
set streettype = (
    select b.addr_suffix
    from st_type_mapping b
    where a.streettype = b.st_type
);
commit;




----- BEGIN - clean up street info in src_dbiavs2 and in tbl_street_all

-- select count(*) from src_dbiavs2;
-- select * from src_dbiavs2 where avs_street_type = 'BL';
-- select avs_street_type, count(*) from src_dbiavs2 group by avs_street_type;
update src_dbiavs2
set avs_street_type = 'BOULEVARD'
where avs_street_type = 'BL';
commit;

-- 'Octavia' misspelled with a zero
-- Must run before the "leading zero processing".
-- select *
-- from src_dbiavs2
-- where avs_street_name = '0CTAVIA';
update src_dbiavs2
set avs_street_name = 'OCTAVIA'
where avs_street_name = '0CTAVIA';
commit;


-- when streetname ends in ' ST', remove it from streetname and put 'STREET' into the suffix
-- select * from src_dbiavs2 where trim(avs_street_name) like '% ST';
update src_dbiavs2
set avs_street_name = rtrim(avs_street_name, ' ST'),
    avs_street_type = 'STREET'
where trim(avs_street_name) like '% ST';
commit;


-- when streetname ends in ' CR', remove it from streetname and put 'CIRCLE' into the suffix
-- select * from src_dbiavs2 where trim(avs_street_name) like '% CR';
-- select * from src_dbiavs2 where trim(avs_street_name) = 'CIRCLE';
update src_dbiavs2
set avs_street_name = rtrim(avs_street_name, ' CR'),
    avs_street_type = 'CIRCLE'
where trim(avs_street_name) like '% CR';
commit;

-- when streetname ends in ' PLACE', remove it from streetname and put 'PLACE' into the suffix
-- select * from src_dbiavs2 where trim(avs_street_name) like '% PLACE';
-- select * from src_dbiavs2 where trim(avs_street_type) = 'PLACE';
update src_dbiavs2
set avs_street_name = rtrim(avs_street_name, ' PLACE'),
    avs_street_type = 'PLACE'
where trim(avs_street_name) like '% PLACE';
commit;

-- when streetname ends in ' AVE', remove it from streetname and put 'AVENUE' into the suffix
-- select * from src_dbiavs2 where trim(avs_street_name) like '% AVE';
update src_dbiavs2
set avs_street_name = rtrim(avs_street_name, ' AVE'),
    avs_street_type = 'AVENUE'
where trim(avs_street_name) like '% AVE';
commit;

-- remove periods
-- select * from src_dbiavs2 where instr(avs_street_name, '.') > 0;
update src_dbiavs2
set avs_street_name = replace(avs_street_name, '.')
where instr(avs_street_name, '.') > 0;
commit;

update tbl_streets_all
set streetname = replace(streetname, '.')
where instr(streetname, '.') > 0;
commit;

-- remove dashes
-- select * from src_dbiavs2 where avs_street_name like '%-%';
update src_dbiavs2
set avs_street_name = replace(avs_street_name, '-')
where avs_street_name like '%-%';
commit;

update tbl_streets_all
set streetname = replace(streetname, '-')
where streetname like '%-%';
commit;


-- remove apostrophes
-- select * from src_dbiavs2 where avs_street_name like '%''%';
update src_dbiavs2
set avs_street_name = replace(avs_street_name, '''')
where avs_street_name like '%''%';
commit;

update tbl_streets_all
set streetname = replace(streetname, '''')
where streetname like '%''%';
commit;

-- strip leading zeros
-- select * from src_dbiavs2 where trim(avs_street_name) like '0%';
update src_dbiavs2
set avs_street_name = ltrim(avs_street_name, '0')
where avs_street_name like '0%';
commit;

update tbl_streets_all
set streetname = ltrim(streetname, '0')
where streetname like '0%';
commit;


----- remove T.I. and TI from avs_street_name
-- select count(*) from tbl_streets_all where streetname like '% - TI';
-- select count(*) from src_dbiavs2 where avs_street_name like '% - TI';
update src_dbiavs2
set avs_street_name = rtrim(avs_street_name, '- TI')
where avs_street_name like '% - TI';
commit;

update tbl_streets_all
set streetname = rtrim(streetname, '- TI')
where streetname like '% - TI';
commit;

update src_dbiavs2
set avs_street_name = rtrim(avs_street_name, '(T.I.)')
where avs_street_name like '%(T.I.)';
commit;

update tbl_streets_all
set streetname = rtrim(streetname, '(T.I.)')
where streetname like '%(T.I.)';
commit;

update src_dbiavs2
set avs_street_name = trim(both ' ' from avs_street_name);
commit;

update tbl_streets_all
set streetname = trim(both ' ' from streetname);
commit;

-- mulitple consecutive white spaces
update src_dbiavs2
set avs_street_name = REGEXP_REPLACE(avs_street_name, '( ){2,}', ' '),
    avs_street_type = REGEXP_REPLACE(avs_street_type, '( ){2,}', ' ');
commit;

update tbl_streets_all
set streetname = REGEXP_REPLACE(streetname, '( ){2,}', ' '),
    streettype = REGEXP_REPLACE(streettype, '( ){2,}', ' ');
commit;


----- clean up avs_street_type
-- select count(*), avs_street_type from src_dbiavs2 group by avs_street_type;
update src_dbiavs2
set avs_street_type = null
where avs_street_type = 'NO STREET SUFFIX';
commit;
----- END clean up streets



----- BEGIN Create and populate src_dbiavs2.zip_spatial

-- select count(*), zip_spatial
-- from SRC_DBIAVS2
-- group by zip_spatial;
--
-- select *
-- from src_dbiavs2
-- where not exists (
--
-- );

-- populate src_dbiavs2 with zip_spatial based on values in related parcels
-- Without these indexes, the update takes a very long time.

DROP INDEX PARCELS_PT_BLKLOT_IDX;
CREATE INDEX PARCELS_PT_BLKLOT_IDX ON PARCELS_PT (BLK_LOT);

DROP INDEX SRC_DBIAVS2_BLOCKLOT_IDX;
CREATE INDEX SRC_DBIAVS2_BLOCKLOT_IDX ON SRC_DBIAVS2 (BLOCKLOT);

update src_dbiavs2 a
set a.zip_spatial = (
  select min(b.zip_spatial)
  from parcels_pt b
  where a.blocklot = b.BLK_LOT
);
commit;
----- END Create and populate src_dbiavs2.zip_spatial