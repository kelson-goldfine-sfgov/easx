-- View: vw_address_search

-- DROP VIEW vw_address_search;

CREATE OR REPLACE VIEW vw_address_search AS
 SELECT b.address_id, a.geometry, a.address_base_id, a.base_address_prefix AS base_prefix, a.base_address_num, a.base_address_suffix AS base_suffix, b.unit_num, c.full_street_name, c.category, d.zipcode, d.jurisdiction
   FROM address_base a, addresses b, streetnames c, zones d
  WHERE b.retire_tms IS NULL AND a.address_base_id = b.address_base_id AND a.street_segment_id = c.street_segment_id AND a.zone_id = d.zone_id;

ALTER TABLE vw_address_search OWNER TO postgres;

