
-- DROP FUNCTION _eas_streetnames_before() cascade;

CREATE FUNCTION _eas_streetnames_before() RETURNS trigger AS $_eas_streetnames_before$
DECLARE
BEGIN

    /* We assume that the following uniqueness constraint is in place.

    CREATE UNIQUE INDEX streetnames_unq_category
        ON streetnames ( seg_cnn)
        WHERE category = 'MAP';

    */


    IF (TG_OP = 'DELETE') THEN

        -- To date, we have not deleted street name rows.
        -- This block is in case we do.
        perform 1 from streetnames where seg_cnn = OLD.seg_cnn and category <> 'MAP' and OLD.category = 'MAP';
        IF FOUND THEN
            RAISE EXCEPTION 'deletion of this streetnames MAP row is not allowed in this case because NON-MAP rows exist - seg_cnn: %', OLD.seg_cnn::text;
        END IF;
        RETURN OLD;

    ELSIF (TG_OP = 'UPDATE') THEN

        IF OLD.category = 'MAP' and NEW.category <> 'MAP' THEN
            RAISE EXCEPTION 'update of this streetnames MAP row is not allowed in this case because the update would leave the street segment with no MAP row - seg_cnn: %', OLD.seg_cnn::text ;
        END IF;

    ELSE
        -- INSERT
        IF NEW.category = 'ALIAS' THEN
            perform 1 from streetnames where seg_cnn = NEW.seg_cnn and category = 'MAP';
            IF NOT FOUND THEN
                RAISE EXCEPTION 'insertion of this streetnames ALIAS row is not allowed in this case because no MAP rows exists - seg_cnn: %', NEW.seg_cnn::text;
            END IF;
        END IF;


    END IF;

    RETURN NEW;

  END;
$_eas_streetnames_before$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_streetnames_before BEFORE INSERT OR UPDATE OR DELETE ON streetnames
    FOR EACH ROW EXECUTE PROCEDURE _eas_streetnames_before();


/*  testing

select * from streetnames where 1=1 order by seg_cnn limit 100;
select * from streetnames where seg_cnn = 100000;
select * from streetnames where seg_cnn = 100000 and category <> 'MAP';

-- exceptions
delete from streetnames where seg_cnn = 100000 and category = 'MAP';
update streetnames set category = 'ALIAS' where seg_cnn = 100000 and category = 'MAP';

-- destructive testing
delete from streetnames where seg_cnn = 100000 and category = 'ALIAS';
delete from streetnames where seg_cnn = 100000 and category = 'MAP';
delete from streetnames where seg_cnn = 100000;

INSERT INTO streetnames(
            streetname_id, seg_cnn, bsm_streetnameid, prefix_direction, prefix_type, 
            base_street_name, street_type, suffix_direction, suffix_type, 
            full_street_name, category, street_segment_id, create_tms)
    VALUES (42582, 100000, null, null, null, '01ST', 'ST', null, null, '01ST ST', 'MAP', 6445, '2009-09-01 15:39:38.717');

INSERT INTO streetnames(
            streetname_id, seg_cnn, bsm_streetnameid, prefix_direction, prefix_type, 
            base_street_name, street_type, suffix_direction, suffix_type, 
            full_street_name, category, street_segment_id, create_tms)
    VALUES (42582, 100000, null, null, null, '01ST', 'ST', null, null, '01ST ST', 'ALIAS', 6445, '2009-09-01 15:39:38.717');

*/
