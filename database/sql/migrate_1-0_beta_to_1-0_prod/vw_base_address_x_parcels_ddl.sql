
-- View: vw_base_address_x_parcels

-- DROP VIEW vw_base_address_x_parcels;

-- A convenient and fast way to get the parcels for an address; used in search.

CREATE OR REPLACE VIEW vw_base_address_x_parcels AS

    SELECT distinct
        p.parcel_id,
        p.map_blk_lot,
        p.blk_lot,
        p.block_num,
        p.lot_num,
        p.date_rec_add,
        p.date_rec_drop,
        p.date_map_add,
        p.date_map_drop,
        p.date_map_alt,
        p.project_id_add,
        p.project_id_drop,
        p.project_id_alt,
        p.exception_notes,
        p.geometry,
        p.create_tms,
        p.update_tms,
        ab.address_base_id
    FROM address_base ab,
        addresses a, 
        address_x_parcels axp,
        parcels p
    WHERE 1 = 1 
    AND ab.address_base_id = a.address_base_id 
    AND a.address_id = axp.address_id
    AND axp.parcel_id = p.parcel_id
    AND ab.retire_tms IS NULL
    AND a.retire_tms IS NULL
    AND axp.retire_tms IS NULL;

ALTER TABLE vw_base_address_x_parcels OWNER TO postgres;
