-- View: vw_address_reviews

-- DROP VIEW vw_address_reviews;

CREATE OR REPLACE VIEW vw_address_reviews AS
 SELECT ar.address_review_id, ar.create_tms, ar.resolution_tms, ar.last_update_tms, ar.last_update_user_id, ar.concurrency_id,
        CASE
            WHEN ar.resolution_tms IS NULL THEN false
            ELSE true
        END AS resolved_flg, ar.message, dart.review_type_desc, ab.address_base_id, ab.base_address_prefix, ab.base_address_num, ab.base_address_suffix, ab.geometry, sn.full_street_name, a.address_id, a.unit_num, a.address_base_flg, 0 AS sort_order
   FROM address_review ar, d_address_review_types dart, addresses a, address_base ab, streetnames sn
  WHERE 1 = 1 AND ar.address_id = a.address_id AND a.address_base_id = ab.address_base_id AND sn.category::text = 'MAP'::text AND ab.street_segment_id = sn.street_segment_id AND dart.review_type_id = ar.review_type_id
  ORDER BY sn.full_street_name, ab.base_address_num, ab.base_address_prefix, ab.base_address_suffix, a.address_base_flg DESC, a.unit_num, ar.create_tms;

ALTER TABLE vw_address_reviews OWNER TO postgres;

