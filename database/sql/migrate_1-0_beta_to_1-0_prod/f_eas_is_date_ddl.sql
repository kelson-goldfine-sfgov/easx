
DROP FUNCTION IF EXISTS public._eas_is_date(date_string character varying(128));

CREATE OR REPLACE FUNCTION public._eas_is_date(date_string character varying(128)) RETURNS Boolean AS
$BODY$
DECLARE
begin
      if date_string is null then
         return false;
      else
         perform date_string::date;
         return true;
      end if;
    exception when others then
        return false;
end
$BODY$
  LANGUAGE 'plpgsql' VOLATILE
  COST 100;
ALTER FUNCTION public._eas_is_date(character varying(128)) OWNER TO postgres;

