
-- DROP FUNCTION _eas_address_x_parcels_after() cascade;

CREATE FUNCTION _eas_address_x_parcels_after() RETURNS trigger AS $_eas_address_x_parcels_after$
DECLARE
    _row_count integer = 0;
    _action character(10) = null;
    _address_number integer;
    _full_street_name character varying(64);
    _block_lot character varying(10);
    _message character varying(128);
    REVIEW_TYPE_PARCEL_RETIRED int = 1; -- parcel is retired
BEGIN

    IF (TG_OP = 'UPDATE') THEN

        _action = 'retire';

        -- If there is an unresolved row in address_review,
        -- and it is parcel retirement type,
        -- and there are no links to retired parcels
         -- then resolve the address_review row.
        UPDATE address_review
        SET resolution_tms = NEW.retire_tms,
            last_update_tms = NEW.retire_tms,
            last_update_user_id = 1,
            concurrency_id = concurrency_id + 1
        WHERE address_id = NEW.address_id
        and resolution_tms is null
        and review_type_id = REVIEW_TYPE_PARCEL_RETIRED
        and not exists (
            select 1
            from address_x_parcels axp,
                parcels p
            where axp.address_id = NEW.address_id
            and axp.parcel_id = p.parcel_id
            and axp.retire_tms is null          -- there is a link
            and p.date_map_drop is not null     -- the parcel is retired
        );

    ELSE
        _action = 'insert';
    END IF;

    INSERT INTO address_x_parcels_history(
        address_x_parcel_id,
        parcel_id,
        address_id,
        confidence_score,
        activate_change_request_id,
        retire_change_request_id,
        create_tms,
        last_change_tms,
        retire_tms,
        history_action
    )
    SELECT
        NEW.id,
        NEW.parcel_id,
        NEW.address_id,
        NEW.confidence_score,
        NEW.activate_change_request_id,
        NEW.retire_change_request_id,
        NEW.create_tms,
        NEW.last_change_tms,
        NEW.retire_tms,
        _action;

    RETURN NEW;

  END;
$_eas_address_x_parcels_after$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_address_x_parcels_after AFTER INSERT OR UPDATE ON address_x_parcels
    FOR EACH ROW EXECUTE PROCEDURE _eas_address_x_parcels_after();

