-- Table: addresses_history

-- DROP TABLE addresses_history cascade;

CREATE TABLE addresses_history
(
  id serial NOT NULL,
  address_id integer NOT NULL,
  address_base_id integer NOT NULL,
  unit_type_id integer,
  floor_id integer,
  unit_num character varying(20),
  create_tms timestamp without time zone,
  retire_tms timestamp without time zone,
  retire_change_request_id integer,
  activate_change_request_id integer,
  concurrency_id serial NOT NULL,
  mailable_flg boolean,
  disposition_code integer,
  address_base_flg boolean NOT NULL,
  unq_adds_id integer,
  update_change_request_id integer,
  last_change_tms timestamp without time zone,
  history_action character varying(10) NOT NULL,
  CONSTRAINT "addresses_history_PK" PRIMARY KEY (id),
  CONSTRAINT "addresses_history_FK_address_disposition" FOREIGN KEY (disposition_code)
      REFERENCES d_address_disposition (disposition_code) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (OIDS=FALSE);
ALTER TABLE addresses_history OWNER TO postgres;

CREATE INDEX "addresses_history_IDX_last_change_tms"
  ON addresses_history
  USING btree (last_change_tms);
