
-- DROP FUNCTION _eas_addresses_after() cascade;

CREATE FUNCTION _eas_addresses_after() RETURNS trigger AS $_eas_addresses_after$
    DECLARE
        _action         character(10) = null;
    BEGIN

        -- Some updates do not change data.
        -- http://sfgovdt.jira.com/browse/MAD-66

        IF TG_OP = 'UPDATE' THEN
            IF NEW.retire_tms IS NOT NULL THEN
                _action = 'retire';
            ELSIF NEW.mailable_flg != OLD.mailable_flg or NEW.disposition_code != OLD.disposition_code THEN
                _action = 'update';
            ELSE
                _action = 'no change';
            END IF;
        ELSE
            _action = 'insert';
        END IF;

        INSERT INTO addresses_history(
            address_id,
            address_base_id,
            unit_type_id,
            floor_id,
            unit_num,
            create_tms,
            retire_tms,
            retire_change_request_id,
            activate_change_request_id,
            concurrency_id,
            mailable_flg,
            disposition_code,
            address_base_flg,
            unq_adds_id,
            update_change_request_id,
            last_change_tms,
            history_action
        )
        SELECT
            NEW.address_id,
            NEW.address_base_id,
            NEW.unit_type_id,
            NEW.floor_id,
            NEW.unit_num,
            NEW.create_tms,
            NEW.retire_tms,
            NEW.retire_change_request_id,
            NEW.activate_change_request_id,
            NEW.concurrency_id,
            NEW.mailable_flg,
            NEW.disposition_code,
            NEW.address_base_flg,
            NEW.unq_adds_id,
            NEW.update_change_request_id,
            NEW.last_change_tms,
            _action;

        RETURN NEW;

    END;
$_eas_addresses_after$ LANGUAGE plpgsql;

CREATE TRIGGER _eas_addresses_after AFTER INSERT OR UPDATE ON addresses
    FOR EACH ROW EXECUTE PROCEDURE _eas_addresses_after();
