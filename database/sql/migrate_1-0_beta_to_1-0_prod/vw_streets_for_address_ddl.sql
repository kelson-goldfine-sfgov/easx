-- View: vw_streets_for_address

-- DROP VIEW vw_streets_for_address;

CREATE OR REPLACE VIEW vw_streets_for_address AS 
SELECT 
    ss.street_segment_id, 
    ss.geometry, 
    ss.date_dropped,
    sn.full_street_name || ' (' || LEAST(sar.right_from_address, sar.right_to_address, sar.left_from_address, sar.left_to_address) || ' - ' || GREATEST(sar.right_from_address, sar.right_to_address, sar.left_from_address, sar.left_to_address) || ')' AS description
FROM
      street_segments ss,
      streetnames sn,
      street_address_ranges sar
WHERE ss.street_segment_id = sn.street_segment_id
AND ss.street_segment_id = sar.street_segment_id
AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_streets_for_address OWNER TO postgres;
