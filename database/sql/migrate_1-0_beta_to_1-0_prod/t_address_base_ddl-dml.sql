
ALTER TABLE address_base
   ALTER COLUMN street_segment_id SET NOT NULL;

ALTER TABLE address_base
   ADD COLUMN last_change_tms timestamp without time zone;

CREATE INDEX address_base_IDX_last_change_tms
  ON address_base
  USING btree (last_change_tms);

-- Default to date of initial ETL.
update address_base set last_change_tms = '2009-09-01 16:39:13.599';

ALTER TABLE address_base add CONSTRAINT "address_base_NN_last_change_tms" CHECK (last_change_tms IS NOT NULL);

CREATE INDEX "address_base_IDX_base_address_num"
   ON address_base USING btree (base_address_num);
    

CREATE INDEX address_base_idx_unq_adds_id
  ON public.address_base
  USING btree
  (unq_adds_id);