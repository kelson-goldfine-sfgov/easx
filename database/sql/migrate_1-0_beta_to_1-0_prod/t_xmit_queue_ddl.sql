-- Table: xmit_queue

-- DROP TABLE xmit_queue;

CREATE TABLE xmit_queue
(
  id serial NOT NULL,
  address_base_history_id integer NOT NULL,
  address_history_id integer,
  address_x_parcel_history_id integer,
  last_change_tms timestamp without time zone NOT NULL,
  xmit_tms timestamp without time zone,
  sort_order int NOT NULL,
  CONSTRAINT "xmit_queue_PK" PRIMARY KEY (id),

  CONSTRAINT "xmit_queue_FK_address_base_history_id" FOREIGN KEY (address_base_history_id)
      REFERENCES address_base_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT "xmit_queue_FK_address_history_id" FOREIGN KEY (address_history_id)
      REFERENCES addresses_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT "xmit_queue_FK_address_x_parcel_history_id" FOREIGN KEY (address_x_parcel_history_id)
      REFERENCES address_x_parcels_history (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,

  CONSTRAINT "xmit_queue_UNQ"
    UNIQUE (address_base_history_id, address_history_id, address_x_parcel_history_id, last_change_tms)

)
WITH (OIDS=FALSE);
ALTER TABLE xmit_queue OWNER TO postgres;

CREATE INDEX "xmit_queue_IDX_xmit_tms"
   ON xmit_queue USING btree (xmit_tms);

CREATE INDEX "xmit_queue_IDX_address_base_history_id"
   ON xmit_queue USING btree (address_base_history_id);

CREATE INDEX "xmit_queue_IDX_address_history_id"
   ON xmit_queue USING btree (address_history_id);

CREATE INDEX "xmit_queue_IDX_address_x_parcel_history_id"
   ON xmit_queue USING btree (address_x_parcel_history_id);

CREATE INDEX "xmit_queue_IDX_last_change_tms"
   ON xmit_queue USING btree (last_change_tms);
