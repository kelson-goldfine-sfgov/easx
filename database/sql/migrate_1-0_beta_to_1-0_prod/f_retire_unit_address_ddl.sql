-- Function: _eas_retire_unit_address()

DROP FUNCTION IF EXISTS _eas_retire_unit_address(
    _address_id int,
    _current_tms timestamp without time zone
);

CREATE OR REPLACE FUNCTION _eas_retire_unit_address(
    _address_id int,
    _current_tms timestamp without time zone
)
RETURNS void AS
$BODY$
DECLARE
BEGIN

    update address_x_parcels
    set
        retire_change_request_id = 0,
        last_change_tms = _current_tms,
        retire_tms = _current_tms
    where address_id = _address_id;

    update addresses
    set
        retire_tms = _current_tms,
        retire_change_request_id = 0,
        update_change_request_id = 0
    where address_id = _address_id;

END;$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION _eas_retire_unit_address(
    _address_id int,
    _current_tms timestamp without time zone
)
OWNER TO postgres;
