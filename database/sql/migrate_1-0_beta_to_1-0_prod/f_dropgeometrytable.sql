-- Function: public.dropgeometrytable(character varying, character varying, character varying)

-- DROP FUNCTION public.dropgeometrytable(character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION public.dropgeometrytable(character varying, character varying, character varying)
  RETURNS text AS
$BODY$
DECLARE
	catalog_name alias for $1; 
	schema_name alias for $2;
	table_name alias for $3;
	real_schema name;

BEGIN


	IF ( schema_name = '' ) THEN
		SELECT current_schema() into real_schema;
	ELSE
		real_schema = schema_name;
	END IF;


	-- Remove refs from geometry_columns table
	EXECUTE 'DELETE FROM geometry_columns WHERE ' ||

		'f_table_schema = ' || quote_literal(real_schema) ||
		' AND ' ||

		' f_table_name = ' || quote_literal(table_name);
	
	-- Remove table 
	EXECUTE 'DROP TABLE '

		|| quote_ident(real_schema) || '.' ||

		quote_ident(table_name);

	RETURN

		real_schema || '.' ||

		table_name ||' dropped.';
	
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE STRICT
  COST 100;
ALTER FUNCTION public.dropgeometrytable(character varying, character varying, character varying) OWNER TO postgres;


-- Function: public.dropgeometrytable(character varying, character varying)

-- DROP FUNCTION public.dropgeometrytable(character varying, character varying);

CREATE OR REPLACE FUNCTION public.dropgeometrytable(character varying, character varying)
  RETURNS text AS
$BODY$SELECT DropGeometryTable('',$1,$2)$BODY$
  LANGUAGE 'sql' VOLATILE STRICT
  COST 100;
ALTER FUNCTION public.dropgeometrytable(character varying, character varying) OWNER TO postgres;


-- Function: public.dropgeometrytable(character varying)

-- DROP FUNCTION public.dropgeometrytable(character varying);

CREATE OR REPLACE FUNCTION public.dropgeometrytable(character varying)
  RETURNS text AS
$BODY$SELECT DropGeometryTable('','',$1)$BODY$
  LANGUAGE 'sql' VOLATILE STRICT
  COST 100;
ALTER FUNCTION public.dropgeometrytable(character varying) OWNER TO postgres;
