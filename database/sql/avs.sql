
update avs set exception_text = null;

-- is street number suffix valid?
-- 51 rows
update avs
set exception_text = 'invalid street number suffix'
where avs.street_number_sfx is not null
and not exists (
    select 1
    from d_address_base_number_suffix abns 
    where abns.suffix_value = avs.street_number_sfx
)
and avs.exception_text is null;


-- does street name exist?
-- 7827 rows
update avs
set exception_text = 'street name does not exist'
where avs.avs_street_name not in (
    select snd.base_street_name
    from vw_streetnames_distinct snd
)
and avs.exception_text is null;


-- does street suffix exist?
-- 3423 rows
update avs
set exception_text = 'street suffix does not exist'
where avs.avs_street_type is not null
and avs.avs_street_type not in (
    select snd.unabbreviated_suffix
    from vw_streetnames_distinct snd
    where snd.unabbreviated_suffix is not null
)
and avs.exception_text is null;


-- does street and street suffix combination exist? (non null suffix)
-- 907 rows
update avs
set exception_text = 'street - street suffix combination does not exist (non null suffix)'
where avs.avs_street_type is not null
and (avs.avs_street_name, avs.avs_street_type) not in (
    select snd.base_street_name, snd.unabbreviated_suffix
    from vw_streetnames_distinct snd
    where snd.unabbreviated_suffix is not null
)
and avs.exception_text is null;


-- does street and street suffix combination exist? (null suffix)
-- 0 rows
update avs
set exception_text = 'street - street suffix combination does not exist (null suffix)'
where avs.avs_street_type is null
and avs.avs_street_name not in (
    select snd.base_street_name
    from vw_streetnames_distinct snd
    where snd.unabbreviated_suffix is null
)
and avs.exception_text is null;


-- are block lot columns consistent?
-- 1 row
update avs
set exception_text = 'block lot values are inconsistent'
where avs.blocklot <> (block || lot)
and avs.exception_text is null;


-- is block and lot valid?
-- 3765 rows
update avs
set exception_text = 'no matching block - lot'
where avs.blocklot not in (
    select p.blk_lot
    from parcels p
)
and avs.exception_text is null;

-- is unit specification valid?
-- 1 row
update avs
set exception_text = 'length of unit num exceeds 10'
where length(unit::text || unit_sfx) > 10
and avs.exception_text is null;


----- is date specification valid?
-- lots of rows
update avs
set end_date = null
where end_date = '';

-- 0 rows
update avs
set exception_text = 'invalid end date value'
where end_date is not null 
and _eas_is_date(end_date) = false;
-----


/*

select * from avs limit 100;
select count(*), avs_street_status from avs group by avs_street_status;
select count(*), address_kind from avs group by address_kind;
select count(*), address_type from avs group by address_type;

select * 
from avs
where avs.avs_street_name not in (
    select snd.base_street_name
    from vw_streetnames_distinct snd
)
and avs.exception_text is null;

select * from d_address_base_number_suffix;
select * from avs where exception_text = 'street name does not exist' limit 100;
select * from streetnames limit 100
select * from vw_streetnames_distinct where base_street_name like '%QUESADA%';
select * from d_street_type;
select distinct avs_street_sfx, avs_street_type from avs;

select count(*), exception_text from avs group by exception_text;
select 
    count(*), 
    CASE WHEN exception_text is null THEN true
    ELSE false
    END as load_flag
from avs
group by load_flag;

select 15975.0 / 325051.0;

select * from avs where exception_text is not null;




*/
