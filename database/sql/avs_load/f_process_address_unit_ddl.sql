
DROP FUNCTION IF EXISTS avs.process_address_unit(    
    _avs_id int,
    _create_tms timestamp without time zone
);


CREATE OR REPLACE FUNCTION avs.process_address_unit( 
    _avs_id int,
    _create_tms timestamp without time zone
)
  RETURNS void AS
$BODY$
DECLARE

    _row_count          int;
    _address_id         int;
    _address_ids        int[];
    _address_base_id    int;
    _std_unit           character varying(10);
    _street_name        character varying(64);
    _disposition_code   int;

BEGIN

    select 
        address_base_id,
        avsa.std_unit,
        avsa.avs_street_name
    into
        _address_base_id,
        _std_unit,
        _street_name
    from avs.avs_addresses avsa
    where id = _avs_id;

    GET DIAGNOSTICS _row_count = ROW_COUNT;
    if _row_count != 1 then
        raise exception 'expected one row but got %', _row_count::text;
    end if;

    if _street_name = 'UNKNOWN' then
        -- disposition is "placeholder"
        _disposition_code := 2;
    else
        -- disposition is "official"
        _disposition_code = 1;
    end if;

    if _address_base_id is null then
        raise exception 'expected non-null adress_base_id but it was null';
    end if;

    if _std_unit is null then
        return;
    end if;


    ----- First we look for a unit address that matches the AVS address.
    _address_id = null;
    select into _address_ids public._eas_find_equiv_unit_address(_address_base_id, _std_unit);
    if array_upper(_address_ids, 1) is not null then
        -- one or more match found
        if array_upper(_address_ids, 1) > 1 then
            -- multiple matches found
            update avs.avs_addresses set exception_text = 'multiple duplicate unit addresses exist' where id = _avs_id;
            return;
        end if;
        -- single match found
        _address_id = _address_ids[1];
        update avs.avs_addresses set address_id = _address_id where id = _avs_id;
        return;
    end if;


    -- At this point we know there is no matching unit address.

    -- domain values
    -- unit type 0: other
    -- floor 105: 'unknown'
    -- disposition code 1: official
    select into _address_id public._eas_insert_unit_address(_address_base_id, _std_unit, 0, 105, _disposition_code, null, 1, _create_tms);

    update avs.avs_addresses set address_id = _address_id where id = _avs_id;

    insert into public.address_sources (eas_id, eas_table, source_id, source_system) values(_address_id, 'addresses', _avs_id, 'AVS');



END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE STRICT
COST 100;
ALTER FUNCTION avs.process_address_unit(
    _avs_id int,
    _create_tms timestamp without time zone
) OWNER TO postgres;
