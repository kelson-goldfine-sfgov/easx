
select blocklot, block, lot
from avs.avs_addresses
where exception_text = 'block lot values are inconsistent';

select *
from avs.avs_addresses
where 1=2
or blocklot like '0000%'
or blocklot like '9999%'
or block like '0000%'
or block like '9999%'

update avs.addresses
set 
    block = '0000',
    lot = '000'
where blocklot = '0000000'
and block = '9999'
and lot = '999';


select * 
from avs.vw_load_results 
where exception_text is not null
and exception_text like 'insert rejected - address_base - duplicate un-retired row was found%'

select avs.find_address_base(
    0,
    null,
    'ALVORD',
    'STREET',
    175686
);



and exception_text = 'multiple matching base address records already exist'
)
and exception_text not in (
    select 'insert rejected - address_x_parcels - address geometry is not contained by the specified parcel'
    union
    select 'referenced parcel has no geometry'
    union
    select 'no matching block - lot'
);




select *
from address
-- select avs.delete_addresses();
-- vacuum analyze;
-- select avs.init_parcel_points();
-- select avs.init_streets_nearest();
-- select avs.init_avs_addresses();
--> AvsLoadCommand
-- select public._eas_validate_addresses_after_etl('ALL');
-- vacuum analyze;

select * from address_x_parcels limit 10
select * from parcels limit 10

select * from address_review where 1=1 limit 10;
select count(*) from address_review where 1=1;

select avs.load(0, 100000, True);

select count(*), std_end_date::date
from avs.avs_addresses 
group by std_end_date::date
order by std_end_date::date;

select count(*) from addresses;
select count(*) from address_base;


select * from avs.vw_load_summary;

-- After a successful run of the avs.load() this should be zero.
select count(*) from avs.avs_addresses where exception_text  is null and address_base_id is null;
select * from avs.avs_addresses where exception_text  is not null and address_base_id is not null and exception_text not like 'insert rejected - address_x_parcels%';
select * from avs.avs_addresses where exception_text  like '%your street suffix is appears to be incorrect%'
select * from avs.avs_addresses where exception_text  like '%unable to find nearby street segment%'
select * from streetnames where base_street_name like '%EDGAR%'
select * from streetnames where street_segment_id in (12086, 408);



select * from avs.avs_addresses where avs_street_name = 'WAWONA' and street_number = 701;
select * from avs.avs_addresses where block = '0000' or block = '9999';



select '15-OCT-07'::date;

select count(*), avs_street_name
from avs.avs_addresses where exception_text  = 'street name does not exist'
group by avs_street_name
order by count desc;

"update addresses set avs_street_sfx = ' || _street_type_abbreviated || ' where id = 62153"
"update addresses set avs_street_sfx = 'CT' where id = 58645"

select std_end_date::date
from avs.avs_addresses
where std_end_date is not null
order by std_end_date::date;

select *
from address_x_parcels 
where id = 240643;


select * 
from address_x_parcels 
where id in (
    240643,
    240781,
    240795,
    240631,
    242498
)



declare
    _sql character varying(1024);
begin
    _sql = 'update addresses set avs_street_sfx = ' || 'poo' 
    select _sql;
end

select * 
from avs.avs_addresses
where 1=1
and avs_street_name = 'PRESIDIO'
and std_street_type = 'AVENUE'
and street_number = 1


select avs.find_nearest_street('JACKSON', 'STREET', false, (select parcel_id from parcels where blk_lot = '0192001'), '');

select * from street_segments where street_segment_id = 10007
select * from street_segments where street_segment_id = 8943

select * from streetnames where street_segment_id = 10007
select * from streetnames where base_street_name = 'JAMES'
select * from streetnames where base_street_name = 'JACKSON'

JACKSON
STREET
select *
from avs.vw_load_exceptions
where exception_text like 'unable to insert%';

select * from avs.vw_load_summary;


select avs.find_nearest_street_fancy(
    107024,
    'BALBOA',
    'STREET',
    (select parcel_id from parcels where blk_lot = '1617033B'),
    null
);

select avs.find_nearest_street('BALBOA', 'STREET', false, (select parcel_id from parcels where blk_lot = '1617033B'), null);
select avs.find_nearest_street('BALBOA', 'STREET', true, (select parcel_id from parcels where blk_lot = '1617033B'), null);


-- select avs.load(0, 1000);

select * 
from avs.vw_load_summary;

select count(*) from avs.avs_addresses


----- find nearest street

select 
    avsa.avs_street_name, 
    avsa.std_street_type,
    avsa.blocklot, 
    (select parcel_id from parcels where blk_lot = avsa.blocklot) as parcel_id,
    avsa.std_end_date,
    avs.find_nearest_street(avsa.avs_street_name, avsa.std_street_type, (select parcel_id from parcels where blk_lot = avsa.blocklot), avsa.std_end_date)
from avs.avs_addresses avsa
where id = 297763
order by street_number, avs_street_name, std_street_type, COALESCE(street_number_sfx, ''), COALESCE(unit, ''), COALESCE(unit_sfx, ''), blocklot;

select parcel_id from parcels "5421003"
select avs.find_nearest_street('BANCROFT', 'AVENUE', 152646, null);
select avs.find_nearest_street('14TH', 'AVENUE', 86704, null);
select avs.find_nearest_street('06TH', 'STREET', 121271, null);
select avs.find_nearest_street('OLYMPIA', 'WAY', 102024, null);
-----


select
  *,
  (case
    when avsa.address_type = 'ASSESSOR' then 1
    when avsa.address_type = 'DBI' then 2
    when avsa.address_type = 'MULTIUNIT' then 3
    when avsa.address_type = 'XREFERENCE' then 4
    when avsa.address_type = 'USER' then 5
    when avsa.address_type = 'THREER' then 6
    when avsa.address_type = 'UNKNOWN' then 7
    else 8
  end) as address_type_sort_order,
  ''::text, ''::text, ''::text
from avs.avs_addresses avsa
order by avsa.std_end_date::date asc, address_type_sort_order, avsa.id
offset 20001
limit 20000;


----- sets that have at least one row end_date'd
select *, ''::text, ''::text, ''::text
from avs.avs_addresses avsa1
where exists (
    select 1 
    from avs.avs_addresses avsa2
    where avsa2.end_date is not null
    and avsa1.street_number = avsa2.street_number
    and avsa1.avs_street_name = avsa2.avs_street_name
)
order by avsa1.street_number, avsa1.avs_street_name, avsa1.std_street_type, COALESCE(avsa1.street_number_sfx, ''), COALESCE(avsa1.unit, ''), COALESCE(avsa1.unit_sfx, ''), avsa1.blocklot
limit 25000;
-----

----- 'street - street suffix combination does not exist (null suffix)'
select *, ''::text, ''::text, ''::text
from avs.avs_addresses avsa1
where exists (
    select 1 
    from avs.avs_addresses avsa2
    where avsa2.exception_text = 'street - street suffix combination does not exist (null suffix)'
    and avsa1.street_number = avsa2.street_number
    and avsa1.avs_street_name = avsa2.avs_street_name
)
order by
    avsa1.street_number, avsa1.avs_street_name, avsa1.std_street_type, COALESCE(avsa1.street_number_sfx, ''), COALESCE(avsa1.unit, ''), COALESCE(avsa1.unit_sfx, ''), avsa1.blocklot;

select * 
from vw_streetnames_distinct
where 1=1
and base_street_name like '%LOYOLA%'

-----



----- 'multiple matching base address records already exist'
select *, ''::text, ''::text, ''::text
from avs.avs_addresses avsa1
where exists (
    select 1 
    from avs.avs_addresses avsa2
    where avsa2.exception_text = 'multiple matching base address records already exist'
    and avsa1.street_number = avsa2.street_number
    and avsa1.avs_street_name = avsa2.avs_street_name
)
order by
    avsa1.street_number, avsa1.avs_street_name, avsa1.std_street_type, COALESCE(avsa1.street_number_sfx, ''), COALESCE(avsa1.unit, ''), COALESCE(avsa1.unit_sfx, ''), avsa1.blocklot;

select avs.find_address_base(15, null, 'ASHBURY', 'TERRACE');
select avs.find_address_base(15, null, 'PIEDMONT', 'STREET');
-----



----- 'no matching nearby street segment'
select *, ''::text, ''::text, ''::text
from avs.avs_addresses avsa1
where exists (
    select 1 
    from avs.avs_addresses avsa2
    where avsa2.exception_text = 'no matching nearby street segment'
    and avsa1.street_number = avsa2.street_number
    and avsa1.avs_street_name = avsa2.avs_street_name
)
order by
    avsa1.street_number, avsa1.avs_street_name, avsa1.std_street_type, COALESCE(avsa1.street_number_sfx, ''), COALESCE(avsa1.unit, ''), COALESCE(avsa1.unit_sfx, ''), avsa1.blocklot;
-----




select 12
from avs.avs_addresses 
where street_number = 1 
and avs_street_name = 'BLUXOME'
order by unit, blocklot



select substring(exception_text from 1 for 95), count(*)
from avs.avs_addresses
where exception_text is not null
group by substring(exception_text from 1 for 95);


select std_end_date, std_end_date::timestamp without time zone from avs.avs_addresses where id = 95067

select avs.find_address_base(0, 'V', 'ALVORD', 'STREET');

select * from d_street_type where unabbreviated = 'PARKWAY'

select *
from avs.avs_addresses avsa
where 1=1
and id = 156196
and avsa.std_street_type is not null
and avsa.std_street_type not in (
    select snd.unabbreviated_suffix
    from vw_streetnames_distinct snd
    where snd.unabbreviated_suffix is not null
)




where 

select public._eas_find_equiv_unit_address(229209, '107')

select distinct a.address_id
from addresses a
where a.unit_num = '107'
and a.address_base_id = 229209
and a.address_base_flg = false
and a.retire_tms is null



    _unit_num character varying(10),
    out _address_ids integer[]
);




select avs.find_or_insert__address_unit(457936, now());


select * from parcels where blk_lot = '5501053'

select *
from avs.avs_addresses avsa
where 1=1
--and avsa.exception_text is null
--and 
order by
    avsa.street_number,
    avsa.street_number_sfx,
    avsa.avs_street_name,
    avsa.std_street_type,
    avsa.unit,
    avsa.unit_sfx
limit 1000;


select * from parcels where blk_lot = '2792004'
select * from avs.avs_addresses where id = 295891 and street_number_sfx is null
select * from address_base where address_base_id = 225596 and base_address_suffix is null

----- find_base_address BEGIN
select avs.find_address_base(0, '', 'PACIFIC', 'AVENUE');

    0;"";"PACIFIC";"AV";"AVENUE";"V";"0";"";"";"AVENUE"

    select distinct ab.address_base_id
    from public.address_base ab
    inner join public.streetnames sn on ab.street_segment_id = sn.street_segment_id
    left outer join public.d_street_type st on sn.street_type = st.abbreviated
    where ab.base_address_num = 0
    and sn.base_street_name = 'PACIFIC'
    and ab.retire_tms is null
    and (
        ('AVENUE' is null and st.unabbreviated is null)
        or
        ('AVENUE' is not null and coalesce(st.unabbreviated, '') = 'AVENUE')
    )
    and (
        (null is null and ab.base_address_suffix is null)
        or
        (null is not null and coalesce(ab.base_address_suffix, '') = null)
    )


----- find_base_address END


select * from address_base
select * from addresses


select avs.init_avs_addresses();

update avs.avs_addresses
set street_segment_id = null
where street_segment_id is not null

update avs.avs_addresses
set exception_text = null
where exception_text is not null

update avs.avs_addresses
set address_base_id = null
where address_base_id is not null

update avs.avs_addresses
set address_id = null
where address_id is not null

update avs.avs_addresses
set address_x_parcel_id = null
where address_x_parcel_id is not null


select * from streetnames where base_street_name = 'BANCROFT'



        select 
            avsa.id,
            avsa.blocklot,
            avsa.street_number,
            avsa.street_number_sfx,
            avsa.avs_street_name,
            avsa.unit,
            avsa.unit_sfx,
            avsa.end_date,
            avsa.std_street_type,
            pp.geometry,
            pp.parcel_id
        from avs.avs_addresses avsa
        inner join public.parcels p on avsa.blocklot = p.blk_lot
        inner join avs.parcel_points pp on pp.parcel_id = p.parcel_id
        inner join avs.streets_nearest snear on snear.parcel_id = pp.parcel_id
        inner join public.streetnames snames on snames.street_segment_id = snear.street_segment_id
        inner join d_street_type st on snames.street_type = st.abbreviated
        where 1=1
        and avs.vw_streets_nearest snear1 on snear1.base_street_name = avsa.avs_street_name and coalesce(snear1.street_type, '') = coalesce(avsa.std_street_type, '') and snear1.parcel_id = pp.parcel_id

        --and snear1.distance = (
        --    select min(snear2.distance)
        --    from avs.vw_streets_nearest snear2
        --    where snear2.base_street_name = snear1.base_street_name
        --    and coalesce(snear2.street_type, '') = coalesce(snear1.street_type, '')
        --    and snear2.parcel_id = snear1.parcel_id
        --)
        and avsa.exception_text is null
        order by
            avsa.street_number,
            avsa.street_number_sfx,
            avsa.avs_street_name,
            avsa.std_street_type,
            avsa.unit,
            avsa.unit_sfx
        limit 1000;


        select 
            avsa.id,
            avsa.blocklot,
            avsa.street_number,
            avsa.street_number_sfx,
            avsa.avs_street_name,
            avsa.unit,
            avsa.unit_sfx,
            avsa.end_date,
            avsa.std_street_type,
            pp.geometry,
            pp.parcel_id
        from avs.avs_addresses avsa
        inner join public.parcels p on avsa.blocklot = p.blk_lot
        inner join avs.parcel_points pp on pp.parcel_id = p.parcel_id
        inner join avs.vw_streets_nearest snear1 on snear1.base_street_name = avsa.avs_street_name and coalesce(snear1.street_type, '') = coalesce(avsa.std_street_type, '') and snear1.parcel_id = pp.parcel_id
        and snear1.distance = (
            select min(snear2.distance)
            from avs.vw_streets_nearest snear2
            where snear2.base_street_name = snear1.base_street_name
            and coalesce(snear2.street_type, '') = coalesce(snear1.street_type, '')
            and snear2.parcel_id = snear1.parcel_id
        )
        where avsa.exception_text is null
        order by
            avsa.street_number,
            avsa.street_number_sfx,
            avsa.avs_street_name,
            avsa.std_street_type,
            avsa.unit,
            avsa.unit_sfx
        limit 1000;


        select 
            avsa.id,
            avsa.blocklot,
            avsa.street_number,
            avsa.street_number_sfx,
            avsa.avs_street_name,
            avsa.unit,
            avsa.unit_sfx,
            avsa.end_date,
            avsa.std_street_type,
            pp.geometry,
            pp.parcel_id
        from avs.avs_addresses avsa
        inner join public.parcels p on avsa.blocklot = p.blk_lot
        inner join avs.parcel_points pp on pp.parcel_id = p.parcel_id
        where avsa.exception_text is null
        order by
            avsa.street_number,
            avsa.street_number_sfx,
            avsa.avs_street_name,
            avsa.std_street_type,
            avsa.unit,
            avsa.unit_sfx
        limit 1000;













select * 
from streetnames
where 1=1
and street_segment_id in (
    select street_segment_id
    from streetnames
    where base_street_name = 'PIEDMONT'
)

limit 10











SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
    ab.unq_adds_id,
    a.address_base_flg,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	ab.zone_id,
	asewkt(ab.geometry),
	a.address_base_flg,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ss.st_name
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
--and sn.category = 'MAP'
and sn.base_street_name like ( '%UNKNOWN%')
--and street_type = 'ST'
--and ab.base_address_num = 20
--and a.address_id = 102984
--and ab.address_base_id = 102984
--and ab.unq_adds_id in (249654)
--and p.blk_lot = '4937018'
--and a.unit_num = '103'
-- and contains((select geometry from parcels where blk_lot = '4937018'), ab.geometry)
and axp.retire_tms is not null
order by
	ss.st_name,
	ss.st_type,
	a.address_base_flg desc,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;

