
DROP FUNCTION IF EXISTS avs.find_address_base(
    _street_number int,
    _street_number_suffix char(1),
    _street_name character varying(64),
    _street_type character varying(32),
    _avs_id int
);

CREATE OR REPLACE FUNCTION avs.find_address_base(
    _street_number int,
    _street_number_suffix char(1),
    _street_name character varying(64),
    _street_type character varying(32),
    _avs_id int
)
  RETURNS int[] AS
$BODY$
DECLARE

    _address_base_ids int[];
    _index int;
    _category char(5);
    _categories text[] = Array['MAP','ALIAS']::Text[];

BEGIN

    -- patch for 
    -- http://code.google.com/p/eas/issues/detail?id=349
    -- Return a match of the street name matches and its in the right place based on the block lot.
    if _street_name = 'UNKNOWN' then
        select into _address_base_ids ARRAY(
            select distinct ab.address_base_id
            from public.address_base ab
            inner join public.streetnames sn on ab.street_segment_id = sn.street_segment_id
            inner join public.parcels p on contains(p.geometry, ab.geometry)
            where sn.category = 'MAP'
            and ab.base_address_num = _street_number
            and sn.base_street_name = _street_name
            and ab.retire_tms is null
            and p.blk_lot = (select blocklot from avs.avs_addresses where id = _avs_id)
        );
        if array_upper(_address_base_ids, 1) > 1 then
            -- We found more than one - in this case (UNKNOWN streets) we'll want to create a separate new base address.
            _address_base_ids := array[]::integer[];
        end if;
        return _address_base_ids;
    end if;

    -- Try the MAP category first. This uses the official street name.
    -- If that fails, try the alias.
    -- 99/100 the MAP category will succeed so it should be first.

    for _index in array_lower(_categories, 1)..array_upper(_categories, 1) loop
        
        _category = _categories[_index];

        select into _address_base_ids ARRAY(
            select distinct ab.address_base_id
            from public.address_base ab
            inner join public.streetnames sn on ab.street_segment_id = sn.street_segment_id
            left outer join public.d_street_type st on sn.street_type = st.abbreviated
            where sn.category = _category
            and ab.base_address_num = _street_number
            and sn.base_street_name = _street_name
            and ab.retire_tms is null
            and (
                (_street_type is null and st.unabbreviated is null)
                or
                (_street_type is not null and coalesce(st.unabbreviated, '') = _street_type)
            )
            and (
                (_street_number_suffix is null and ab.base_address_suffix is null)
                or
                (_street_number_suffix is not null and coalesce(ab.base_address_suffix, '') = _street_number_suffix)
            )
        );

        if array_upper(_address_base_ids, 1) is not null then
            return _address_base_ids;
        end if;

    end loop;


    return _address_base_ids;


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.find_address_base(
    _street_number int,
    _street_number_suffix char(1),
    _street_name character varying(64),
    _street_type character varying(32),
    _avs_id int
) OWNER TO postgres;
