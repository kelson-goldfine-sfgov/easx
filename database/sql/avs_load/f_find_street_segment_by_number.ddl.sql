-- Given a street number, name, and type, this function finds the appropriate
-- street segment.  If the particular number doesn't appear on any segment,
-- it returns 0, unless the flag "_ok_to_find_nearest" is set.  In that case,
-- it will return either the next higher (by street number) or next lower
-- street segment.

-- If it cannot find a street segment at all, it will return 0.

DROP FUNCTION IF EXISTS avs.find_street_segment_by_number(
    _street_number          int,
    _street_name            character varying(64),
    _street_type            character varying(32),
    _end_date               date,
    _ok_to_find_nearest     boolean,
    result_id               out int,
    on_left                 out boolean
);

CREATE OR REPLACE FUNCTION avs.find_street_segment_by_number(
    _street_number          int,
    _street_name            character varying(64),
    _street_type            character varying(32),
    _end_date               date,
    _ok_to_find_nearest     boolean,
    result_id               out int,
    on_left                 out boolean
) AS
$find_street_segment_by_number$
BEGIN

    -- We create a materialized view of all segments of the candidate street that
    -- have plausible street address numbers, and which are not expired based on
    -- the input end-date.
    
    drop table if exists street_segment_candidates;

    create temporary table street_segment_candidates
        on commit drop
        as select 
            street_segments.street_segment_id,
            l_f_add, 
            l_t_add,
            r_f_add,
            r_t_add,
            street_segments.date_dropped
            from street_segments
            join streetnames 
                on street_segments.street_segment_id = streetnames.street_segment_id
            join vw_street_type
                on vw_street_type.abbreviated = streetnames.street_type
            where base_street_name = _street_name
              and vw_street_type.unabbreviated = _street_type
              and coalesce(street_segments.date_dropped < _end_date, true)
              and l_f_add <> 0
              and l_t_add <> 0
              and r_f_add <> 0
              and r_t_add <> 0;
    
    -- Next, we try for an exact match: A segment that includes the input street number,
    -- on the correct side of the street.  (We assume here that a street is always
    -- odd on one side, even on the other, for the length of a segment.  Very SF-
    -- centric!)
          
    select
        street_segment_id,
        mod(_street_number, 2) = mod(l_f_add, 2)
        into result_id, on_left
        from street_segment_candidates
        where ( 
                (_street_number between l_f_add and l_t_add) and
                (mod(_street_number, 2) = mod(l_f_add, 2))
              ) or (
                (_street_number between r_f_add and r_t_add) and
                (mod(_street_number, 2) = mod(r_f_add, 2)) 
              )
        order by l_f_add asc, date_dropped desc nulls first
        limit 1;
    
    if found then
        return;
    end if;
    
    -- At this point, we bail out unless we've been told that simply a nearest
    -- segment is OK.
    
    if not _ok_to_find_nearest then
        result_id := null;
        return;
    end if;
    
    -- We now try to find a close-by segment.  We define the closest segment as the
    -- one nearest the input street number, by absolute street number.  There is just
    -- the barest possibility of a weird result here if a segment has a very different
    -- set of addresses on the left and right, but the odds and negative consequences
    -- are so small as to not be worth guarding against.
              
    select
        street_segment_id,
        mod(_street_number, 2) = mod(l_f_add, 2)
        into result_id, on_left
        from street_segment_candidates
        order by least(abs(_street_number-l_f_add),
                       abs(_street_number-l_t_add),
                       abs(_street_number-r_f_add),
                       abs(_street_number-r_t_add)),
            date_dropped desc nulls first
        limit 1;
    
    if found then
        return;
    else
        result_id := null;
        return;
    end if;
    
END;
$find_street_segment_by_number$
LANGUAGE 'plpgsql' VOLATILE;
