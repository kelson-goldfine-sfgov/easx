
DROP FUNCTION IF EXISTS avs.find_nearest_street_2(
    _avs_id         int,
    _street_name    character varying(64),
    _street_type    character varying(32),
    _parcel_id      int,
    _std_end_date   character varying(10),
    _category       character varying(5),
    out _street_segment_id int
);

CREATE OR REPLACE FUNCTION avs.find_nearest_street_2(
    _avs_id         int,
    _street_name    character varying(64),
    _street_type    character varying(32),
    _parcel_id      int,
    _std_end_date   character varying(10),
    _category       character varying(5),
    out _street_segment_id int
)
  RETURNS int AS
$BODY$
DECLARE

    _row_count int;
    _street_segment_id_1 int;
    _street_segment_id_2 int;
    _street_type_unabbreviated character varying(64);
    _exception_text character varying(256);

BEGIN

    if _street_name = 'UNKNOWN' then
        -- We generally do not use street_segments.st_name - it will be dropped from the data model.
        -- this is a shortcut - do not follow this example!
        select street_segment_id into _street_segment_id from street_segments where st_name = _street_name;
    else
        -- Do not use the street_type.
        select into _street_segment_id_1 avs.find_nearest_street_3(_street_name, _street_type, false, _parcel_id, _std_end_date, _category);
        if _street_segment_id_1 is not null then
            -- Use the street_type.
            select into _street_segment_id_2 avs.find_nearest_street_3(_street_name, _street_type, true, _parcel_id, _std_end_date, _category);

            -- Compare results.
            if _street_segment_id_1 = coalesce(_street_segment_id_2, 0) then
                _street_segment_id = _street_segment_id_1;
                return;
            end if;

            select street_type into _street_type_unabbreviated from avs.vw_streets_nearest where street_segment_id = _street_segment_id_1 and category = 'MAP' order by date_dropped desc nulls first limit 1;
            -- Using pipe character as a hack to support summary reporting.
            -- When we move to 9.2 use "GET STACKED DIAGNOSTICS".
            _exception_text = 'your street suffix appears to be incorrect | try ' || COALESCE(_street_type_unabbreviated, 'null (literal)');
            raise exception '%', _exception_text;

        end if;
    end if;


END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE
COST 100;
ALTER FUNCTION avs.find_nearest_street_2(
    _avs_id         int,
    _street_name    character varying(64),
    _street_type    character varying(32),
    _parcel_id      int,
    _std_end_date   character varying(10),
    _category       character varying(5),
    out _street_segment_id int
) OWNER TO postgres;
