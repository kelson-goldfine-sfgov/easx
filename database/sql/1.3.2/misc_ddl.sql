CREATE TABLE address_change_notification_history
(
  id serial NOT NULL,
  last_notification_tms timestamp without time zone NOT NULL
)
WITH (
  OIDS=FALSE
);
ALTER TABLE address_change_notification_history OWNER TO eas_dbo;