-- DROP VIEW vw_addresses_flat;
/*

select * from vw_addresses_flat limit 10;

The view id is to support use with desktop GIS such as qgis which requires a unique id. The id field is not a PK and the view cannot be edited.

*/
CREATE OR REPLACE VIEW vw_addresses_flat AS
SELECT
	row_number() over (
		order by
			ss.st_name,
			ss.st_type,
			ab.base_address_num,
			ab.base_address_suffix,
			a.unit_num,
			p.blk_lot	
	) as id,
	ab.address_base_id as eas_address_base_id,
	ab.base_address_num,
	ab.base_address_suffix,
	sn.base_street_name as street_name,
	sn.street_type as street_type,
	ss.seg_cnn as street_cnn,
	ab.create_tms as base_address_create_tms,
	ab.retire_tms as base_address_retire_tms,
	a.address_id as eas_unit_address_id,
	a.address_base_flg as unit_address_base_flg,
	a.unit_num as unit_address,
	a.create_tms as unit_address_create_tms,
	a.retire_tms as unit_address_retire_tms,
	dap.disposition_description,
	p.blk_lot as parcel_block_lot,
	axp.id as eas_address_x_parcel_id,
	axp.create_tms as address_x_parcel_create_tms,
	axp.retire_tms as address_x_parcel_retire_tms,
	z.zipcode,
        x(transform(ab.geometry, 4326)) as longitude,
	y(transform(ab.geometry, 4326)) as latitude,
	ab.geometry
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
inner join zones z on (z.zone_id = ab.zone_id)
inner join d_address_disposition dap on (a.disposition_code = dap.disposition_code)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	p.blk_lot;


ALTER TABLE vw_addresses_flat OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_addresses_flat TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_addresses_flat TO django;


INSERT INTO geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
select '', 'public', 'vw_addresses_flat', 'geometry', 2, 2227, 'POINT'
where not exists (
	select 1 from geometry_columns where f_table_schema = 'public'	and f_table_name = 'vw_addresses_flat'
);


GRANT SELECT ON TABLE public.vw_addresses_flat TO geoserver;
