-- View: v_base_address_search

DROP VIEW v_base_address_search;

CREATE OR REPLACE VIEW v_base_address_search AS
SELECT
  ab.address_base_id,
  ab.base_address_prefix,
  ab.base_address_num,
  ab.base_address_suffix,
  sn.base_street_name AS street_name,
  sn.street_type,
  sn.category AS street_category,
  sn.prefix_direction AS street_prefix_direction,
  sn.suffix_direction AS street_suffix_direction,
  sn.full_street_name,
  z.zipcode, 
  z.jurisdiction,
  ad.disposition_description::text as disposition,
  st_transform(ab.geometry, 900913) AS geometry,
  a.create_tms,
  a.last_change_tms,
  a.retire_tms
FROM
  address_base ab,
  addresses a,
  streetnames sn,
  zones z,
  d_address_disposition ad
WHERE 1 = 1
AND ab.address_base_id = a.address_base_id
AND a.retire_tms is null
AND a.address_base_flg = true
AND ab.zone_id = z.zone_id
AND ab.street_segment_id = sn.street_segment_id
AND a.disposition_code = ad.disposition_code;

ALTER TABLE v_base_address_search OWNER TO eas_dbo;
GRANT ALL ON TABLE v_base_address_search TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE v_base_address_search TO django;
