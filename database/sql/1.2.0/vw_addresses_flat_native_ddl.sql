-- View: vw_addresses_flat_native

DROP VIEW vw_addresses_flat_native;

CREATE OR REPLACE VIEW vw_addresses_flat_native AS
 SELECT (((ab.address_base_id::text || '-'::text) || a.address_id::text) || '-'::text) ||
        CASE
            WHEN axp.id IS NULL THEN '0'::text
            ELSE axp.id::text
        END AS id,
        ab.address_base_id,
        ab.base_address_num,
        ab.base_address_suffix,
        sn.base_street_name,
        sn.street_type,
        sn.post_direction,
        sn.full_street_name,
        ss.seg_cnn,
        ss.street_segment_id,
        ab.create_tms AS base_address_create_tms,
        ab.retire_tms AS base_address_retire_tms,
        a.address_id,
        a.address_base_flg,
        a.unit_num,
        a.unit_type_id,
        a.floor_id,
        a.disposition_code,
        dap.disposition_description,
        a.create_tms AS unit_address_create_tms,
        a.retire_tms AS unit_address_retire_tms,
        p.map_blk_lot,
        p.blk_lot,
        p.date_map_add AS parcel_date_map_add,
        p.date_map_drop AS parcel_date_map_drop,
        axp.id AS address_x_parcel_id,
        axp.create_tms AS address_x_parcel_create_tms,
        axp.retire_tms AS address_x_parcel_retire_tms,
        axp.activate_change_request_id AS address_x_parcel_activate_change_request_id,
        axp.retire_change_request_id AS address_x_parcel_retire_change_request_id,
        z.zipcode,
        ab.geometry
   FROM address_base ab
   JOIN addresses a ON ab.address_base_id = a.address_base_id
   JOIN streetnames sn ON ab.street_segment_id = sn.street_segment_id
   JOIN street_segments ss ON ab.street_segment_id = ss.street_segment_id
   JOIN zones z ON z.zone_id = ab.zone_id
   JOIN d_address_disposition dap ON a.disposition_code = dap.disposition_code
   LEFT JOIN address_x_parcels axp ON axp.address_id = a.address_id
   LEFT JOIN parcels p ON axp.parcel_id = p.parcel_id
  WHERE 1 = 1 AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_addresses_flat_native OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_addresses_flat_native TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_addresses_flat_native TO django;
GRANT SELECT ON TABLE vw_addresses_flat_native TO geoserver;
