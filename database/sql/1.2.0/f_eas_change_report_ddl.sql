
-- DROP FUNCTION _eas_change_report();
-- select _eas_change_report();
/*
	select * 
	from address_change_report  
	where 1=1
	order by
		base_address_num,
		base_address_suffix,
		full_street_name,
		blk_lot,
		activate_tms,
		retire_tms
	nulls last
	
*/


CREATE OR REPLACE FUNCTION _eas_change_report()
  RETURNS text AS
$BODY$
DECLARE

    _change_id int;
    _base_address_num int;
    _base_address_suffix character varying(10);
    _full_street_name character varying(64);
    _blk_lot character varying(9);
    _activate_change_request_id int;
    _retire_change_request_id int;

BEGIN

    truncate address_change_report;

    INSERT INTO address_change_report(
        base_address_num,
        base_address_suffix,
        full_street_name,
        address_base_flg,
        unit_num,
        blk_lot,
        activate_change_request_id,
        activate_tms,
        retire_change_request_id,
        retire_tms
    )
    select
        base_address_num,
        base_address_suffix,
        full_street_name,
        address_base_flg,
        unit_num,
        blk_lot,
        address_x_parcel_activate_change_request_id,
        address_x_parcel_create_tms,
        address_x_parcel_retire_change_request_id,
        address_x_parcel_retire_tms
    from vw_addresses_flat_native
    where address_x_parcel_id is not null
    and ( 
        address_x_parcel_create_tms > now()::date - 7
        or
        ( address_x_parcel_retire_tms is not null and address_x_parcel_retire_tms > now()::date - 7 )
      )
    and not (address_x_parcel_activate_change_request_id = 1 and address_x_parcel_retire_change_request_id = 1);


    declare cursorAddressReport cursor for
    SELECT id, base_address_num, base_address_suffix, full_street_name, blk_lot, activate_change_request_id, retire_change_request_id
    FROM address_change_report;
    begin
        open cursorAddressReport;
        loop
            fetch cursorAddressReport into _change_id, _base_address_num, _base_address_suffix, _full_street_name, _blk_lot, _activate_change_request_id, _retire_change_request_id;
            if not found then
                exit;
            end if;

            ----- validations BEGIN

            update address_change_report
            set exception = 'probably disaggregated'
            where id = _change_id
            and exists (
                select 1
                from address_change_report
                where id != _change_id
                      -- matching base address
                      and base_address_num = _base_address_num
                      and coalesce(base_address_suffix, '') = coalesce(_base_address_suffix, '')
                      and full_street_name = _full_street_name
                      and (
                              (
                                  -- other activates that do not match this activate
                                  _activate_change_request_id != activate_change_request_id
                                  and _retire_change_request_id is null
                                  and retire_change_request_id is null
                              )
                              or
                              (
                                    -- other activates that do not match this retire
                                    _retire_change_request_id is not null
                                    and _retire_change_request_id != activate_change_request_id
                                    and activate_change_request_id != 1
                              )
                              or
                              (
                                    -- other retires that do not match this activate
                                    retire_change_request_id is not null
                                    and retire_change_request_id != _activate_change_request_id
                                    and _activate_change_request_id != 1
                              )
                              or
                              (
                                  -- other retires that do not match this retire
                                  _retire_change_request_id is not null
                                  and retire_change_request_id is not null
                                  and _retire_change_request_id != retire_change_request_id
                              )

                      )

            );


            -- other possible validations...
            -- touching parcels in different change requests
            -- same parcel in different change requests

            ----- validations END


            ----- update other fields BEGIN

            update address_change_report
            set activate_reviewer = (
                select reviewer_full_name
                from vw_change_request
                where change_request_id = _activate_change_request_id
            ),
            retire_reviewer = (
                select reviewer_full_name
                from vw_change_request
                where change_request_id = coalesce(_retire_change_request_id, 0)
            )
            where id = _change_id;

            ----- update other fields END

        end loop;
        close cursorAddressReport;
    end;


return 'success!';


END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_change_report() OWNER TO eas_dbo;
