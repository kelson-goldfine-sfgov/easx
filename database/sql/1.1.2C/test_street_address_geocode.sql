
/* reset
update test_street_address_geocode
set
	found_match = null,
	found_eas_match = null,
	json_response = null,
	geometry = null;
*/	


-- examine sample
select *
from test_street_address_geocode
limit 1000


-- examine unsuccessful 
select *
from test_street_address_geocode
where 1=1
and found_match is not null 
and found_match = false
limit 1000


-- report
select 
	found_match, 
	found_eas_match, 
	count(*),
	(count(*)::float / (select count(*) from test_street_address_geocode where found_match is not null)::float) * 100 as percent
from test_street_address_geocode
where found_match is not null
group by found_match, found_eas_match
union all
select null, null, (select count(*) from test_street_address_geocode where found_match is not null), 100.0;


