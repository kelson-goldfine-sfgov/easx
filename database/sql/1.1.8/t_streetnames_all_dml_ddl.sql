
----- streetnames_staging
-- select * from streetnames_staging where 1=2
ALTER TABLE streetnames_staging DROP COLUMN prefix_direction;
ALTER TABLE streetnames_staging DROP COLUMN suffix_direction;
ALTER TABLE streetnames_staging DROP COLUMN prefix_type;
ALTER TABLE streetnames_staging DROP COLUMN suffix_type;
ALTER TABLE streetnames_staging DROP COLUMN geom;
ALTER TABLE streetnames_staging ADD COLUMN pre_direction varchar(10);
ALTER TABLE streetnames_staging ADD COLUMN post_direction varchar(10);
delete
from geometry_columns
where f_table_schema = 'public'
and f_table_name = 'streetnames_staging';
-----


----- streetnames_work
-- select * from streetnames_work where 1=2
ALTER TABLE streetnames_work DROP COLUMN prefix_direction;
ALTER TABLE streetnames_work DROP COLUMN suffix_direction;
ALTER TABLE streetnames_work DROP COLUMN prefix_type;
ALTER TABLE streetnames_work DROP COLUMN suffix_type;
ALTER TABLE streetnames_work ADD COLUMN pre_direction varchar(10);
ALTER TABLE streetnames_work ADD COLUMN post_direction varchar(10);
-----


----- streetnames
-- select * from streetnames where 1=2
ALTER TABLE streetnames DROP COLUMN prefix_direction;
ALTER TABLE streetnames DROP COLUMN suffix_direction;
ALTER TABLE streetnames DROP COLUMN prefix_type;
ALTER TABLE streetnames DROP COLUMN suffix_type;
ALTER TABLE streetnames ADD COLUMN pre_direction varchar(10);
ALTER TABLE streetnames ADD COLUMN post_direction varchar(10);
ALTER TABLE streetnames ADD COLUMN update_tms timestamp without time zone;
-----


-- set bsm id for the unknown streetname
update streetnames
set bsm_streetnameid = -1,
    seg_cnn = -1
where streetname_id = 50236;

-- set bsm id for the vista mar streetname (provisioning in progress)
update streetnames
set bsm_streetnameid = -2
where streetname_id in (50585, 50584);
