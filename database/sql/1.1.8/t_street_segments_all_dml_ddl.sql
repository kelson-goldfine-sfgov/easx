
-- select * from street_segments where 1=2

ALTER TABLE street_segments DROP COLUMN st_name;
ALTER TABLE street_segments DROP COLUMN st_type;

ALTER TABLE street_segments_staging DROP COLUMN st_name;
ALTER TABLE street_segments_staging DROP COLUMN st_type;

ALTER TABLE street_segments_work DROP COLUMN st_name;
ALTER TABLE street_segments_work DROP COLUMN st_type;

update street_segments
set seg_cnn = -1
where seg_cnn is null;

update street_segments
set l_f_add = 0,
	l_t_add = 0,
	r_f_add = 0,
	r_t_add = 0
where street_segment_id = 18874;

alter table street_segments alter l_f_add set not null;
alter table street_segments alter l_t_add set not null;
alter table street_segments alter r_f_add set not null;
alter table street_segments alter r_t_add set not null;
