-- Function: _eas_address_x_parcels_after()

-- DROP FUNCTION _eas_address_x_parcels_after();

CREATE OR REPLACE FUNCTION _eas_address_x_parcels_after()
  RETURNS trigger AS
$BODY$
DECLARE
    _row_count integer = 0;
    _action character(10) = null;
    _address_number integer;
    _full_street_name character varying(64);
    _block_lot character varying(10);
    _message character varying(128);
    INVALID_TYPE_PARCEL_RETIRED int = 1;
BEGIN

    IF (TG_OP = 'UPDATE') THEN

        _action = 'retire';

        delete from invalid_addresses
        WHERE address_id = NEW.address_id
        and invalid_type_id = INVALID_TYPE_PARCEL_RETIRED
        and not exists (
            select 1
            from address_x_parcels axp,
                parcels p
            where axp.address_id = NEW.address_id
            and axp.parcel_id = p.parcel_id
            and axp.retire_tms is null          -- there is a link
            and p.date_map_drop is not null     -- the parcel is retired
        );

    ELSE
        _action = 'insert';
    END IF;

    INSERT INTO address_x_parcels_history(
        address_x_parcel_id,
        parcel_id,
        address_id,
        activate_change_request_id,
        retire_change_request_id,
        create_tms,
        last_change_tms,
        retire_tms,
        history_action
    )
    SELECT
        NEW.id,
        NEW.parcel_id,
        NEW.address_id,
        NEW.activate_change_request_id,
        NEW.retire_change_request_id,
        NEW.create_tms,
        NEW.last_change_tms,
        NEW.retire_tms,
        _action;

    RETURN NEW;

  END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _eas_address_x_parcels_after() OWNER TO eas_dbo;
