-- select * from xmit_queue;

update xmit_queue
set xmit_tms = null
where id in (
    select 
        xq.id
        --xq.xmit_tms,
        --abh.base_address_prefix,
        --abh.base_address_num,
        --abh.base_address_suffix,
        --sn.base_street_name,
        --sn.street_type,
        --ah.*,
        --axph.*,
        --p.*
    from xmit_queue xq, 
        address_base_history abh,
        addresses_history ah,
        address_x_parcels_history axph,
        parcels p,
        streetnames sn
    where 1=1
    and xq.address_base_history_id = abh.id
    and xq.address_history_id = ah.id
    and xq.address_x_parcel_history_id = axph.id
    and axph.parcel_id = p.parcel_id
    and abh.street_segment_id = sn.street_segment_id
    and sn.category = 'MAP'
    and base_street_name = 'UNKNOWN'
    and p.blk_lot in (
        '0854010',
        '0854124',
        '0742046',
        '0742044',
        '0742045',
        '0742047',
        '0742048',
        '0742049',
        '0742050',
        '0742045',
        '0742044',
        '0742050',
        '0742049',
        '0742048',
        '0742047',
        '0742046'
    )
);

