-- Function: _sfmad_etl_parcels(character varying)

-- DROP FUNCTION _sfmad_etl_parcels(character varying);

CREATE OR REPLACE FUNCTION _sfmad_etl_parcels(IN _job_uuid character varying, OUT success boolean, OUT messagetext text)
  RETURNS record AS
$BODY$
DECLARE 

BEGIN

    -- Note that the parcels_work table is used for reporting after this process finishes.

    truncate parcels_work;
    success := false;

    ----- validations BEGIN

    -- if there are records in parcels_staging with nulls in block_lot, block_num, and lot_num, halt.
    if (
    		select count(*)
    		from parcels_staging
    		where blk_lot is null
    		or block_num is null
    		or lot_num is null
        ) > 0 then
            success := false;
            messageText := 'Stopping ETL. The parcels_staging table has records with null values in one of these fields: blk_lot, block_num, lot_num.';
            return;
    end if;

    -- if there are records in parcels_staging whose block_lot, block_num, and lot_num fields are not consistent, halt.
    if (
		select count(*)
		from parcels_staging
		where blk_lot <> (block_num || lot_num)
        ) > 0 then
            success := false;
            messageText := 'Stopping ETL. The parcels_staging table has records with inconsistent values between blk_lot and block_num and lot_num.';
            return;
    end if;

    -- parcels_staging records must have a unique blk_lot value
    if (
        select (select count(*) from parcels_staging) <> (select count(*) from (select distinct (blk_lot) from parcels_staging) a) 
    ) then 
        success := false;
        messageText := 'Stopping ETL. The blk_lot values in parcels_staging are not unique.';
        return;
    end if;

    -- If there are records in parcels that do not exist in parcels_staging, halt.
    -- This should catch the cases where there are missing parcels that have been linked.
    if (
	--select *
        select count(*)
        from parcels p
        left outer join parcels_staging ps on ps.blk_lot = p.blk_lot
        where ps.blk_lot is null
        and p.parcel_id not in (select parcel_id from parcels_provisioning where provisioned_tms is null)
        and exists (
            -- with links to addresses
            select 1
            from address_x_parcels axp
            where axp.parcel_id = p.parcel_id
        )
        ) > 0 then
            success := false;
            messageText := 'Stopping ETL. The parcels table has records that do not exist in parcels_staging table.';
            return;
    end if;

    -- If there are records in parcels that are retired and are being unretired, halt.
    if (
		select count(*)
		from parcels p
		inner join parcels_staging ps on p.blk_lot = ps.blk_lot
		where p.date_map_drop is not null
		and ps.date_map_drop is null
        ) > 0 then
            success := false;
            messageText := 'Stopping ETL. The parcels_staging table has records that would unretire records in the parcels table.';
            return;
    end if;

    ----- validations END


    ----- populate parcels_work BEGIN

    -- Delete parcels that
    --  are not in parcels_staging
    --  have never been linked
    --  are not being provisioned
    --  (RE http://code.google.com/p/eas/issues/detail?id=805)
    -- select * from parcels_work
    insert into parcels_work (
        parcel_id,
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        create_tms,
        status
    )
    select
        p.parcel_id,
        p.map_blk_lot,
        p.blk_lot,
        p.block_num,
        p.lot_num,
        p.date_rec_add,
        p.date_rec_drop,
        p.date_map_add,
        p.date_map_drop,
        p.date_map_alt,
        p.project_id_add,
        p.project_id_drop,
        p.project_id_alt,
        now(),
        'DELETED'
    from parcels p
    where parcel_id in (
        -- parcels that are not in parcels_staging
        select parcel_id
        from parcels p
        left outer join parcels_staging ps on ps.blk_lot = p.blk_lot
        where ps.blk_lot is null
    ) and not exists (
        -- with no links to addresses
        select 1
        from address_x_parcels axp
        where axp.parcel_id = p.parcel_id
    ) and p.parcel_id not in (
        -- the parcels are not being provisioned
        select parcel_id from parcels_provisioning where provisioned_tms is null
    );

    -- prepare new parcels
    insert into parcels_work (
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry,
        status
    )
    select
        a.map_blk_lot,
        a.blk_lot,
        a.block_num,
        a.lot_num,
        a.date_rec_add,
        a.date_rec_drop,
        a.date_map_add,
        a.date_map_drop,
        a.date_map_alt,
        a.project_id_add,
        a.project_id_drop,
        a.project_id_alt,
        a.exception_notes,
        now(),
        st_transform(a.geometry, 2227) as geometry,
        'NEW'
    from parcels_staging a
    left join parcels b on a.blk_lot = b.blk_lot
    where b.blk_lot is null;


    -- prepare updated parcels
    -- We look for any parcel that has changed.
    -- The ETL processing changes the geometry data ever so slightly so we cannot use a simple st_equals to find the geometries that have changed.
    -- Therefore we use an approximation
    --     st_area(ST_symDifference(ps.geometry, p.geometry)) / st_perimeter(ps.geometry) > 0.05
    -- for geometry change that I arrived at after about an hour of experimentation.
    -- The threshold of 0.05 represents the place where there seems to be a break in the trend
    -- and is about where you can't see the difference with your eye on the screen.
    insert into parcels_work (
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry,
        status
    )
    select
        ps.map_blk_lot,
        ps.blk_lot,
        ps.block_num,
        ps.lot_num,
        ps.date_rec_add,
        ps.date_rec_drop,
        ps.date_map_add,
        ps.date_map_drop,
        ps.date_map_alt,
        ps.project_id_add,
        ps.project_id_drop,
        ps.project_id_alt,
        ps.exception_notes,
        now(),
        ps.geometry,
        'UPDATED'
    from
        parcels p,
        parcels_staging ps
    WHERE p.blk_lot = ps.blk_lot
    AND (
           coalesce(p.map_blk_lot, '')                  != coalesce(ps.map_blk_lot, '')
        OR coalesce(p.block_num, '')                    != coalesce(ps.block_num, '')
        OR coalesce(p.lot_num, '')                      != coalesce(ps.lot_num, '')
        OR coalesce(p.date_rec_add::date, '1-1-1970'::date)   != coalesce(ps.date_rec_add::date, '1-1-1970'::date)
        OR coalesce(p.date_rec_drop::date, '1-1-1970'::date)  != coalesce(ps.date_rec_drop::date, '1-1-1970'::date)
        OR coalesce(p.date_map_add::date, '1-1-1970'::date)   != coalesce(ps.date_map_add::date, '1-1-1970'::date)
        OR coalesce(p.date_map_drop::date, '1-1-1970'::date)  != coalesce(ps.date_map_drop::date, '1-1-1970'::date)
        OR coalesce(p.date_map_alt::date, '1-1-1970'::date)   != coalesce(ps.date_map_alt::date, '1-1-1970'::date)
        OR coalesce(p.project_id_add, '')               != coalesce(ps.project_id_add, '')
        OR coalesce(p.project_id_drop, '')              != coalesce(ps.project_id_drop, '')
        OR coalesce(p.project_id_alt, '')               != coalesce(ps.project_id_alt, '')
        OR (
            ps.geometry is not null
            AND
            p.geometry is not null
            AND
            st_area(ST_symDifference(ps.geometry, p.geometry)) / st_perimeter(ps.geometry) > 0.05
        )
    );

    ----- populate parcels_work END

    ----- insert/update/delete parcels BEGIN
    delete
    from parcels
    where parcel_id in (
        select parcel_id
        from parcels_work
        where status = 'DELETED'
    );

    insert into parcels (
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry
    ) select
        map_blk_lot,
        blk_lot,
        block_num,
        lot_num,
        date_rec_add,
        date_rec_drop,
        date_map_add,
        date_map_drop,
        date_map_alt,
        project_id_add,
        project_id_drop,
        project_id_alt,
        exception_notes,
        create_tms,
        geometry
    from parcels_work
    where status = 'NEW';

    update parcels p
    set 
        map_blk_lot = pw.map_blk_lot,
        blk_lot = pw.blk_lot,
        block_num = pw.block_num,
        lot_num = pw.lot_num,
        date_rec_add = pw.date_rec_add,
        date_rec_drop = pw.date_rec_drop,
        date_map_add = pw.date_map_add,
        date_map_drop = pw.date_map_drop,
        date_map_alt = pw.date_map_alt,
        project_id_add = pw.project_id_add,
        project_id_drop = pw.project_id_drop,
        project_id_alt = pw.project_id_alt,
        exception_notes = pw.exception_notes,
        create_tms = pw.create_tms,
        geometry = st_transform(pw.geometry,2227),
        update_tms =  now()
    from parcels_work pw
    where p.blk_lot = pw.blk_lot
    and pw.status = 'UPDATED';

    -- Here we do some special processing for the service parcel geometry.
    -- The incoming geometry may be invalid because of the way it is produced; in this step we make it valid.
    -- If we were using postgis 2.0 we could use st_makevalid; alas we are using 1.5 so we must use this st_buffer hack.
    update parcels
    set geometry = (
        select st_buffer(geometry, 0)
        from parcels_staging
        where blk_lot = '0000000'
    )
    where blk_lot = '0000000';
    ----- insert/update/delete parcels END

    
    -- This validation flags addresses that are linked to retired parcels.
    -- select _eas_validate_addresses_after_etl('PARCELS');
    perform _eas_validate_addresses_after_etl('PARCELS');


    -- mark parcel provisioning rows
    update parcels_provisioning
    set provisioned_tms = now()
    where parcel_id in (
        select p.parcel_id
        from
            parcels p,
            parcels_work pw
        where p.blk_lot = pw.blk_lot
        and pw.status = 'UPDATED'
    );


    -- Record bounding boxes for map cache refresh.
    -- We do not use these as yet.
    -- It is part of a tile cache refesh optimization.
    -- But there is an issue:
    -- http://code.google.com/p/eas/issues/detail?id=179&q=geoserver
    /*
    insert into etl_bboxes (tms, geometry)
    select distinct now(), st_envelope(geometry)
    from parcels_work
    where geometry is not null;
    */

    success := true;
    messageText := 'Parcel ETL Successful.';
    return;

END;$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _sfmad_etl_parcels(character varying) OWNER TO postgres;
