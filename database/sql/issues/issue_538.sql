
-- support provisioning of streets when DPW turn around is too slow 	
-- http://code.google.com/p/eas/issues/detail?id=538

/*

Jeff and Paul and Judy agreed that we would use the block of seg_cnn values from 99,000,001 through 99,999,999
to indicate a provisional street segment.




*/

-- identify base addresses that are using DT provisioned segments.
select *
from address_base
where street_segment_id in (
    select street_segment_id
    from street_segments
    where seg_cnn > 99000000
);
