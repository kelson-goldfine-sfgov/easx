SELECT
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ss.seg_cnn,
    ab.zone_id
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP'
and sn.base_street_name = ( 'FAIRFAX')
and ab.base_address_num in (1151, 1101)
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num
limit 1000;


-- 1101 FAIRFAX
update address_base
set street_segment_id = ( 
    select street_segment_id
    from street_segments
    where seg_cnn = 5389001
) where address_base_id = (
    SELECT ab.address_base_id
    FROM address_base ab
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    WHERE sn.category = 'MAP'
    and sn.base_street_name = 'FAIRFAX'
    and ab.base_address_num = 1101
);


-- 1151 FAIRFAX
update address_base
set street_segment_id = ( 
    select street_segment_id
    from street_segments
    where seg_cnn = 5389002
) where address_base_id = (
    SELECT ab.address_base_id
    FROM address_base ab
    inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
    inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
    WHERE sn.category = 'MAP'
    and sn.base_street_name = 'FAIRFAX'
    and ab.base_address_num = 1151
);




