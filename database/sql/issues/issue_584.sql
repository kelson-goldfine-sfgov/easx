
select 
    min(left_from_address), max(left_from_address), 
    min(left_to_address), max(left_to_address), 
    min(right_from_address), max(right_from_address), 
    min(right_to_address), max(right_to_address)
from street_address_ranges;


select 
    min(l_f_add), max(l_f_add),
    min(l_t_add), max(l_t_add),
    min(r_f_add), max(r_f_add),
    min(r_t_add), max(r_t_add)
from street_segments
limit 10


select *
from xmit_queue
order by xmit_tms;

select *
from xmit_queue
where address_base_history_id in (381, 384);

update xmit_queue
set xmit_tms = now()
where address_base_history_id in (381, 384);

select * from address_base_history where id in (381, 382, 383, 384);

