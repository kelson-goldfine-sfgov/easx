﻿

-- future count
--   null 16476
--      1    38
--      0   245
select future, count(*)
from street_segments_staging
group by future


-- future count
--   null    12
select future, count(*)
from street_segments_work
group by future


-- future count
--   null    47
--     -1    67
--      1    38
--      0 16608
select future, count(*)
from street_segments
group by future



select *
from street_segments
where future = -1


select create_tms, count(*)
from street_segments
where future = 0 
group by create_tms
