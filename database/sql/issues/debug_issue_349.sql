
--select 1::varchar(10)

insert into street_segments (
  seg_cnn,
  st_name,
  st_type,
  date_added,
  create_tms,
  geometry
)
select 
    0,
    'UNKNOWN',
    'ST',
    now(),
    now(),
    st_geomFromEWKT(
        'SRID=2227;' || 
        'MULTILINESTRING((' || 
        min(st_x(st_pointn(geometry, 1)))::varchar(32) || ' ' || min(st_y(st_pointn(geometry, 1)))::varchar(32) ||  
        ',' || 
        min(st_x(st_pointn(geometry, 1)) + 100)::varchar(32) || ' ' || min(st_y(st_pointn(geometry, 1)) + 100)::varchar(32) ||  
        '))'
    )
from street_segments;

-- delete from street_segments where street_segment_id = 18886;

select *
from street_segments
where street_segment_id = (select max(street_segment_id) from street_segments);

18874

select currval('street_segments_street_segment_id_seq');


insert into streetnames (
  seg_cnn,
  base_street_name,
  street_type,
  full_street_name,
  category,
  street_segment_id,
  create_tms
)
values (
    0,
    'UNKNOWN',
    'ST',
    'UNKNOWN ST',
    'MAP',
    currval('street_segments_street_segment_id_seq'),
    now()
);


select *
from streetnames
where 1=1
and streetname_id >= (select max(streetname_id) - 100 from streetnames);



INSERT INTO street_address_ranges(
    left_from_address,
    left_to_address,
    right_from_address,
    right_to_address,
    update_tms,
    create_tms,
    street_segment_id)
VALUES (
    0,
    0,
    0,
    0,
    now(),
    now(),
    18874
);
