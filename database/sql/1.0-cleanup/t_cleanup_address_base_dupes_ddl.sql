-- drop table cleanup_address_base_dupes;
create table cleanup_address_base_dupes as 
SELECT
	ab1.address_base_id,
	ab1.base_address_prefix,
	ab1.base_address_num,
	ab1.base_address_suffix,
	ss1.st_name,
    ss1.st_type,
    z1.zipcode,
    coalesce(ab1.base_address_prefix,'')||ab1.base_address_num||coalesce(ab1.base_address_suffix,'')||ss1.st_name||coalesce(ss1.st_type,'')||z1.zipcode as full_addr,
    (select count(*) from addresses where addresses.address_base_id = ab1.address_base_id) as addresses_records_cnt,
    (select count(*) from address_x_parcels where address_id in (select address_id from addresses where addresses.address_base_id = ab1.address_base_id)) as axp_records_cnt,
    _sfmad_distance_to_segment (ab1.address_base_id) as dist_to_segment,
    '0'::character varying (1) as flag
FROM
	address_base ab1,
	street_segments ss1,
    zones z1
WHERE 1 = 1
AND ab1.street_segment_id = ss1.street_segment_id
and ab1.zone_id = z1.zone_id
and exists (
    SELECT 1
    FROM
        address_base ab2,
        street_segments ss2,
        zones z2
    WHERE ab2.street_segment_id = ss2.street_segment_id
    and ab2.address_base_id <> ab1.address_base_id
    and z2.zone_id = ab2.zone_id
	and COALESCE(ab1.base_address_prefix, '') = COALESCE(ab2.base_address_prefix, '')
	and ab1.base_address_num = ab2.base_address_num
	and COALESCE(ab1.base_address_suffix, '') = COALESCE(ab2.base_address_suffix, '')
    and ss1.st_name = ss2.st_name
    and COALESCE(ss1.st_type, '') = COALESCE(ss2.st_type, '')
    and z1.zone_id = z2.zone_id
    and ab2.retire_tms is null
)
order by
    ab1.base_address_prefix,
	ab1.base_address_num,
	ab1.base_address_suffix,
	ss1.st_name,
	ss1.st_type;
