
-- select _eas_cleanup_conflate_init_base_addresses();

-- select _eas_cleanup_conflate_run_base_addresses();

-- select * from cleanup_conflate_base_addresses order by base_address_num, base_street_name, street_type, base_address_suffix;

-- select distinct message from cleanup_conflate_base_addresses;

SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	ab.zone_id,
	asewkt(ab.geometry),
	a.address_base_flg,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ss.st_name
FROM address_base ab
inner join addresses a on ab.address_base_id = a.address_base_id
inner join streetnames sn on ab.street_segment_id = sn.street_segment_id
inner join street_segments ss on ab.street_segment_id = ss.street_segment_id
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id)
WHERE 1 = 1
and sn.category = 'MAP'
and sn.base_street_name like ( '%INCA%')
and ab.base_address_num = 1
--and a.address_id = 102984
--and ab.address_base_id = 225526
--and ab.unq_adds_id in (249654)
--and p.blk_lot = '0052119'
--and a.unit_num = '103'
/*
and ab.address_base_id in (
    select address_base_id
    from address_base
    where create_tms = '2011-04-18 10:50:51.178072'
)
*/
order by
	ss.st_name,
	ss.st_type,
	a.address_base_flg desc,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;

