
-- DROP VIEW vw_street_segments_map;

CREATE OR REPLACE VIEW vw_street_segments_map AS 
    SELECT 
        ss.street_segment_id,
        ss.seg_cnn,
        ss.str_seg_cnn,
        sn.base_street_name, 
        sn.street_type,
        ss.l_f_add,
        ss.l_t_add,
        ss.r_f_add,
        ss.r_t_add,
        ss.f_st,
        ss.t_st,
        ss.f_node_cnn,
        ss.t_node_cnn,
        ss.date_added,
        ss.gds_chg_id_add,
        ss.date_dropped,
        ss.gds_chg_id_dropped,
        ss.date_altered,
        ss.gds_chg_id_altered,
        ss.zip_code,
        ss.district,
        ss.accepted,
        ss.jurisdiction,
        ss.n_hood,
        ss.layer,
        ss.active,
        ss.future,
        ss.toggle_date,
        ss.create_tms,
        ss.update_tms,
        ss.geometry
    FROM 
        street_segments ss, 
        streetnames sn
    WHERE ss.street_segment_id = sn.street_segment_id 
    AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_street_segments_map OWNER TO eas_dbo;
GRANT ALL ON TABLE vw_street_segments_map TO eas_dbo;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE vw_street_segments_map TO django;
