
SELECT                  -- A flattened view of addresses.
	ab.address_base_id,
    ab.unq_adds_id,
	a.address_id,
    a.unq_adds_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
    sn.street_type,
    p.blk_lot,
    p.parcel_id,
	ab.create_tms as ab_create_tms,
	a.create_tms as a_create_tms,
    axp.create_tms as axp_create_tms,
	ab.retire_tms as ab_retire_tms,
	a.retire_tms as a_retire_tms,
    axp.retire_tms as axp_retire_tms,
    ab.street_segment_id,
    ab.zone_id
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP'
--and a.unit_num is not null
--and a.retire_tms is null
and sn.base_street_name = ( 'CLAY')
and ab.base_address_num = 1037
--and a.address_id = 276516
-- and ab.address_base_id = 21962
--and ab.unq_adds_id = 200095
--and p.blk_lot = '0052119'
--and a.unit_num = '103'
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num
limit 1000;






























select * 
from streetnames
where base_street_name like 'SOUTH PARK%';
-- address_base_id, unit_num, retire_tms

select count(*), address_base_id, unit_num, retire_tms
from addresses
group by address_base_id, unit_num, retire_tms
having count(*) > 1

select * 
from addresses
where address_base_id = 226675
and unit_num = '9'

"{21962}"

select *
from addresses
where 1=1
--and unit_num = '9'
and address_base_id = 226675
and retire_tms is null

SELECT
	ab.address_base_id,
	a.address_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num_prefix,
	a.unit_num,
	a.unit_num_suffix,
	ss.st_name,
    ss.st_type
FROM
	address_base ab,
	addresses a,
	street_segments ss
WHERE 1 = 1
AND ab.address_base_id = a.address_base_id
AND ab.street_segment_id = ss.street_segment_id
--and ss.st_name like 'SOUTH PARK%'
-- and ab.base_address_num = 10
and ab.unq_adds_id = 41889
order by 	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num_prefix,
	a.unit_num,
	a.unit_num_suffix,
	ss.st_name,
	ss.st_type
;

SELECT *,
	ab.address_base_id,
	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	ss.st_name,
    ss.st_type
FROM
	address_base ab,
	street_segments ss,
    zones z
WHERE 1 = 1
AND ab.street_segment_id = ss.street_segment_id
--and ss.st_name like 'SOUTH PARK%'
-- and ab.base_address_num = 10
and ab.unq_adds_id = 41889
and z.zone_id = ab.zone_id
order by 	ab.base_address_prefix,
	ab.base_address_num,
	ab.base_address_suffix,
	ss.st_name,
	ss.st_type
;



-- BEGIN street types
select *
from streetnames
where street_type not in (
	select abbreviated
	from d_street_type
) and category = 'MAP';


select * from d_street_type
distinct abbreviated from d_street_type

select abbreviated, count(*)
from d_street_type 
group by abbreviated
having count(*) > 1

select * 
from d_street_type
where abbreviated in ('SPUR', 'PARK', 'WALK', 'PKWY')

update d_street_type set abbreviated = 'PARKS', domain = 'CITY' where unabbreviated  = 'PARKS';
update d_street_type set abbreviated = 'PKWYS', domain = 'CITY' where unabbreviated  = 'PARKWAYS';
update d_street_type set abbreviated = 'SPURS', domain = 'CITY' where unabbreviated  = 'SPURS';
update d_street_type set abbreviated = 'WALKS', domain = 'CITY' where unabbreviated  = 'WALKS';



-- 205

select ss.st_type
from street_segments ss
where ss.st_type is not null;

select ss.st_type
from street_segments ss,
 d_street_type dst
where ss.st_type is not null
and dst.abbreviated = ss.st_type;


select 16235 - 16226

-- END street types





-- histogram of parcels linked to multiple adresseses
select axp.parcel_id, count(*)
from 	address_x_parcels axp,
	addresses a
where axp.address_id = a.address_id
group by axp.parcel_id
having count(*) > 9
order by count(*)


select distinct a.*
from 	parcels p,
	address_x_parcels axp,
	addresses a
where p.parcel_id = 148641
and axp.parcel_id = p.parcel_id
and axp.address_id = a.address_id;

select * 
from parcels 
where parcel_id = 148641

SELECT
  "cr_address_x_parcels"."id",
  "cr_address_x_parcels"."cr_address_id",
  "cr_address_x_parcels"."parcel_id",
  "cr_addresses"."cr_address_id",
  "cr_addresses"."cr_address_base_id",
  "cr_addresses"."unit_type",
  "cr_addresses"."floor_id",
  "cr_addresses"."unit_num_prefix",
  "cr_addresses"."unit_num",
  "cr_addresses"."unit_num_suffix",
  "cr_addresses"."change_request_id",
  "cr_addresses"."address_id",
  "cr_addresses"."mailable_flg",
  "cr_addresses"."disposition_code",
  "cr_addresses"."retire_flg",
  "cr_addresses"."address_base_flg",
  "parcels"."parcel_id",
  "parcels"."map_blk_lot",
  "parcels"."blk_lot",
  "parcels"."date_rec_add",
  "parcels"."date_rec_drop",
  "parcels"."date_map_add",
  "parcels"."date_map_drop",
  "parcels"."date_map_alt",
  "parcels"."project_id_add",
  "parcels"."project_id_drop",
  "parcels"."project_id_alt",
  "parcels"."exception_notes",
  "parcels"."create_tms",
  "parcels"."update_tms",
  "parcels"."geometry",
  "parcels"."block_num",
  "parcels"."lot_num"
FROM
  "cr_address_x_parcels"
  INNER JOIN "cr_addresses" ON ("cr_address_x_parcels"."cr_address_id" = "cr_addresses"."cr_address_id")
  INNER JOIN "parcels" ON ("cr_address_x_parcels"."parcel_id" = "parcels"."parcel_id")
WHERE "cr_address_x_parcels"."cr_address_id" IN (
  SELECT U0."cr_address_id"
  FROM "cr_addresses" U0
  WHERE (U0."address_base_flg" = False  AND U0."cr_address_base_id" = 274 )
)
ORDER BY "cr_address_x_parcels"."cr_address_id" ASC, "parcels"."blk_lot" ASC'

select * from etl_bboxes;

delete from etl_bboxes

INSERT INTO etl_bboxes(
            tms, geometry)
    VALUES (now(), 
        st_transform(GeometryFromText('POLYGON((-13638689.0 4537935.0,-13638689.0 4553222.0,-13620153.0 4553222.0,-13620153.0 4537935.0,-13638689.0 4537935.0))', 900913), 2227)
    );

    select
        b.map_blk_lot,
        b.blk_lot,
        b.block_num,
        b.lot_num,
        b.date_rec_add,
        b.date_rec_drop,
        b.date_map_add,
        b.date_map_drop,
        b.date_map_alt,
        b.project_id_add,
        b.project_id_drop,
        b.project_id_alt,
        b.exception_notes,
        now() as create_date,
        st_transform(a.geometry, 2227) as geometry,
        'RETIRED'
    from
        parcels a,
        parcels_staging b
    WHERE a.blk_lot = b.blk_lot
    AND a.date_rec_drop is null and b.date_rec_drop is not null;


select *
from parcels
where blk_lot = '2647036';



SELECT
  "vw_address_reviews"."address_review_id",
  "vw_address_reviews"."resolution_tms",
  "vw_address_reviews"."resolved_flg",
  "vw_address_reviews"."create_tms",
  "vw_address_reviews"."last_update_tms",
  "vw_address_reviews"."last_update_user_id",
  "vw_address_reviews"."concurrency_id",
  "vw_address_reviews"."message",
  "vw_address_reviews"."review_type_desc",
  "vw_address_reviews"."address_base_id",
  "vw_address_reviews"."base_address_prefix",
  "vw_address_reviews"."base_address_num",
  "vw_address_reviews"."base_address_suffix",
  "vw_address_reviews"."geometry",
  "vw_address_reviews"."full_street_name",
  "vw_address_reviews"."address_id",
  "vw_address_reviews"."unit_num_prefix",
  "vw_address_reviews"."unit_num",
  "vw_address_reviews"."unit_num_suffix",
  "vw_address_reviews"."address_base_flg",
  "vw_address_reviews"."sort_order"
FROM "vw_address_reviews"
WHERE "vw_address_reviews"."resolution_tms" IS NULL
and   st_intersects(vw_address_reviews.geometry, transform(GeomFromText('POLYGON((-13649314.601528 4538489.2804856,-13611325.398472 4538489.2804856,-13611325.398472 4554579.2749386,-13649314.601528 4554579.2749386,-13649314.601528 4538489.2804856))', 900913), 2227))
LIMIT 10;



select count(*)
from etl_bboxes

select count(*) from basemap_stclines_arterials

select * from street_segments_work
select * from streetnames_work

select * 
from street_segments_staging
limit 1000

-- n=133400
select count(*) from street_segments_staging

select count(*) from street_segments

-- n=16675
select count(*) from (select distinct (seg_cnn) from street_segments_staging) a
  

----- etl streets qa BEGIN
-- should be all zeros
select count(*) from basemap_stclines_city
union all
select count(*) from basemap_stclines_city_staging
union all
select count(*) from basemap_stclines_arterials
union all
select count(*) from basemap_stclines_arterials_staging
union all
select count(*) from basemap_stclines_freeway
union all
select count(*) from basemap_stclines_freeway_staging
union all
select count(*) from basemap_stclines_highway
union all
select count(*) from basemap_stclines_highway_staging
union all
select count(*) from basemap_stclines_major
union all
select count(*) from basemap_stclines_major_staging
union all
select count(*) from basemap_stclines_minor
union all
select count(*) from basemap_stclines_minor_staging
union all
select count(*) from basemap_stclines_non_paper
union all
select count(*) from basemap_stclines_non_paper_staging
union all
select count(*) from basemap_stclines_other
union all
select count(*) from basemap_stclines_other_staging
union all
select count(*) from basemap_stclines_ramp
union all
select count(*) from basemap_stclines_ramp_staging


----- etl streets qa END



select count(*)
from parcels where geometry is null


-- RETIRED, ALTERED (n=516)
select count(*)
from parcels 
where update_tms = '2010-04-22 17:08:29.240155'

-- NEW (n=8906)
select count(*)
from parcels
where create_tms = '2010-04-22 17:08:29.240155';

-- n=214625
select count(*) from parcels;

-- n=214461
select count(*) from parcels_staging;

-- eq 164
select 214625 - 214461;

-- eq 9417
select 8901 + 516;


select count(*) 
from parcels_work
where status = 'NEW'

select * 
from parcels 
where 1=1
--and parcel_id = 123104
and blk_lot = '3596127'
limit 100;





----- BEGIN etl jobs

/*
insert into etl_jobs (uuid, job_name)
select '0e119deb-d92d-4d11-b3b5-bca9f60abf1f', 'poi'
*/

select * from etl_jobs;
select * from etl_exceptions;
select * from etl_tables;

-- delete from etl_jobs where uuid = '1152270d-750d-4277-ac5a-25c36e48a03e';
/* 
update etl_jobs 
set committed_tms = null
where uuid = 'daf80c54-ad8a-4f60-aea0-2921fadee618';
*/


select j.uuid, j.job_name, j.staged_tms, j.committed_tms, t.staging_table_name, t.target_table_name
from 
	etl_jobs j,
	etl_tables t
where j.uuid = t.job_uuid
and j.uuid = 'f765391e-0b22-43b0-a0db-c7c43c3e362f';



----- END etl jobs





  select distinct
    a.address_id,
    (select review_type_id from d_address_review_types where review_type_desc = 'street segment retire'),
    'street: ' || sn.full_street_name
  from
    addresses a,
    address_base ab,
    street_segments ss,
    streetnames sn
  where ss.street_Segment_id = ab.street_segment_id
  and ab.address_base_id = a.address_base_id
  and a.address_base_flg = 'TRUE'
  and ab.street_segment_id = sn.street_segment_id
  and sn.category = 'MAP'
  and ss.date_dropped is not null;




select xmin(geometry)
from parcels
limit 100



----- begin etl_bboxes
truncate etl_bboxes;
delete 
from etl_bboxes
where id <> 4;


select count(*) from etl_bboxes;

select * from etl_bboxes;

INSERT into etl_bboxes(geometry)
(	
	select 
		st_envelope(geometry)
	from 
		parcels
	where 1=1
	and mod(parcel_id, 50000) = 0
)

select
	id,
	xmin(st_transform(geometry, 900913)) as llx,
	ymin(st_transform(geometry, 900913)) as lly,
	xmax(st_transform(geometry, 900913)) as urx,
	ymax(st_transform(geometry, 900913)) as ury
from etl_bboxes
where map_cache_refresh_tms is null;

----- end etl_bboxes





select * 
from parcels
where geometry is null;


-- select unretired addresses within a bounding box
select *
from addresses
where contains(, geometry)


-- addresses within a bbox
select *
from address_base ab, 
	addresses a
where contains( 
	st_transform(
		st_envelope(ST_GeomFromEWKT('SRID=4326;POLYGON((-122.42134 37.77179 0,-122.42134 37.77780 0,-122.41537 37.77780 0,-122.41537 37.77179 0,-122.42134 37.77179 0))')),
		2227
	), 
	ab.geometry
)
and ab.address_base_id = a.address_base_id



-- use this to create data for dev
-- every address within a distance from a point
-- sampling using modulus op
--select * from address_review
-- lat lon of 1 south van ness: ST_GeomFromEWKT('SRID=4326;POINT(-122.4189 37.775)'),
-- truncate address_review;
INSERT into address_review(address_id, review_type_id, message)
(	select 
		a.address_id, 
		1, 
		'this is a dummy message'
	from 
		address_base ab
		,addresses a
	where 1=1
	and st_dwithin(
		st_transform(
			ST_GeomFromEWKT('SRID=4326;POINT(-122.4189 37.775)'),
			2227
			), 
		ab.geometry,
		4000
	)
	and ab.address_base_id = a.address_base_id
	and mod(ab.address_base_id, 50) = 0
)

--  The initial view definition for address reviews
select 
	ar.address_review_id,
	ar.create_tms, 
        ar.message,
        dart.review_type_desc,
	ab.address_base_id, 
        ab.base_address_prefix,
        ab.base_address_num,
        ab.base_address_suffix,
        ab.geometry,
        sn.full_street_name,
        a.address_id,
        a.unit_num_prefix,
        a.unit_num,
        a.unit_num_suffix,
        a.address_base_flg,
        0 as sort_order
from 
	address_review ar,
	d_address_review_types dart,
	addresses a,
	address_base ab,
	streetnames sn
where 1=1
and ar.address_id = a.address_id
and a.address_base_id = ab.address_base_id
and sn.category = 'MAP'
and ab.street_segment_id = sn.street_segment_id
and dart.review_type_id = ar.review_type_id	
order by 
	ab.address_base_id, 
	a.address_id,
	ar.create_tms,
	a.unit_num






--- record all addresses that are associated with a parcel that is no longer underneath
   select
	distinct
	ab1.base_address_num,
	sn1.base_street_name AS street_name,
	sn1.street_type,
	sn1.category,
	a1.address_id,
	a1.unit_num_prefix,
	a1.unit_num,
	a1.unit_num_suffix,
	p1.blk_lot
   from
       address_base ab1,
       addresses a1,
       address_x_parcels axp1,
       parcels p1,
       streetnames sn1
   where ab1.address_base_id = a1.address_base_id
   and ab1.retire_tms is null
   and a1.retire_tms is null
   and a1.address_id = axp1.address_id
   and axp1.parcel_id = p1.parcel_id
   and p1.date_rec_drop is null
   -- todo - do we need date_map_drop?
   --and p1.date_map_drop is null
   and ab1.street_segment_id = sn1.street_segment_id
   and sn1.category = 'MAP'
   and not exists (
        select 1
        from
           parcels p2,
           address_base ab2,
           addresses a2
        where contains(p2.geometry, ab2.geometry)
	and ab2.address_base_id = ab1.address_base_id
	and a2.address_id = a1.address_id
	and p2.blk_lot = p1.blk_lot
   )
   order by 
	ab1.base_address_num,
	sn1.base_street_name,
	sn1.street_type,
	sn1.category,
	a1.address_id,
	a1.unit_num_prefix,
	a1.unit_num,
	a1.unit_num_suffix,
	p1.blk_lot
   limit 1000

-- same as above but refined for MAD
    select distinct
	a1.address_id,
	1,
	p1.blk_lot
   from
       address_base ab1,
       addresses a1,
       address_x_parcels axp1,
       parcels p1
   where ab1.address_base_id = a1.address_base_id
   and ab1.retire_tms is null
   and a1.retire_tms is null
   and a1.address_id = axp1.address_id
   and axp1.parcel_id = p1.parcel_id
   and p1.date_rec_drop is null
   -- todo - do we need date_map_drop?
   --and p1.date_map_drop is null
   and not exists (
        select 1
        from
           parcels p2,
           address_base ab2,
           addresses a2
        where contains(p2.geometry, ab2.geometry)
	and ab2.address_base_id = ab1.address_base_id
	and a2.address_id = a1.address_id
	and p2.blk_lot = p1.blk_lot
   )
   order by 1,2,3
   limit 1000



SELECT
    ab.address_base_id,
    p.block_num,
    p.lot_num
FROM address_base ab, 
    parcels p
WHERE 1 = 1 
and contains(p.geometry, ab.geometry)
limit 10












select *
from addresses
where address_base_id = 162206

select *
from v_base_address_search
where 1=1
and base_address_num = 2633
and street_name = 'TURK'
--and street_type = 'ST'


select axp.* 
from 
	addresses a, 
	address_x_parcels axp
where a.address_base_id = 70220
and axp.address_id = a.address_id


SELECT address_base_id, base_address_prefix, base_address_num, base_address_suffix, 
       create_tms, geometry, street_name, street_type, street_category, 
       street_prefix_direction, street_suffix_direction, full_street_name, 
       zipcode, jurisdiction, summary_of_units, count_of_units, disposition_min, 
       disposition_max
  FROM v_base_address_search
  where address_base_id = 62706;

----- select base address rows



select *
from v_base_address_search
where 1=1
and base_address_num = 1
and street_name = 'SOUTH VAN NESS'
--and street_type = 'ST'

SELECT *
FROM v_base_address_search
where address_base_id = 225556;

select * 
from address_base 
where address_base_id = 225556;

select *
from addresses
where address_base_id = 225556
and retire_tms is null;

select *
from address_x_parcels
where address_id in (
	select address_id
	from addresses
	where address_base_id = 225556
);



----- select or delete change request

delete
--select *
from cr_address_x_parcels
where cr_address_id in (
	select cr_address_id
	from cr_addresses
	where change_request_id = 2
);

delete
--select *
from cr_addresses
where change_request_id = 2;

delete
--select *
from cr_address_base 
where cr_address_base_id in (
	select cr_address_base_id
	from cr_addresses
	where change_request_id = 2
);

delete 
--select *
from cr_change_requests
where change_request_id = 2;

-----


select  
	dcrs.*, '|||'
	,cr.*, '|||'
	,crab.*, '|||'
	,cra.*, '|||'
	,crxp.*
from 	
	cr_change_requests cr
	,d_change_request_status dcrs
	,cr_address_base crab
	,cr_addresses cra
	,cr_address_x_parcels crxp
where	crab.cr_address_base_id = cra.cr_address_base_id
and	cra.change_request_id = cr.change_request_id
and 	cr.review_status = dcrs.status_id
--and 	cr.requestor_user_id = 3
and 	cra.cr_address_id = crxp.cr_address_id
order by cr.change_request_id



delete 
from cr_change_requests
where change_request_id = 108

select * 
from cr_change_requests
order by change_request_id

select * from d_change_request_status

select *
from auth_user

select *
from zones

select  
	dcrs.*, '|||',
	cr.*, '|||',
	crab.*, '|||',
	cra.*, '|||'
from 	
	cr_change_requests cr,
	d_change_request_status dcrs,
	cr_address_base crab, 
	cr_addresses cra
where	crab.cr_address_base_id = cra.cr_address_base_id
and	cra.change_request_id = cr.change_request_id
and 	cr.review_status = dcrs.status_id
and status_id = 2



select * 
from parcels 
where block_num = '7517'
and lot_num = 'COM'


SELECT (st_distance(vw_streets_for_new_address.geometry, GeomFromText('POINT (6007068.3260072079000000 2111312.8296672939000000)', 2227))) AS "distance", 
	"vw_streets_for_new_address"."street_segment_id", 
	"vw_streets_for_new_address"."description", 
	"vw_streets_for_new_address"."geometry" 
FROM "vw_streets_for_new_address" 
WHERE ST_DWithin("vw_streets_for_new_address"."geometry", GeomFromText('POINT (6007068.3260072079000000 2111312.8296672939000000)', 2227), 500) ORDER BY "distance" ASC


-- nearby streets
SELECT 
    "vw_streets_for_new_address"."street_segment_id", 
    "vw_streets_for_new_address"."description", 
    "vw_streets_for_new_address"."geometry" ,
    st_distance(GeomFromText('POINT (6007068.3260072079000000 2111312.8296672939000000)', 2227), "vw_streets_for_new_address"."geometry")
FROM "vw_streets_for_new_address" 
WHERE ST_DWithin("vw_streets_for_new_address"."geometry", GeomFromText('POINT (6007068.3260072079000000 2111312.8296672939000000)', 2227), 500)
order by 
    st_distance(GeomFromText('POINT (6007068.3260072079000000 2111312.8296672939000000)', 2227), "vw_streets_for_new_address"."geometry")


 SELECT sn.category, ss.*, 
 ss.street_segment_id, 
	sn.full_street_name,
	sar.right_from_address, 
	sar.right_to_address, 
	sar.left_from_address, 
	sar.left_to_address,
	sar.right_from_address,
	sar.right_to_address, 
	sar.left_from_address, 
	sar.left_to_address, 
	ss.geometry,
	length(ss.geometry)
   FROM street_segments ss, 
	streetnames sn, 	
	street_address_ranges sar
  WHERE ss.street_segment_id = sn.street_segment_id 
  AND ss.date_dropped IS NULL 
  AND ss.street_segment_id = sar.street_segment_id 
  AND sn.category = 'MAP'
  and active = true
and ST_DWithin(ss.geometry, GeomFromText('POINT (6007068.3260072079000000 2111312.8296672939000000)', 2227), 500)





-- project geom
select asewkt(transform(GeomFromText('POINT(-122.4195 37.77767)', 4326), 900913))

select 
	x(transform(GeomFromText('POINT(" . $x . " " . $y . ")', 4326), 900913)) as x_coordinate, 
	y(transform(GeomFromText('POINT(" . $x . " " . $y . ")', 4326), 900913)) as y_coordinate
  	


  	

SELECT 
    count(*),
    ab.prefix, 
    ab.base_address_num, 
    ab.suffix, 
    sn.base_street_name AS street_name, 
    sn.street_suffix
FROM address_base ab,
    addresses a, 
    streetnames sn,
    address_x_parcels axp,
    parcels p
WHERE 1 = 1 
AND ab.address_base_id = a.address_base_id 
AND ab.retire_tms IS NULL 
AND ab.street_segment_id = sn.street_segment_id
group by ab.prefix, 
    ab.base_address_num, 
    ab.suffix, 
    sn.base_street_name AS street_name, 
    sn.street_suffix,
order by 1


SELECT *
FROM address_base ab,
    addresses a,
    streetnames sn,
    address_x_parcels axp,
    parcels p
WHERE 1 = 1
AND ab.address_base_id = a.address_base_id
-- exclude retired base addresses
AND ab.retire_tms IS NULL
AND ab.street_segment_id = sn.street_segment_id
-- exclude aliases e.g. include "CESAR CHAVEZ ST" but exclude "ARMY ST"
AND sn.category = 'MAP'



select * 
from address_base
where base_address_num > 10000

-- example of ambiguous results
select 
    t1.base_address_num, 
    t1.street_name, 
    t1.street_type, 
    t2.base_address_num, 
    t2.street_name, 
    t2.street_type
from v_base_address_search t1,
     v_base_address_search t2
where 1=1
and t1.street_name = t2.street_name
and t1.base_address_num = t2.base_address_num
and t1.street_type != t2.street_type
and t1.street_name like '18%'
and t1.base_address_num < 1000
order by t1.base_address_num


select *
from v_base_address_search_dev
where base_address_num = 55
and street_name = 'FELL'

select * 
from v_base_address_search_dev
where 1=1
and street_name = '18TH'
order by base_address_num

select * from addresses where address_base_id in (41454, 48139)


select 	*
from v_base_address_search
where address_base_id = 282427

and street_name = 'GEARY'

select count(*)
from addresses
where subaddress_num like '%,%'
and subaddress_num != '-999'

select 
	count(*),
	a.disposition_id, 
	ad.disposition_code, 
	ad.disposition_description
from 
	addresses a, 
	d_address_disposition ad
where a.disposition_id = ad.disposition_id
group by 2,3,4


select *
from d_address_disposition ad



select *
from address_base
where address_base_id = 282427


SELECT 
    (abs(base_address_num - 107)) AS calc_sort_order,
    v_base_address_search.address_base_id,
    v_base_address_search.prefix,
    v_base_address_search.base_address_num,
    v_base_address_search.suffix,
    v_base_address_search.street_name,
    v_base_address_search.street_type,
    v_base_address_search.street_suffix,
    v_base_address_search.street_category,
    v_base_address_search.street_prefix_direction,
    v_base_address_search.street_suffix_direction,
    v_base_address_search.full_streetname,
    v_base_address_search.zipcode,
    v_base_address_search.jurisdiction,
    v_base_address_search.geometry,
    v_base_address_search.summary_of_units,
    v_base_address_search.units_count 
FROM v_base_address_search 
WHERE street_name like 'SOUTH%' 
ORDER BY calc_sort_order ASC
limit 20

SELECT (abs(base_address_num - 15)) AS calc_sort_order,
 v_base_address_search.address_base_id,
 v_base_address_search.prefix,
 v_base_address_search.base_address_num,
 v_base_address_search.suffix,
 v_base_address_search.street_name,
 v_base_address_search.street_type,
 v_base_address_search.street_suffix,
 v_base_address_search.street_category,
 v_base_address_search.street_prefix_direction,
 v_base_address_search.street_suffix_direction,
 v_base_address_search.full_streetname,
 v_base_address_search.zipcode,
 v_base_address_search.jurisdiction,
 v_base_address_search.geometry,
 v_base_address_search.summary_of_units,
 v_base_address_search.count_of_units
 FROM v_base_address_search 
 WHERE street_name like 'GEAR%' 
 ORDER BY calc_sort_order ASC,
 v_base_address_search.base_address_num ASC LIMIT 20


select 1


---------- BEGIN get date info for address

SELECT
	min(ab.create_tms) as create_tms,
	CASE WHEN max(a.create_tms) > max(a.retire_tms) THEN max(a.create_tms)
	    ELSE max(a.create_tms)
	END as last_change_tms
FROM address_base ab, 
    addresses a
WHERE 1 = 1 
AND ab.address_base_id = a.address_base_id 
AND ab.retire_tms IS NULL 
and ab.address_base_id = 166589;



---------- END get date info for address





------- BEGIN This section for dev of base address X APN

select * 
from address_base
where address_base_id = 166589;

select * 
from addresses
where address_base_id = 166589;

select distinct 
	--address_id, 
	parcel_id
from address_x_parcels
where address_id in (
	select address_id
	from addresses
	where address_base_id = 166589
)
order by 
	--address_id,
	parcel_id;

-- this is what we shall use to get APNs for a given base address
SELECT distinct
    ab.address_base_id,
    p.block_num,
    p.lot_num
FROM address_base ab, 
    addresses a, 
    address_x_parcels axp,
    parcels p
WHERE 1 = 1 
AND ab.address_base_id = a.address_base_id 
AND ab.retire_tms IS NULL 
and a.address_id = axp.address_id
and axp.parcel_id = p.parcel_id
and ab.address_base_id = 166589;



SELECT distinct
/*
    ab.address_base_id, 
    ab.prefix, 
    ab.base_address_num, 
    ab.suffix, 
    sn.base_street_name AS street_name, 
    sn.street_suffix,
    a.address_id,
    a.subaddress_num_prefix,
    a.subaddress_num,
    a.subaddress_num_suffix,
    --axp.*,
*/
    p.block_num,
    p.lot_num
FROM address_base ab, 
    addresses a, 
    streetnames sn,
    address_x_parcels axp,
    parcels p
WHERE 1 = 1 
AND ab.address_base_id = a.address_base_id 
AND ab.retire_tms IS NULL 
AND ab.street_segment_id = sn.street_segment_id
--and sn.base_street_name = 'CHESTNUT'
--and ab.base_address_num = 1
--and ab.base_address_num != 0
--and sn.category = 'MAP' 
and a.address_id = axp.address_id
and axp.parcel_id = p.parcel_id
and address_base_id = 166589;
--order by 
    --ab.base_address_num, 
    --a.subaddress_num
limit 5000

------- END This section for dev of base address X APN




-- this is, in part, what geocoding does 
SELECT (abs(base_address_num - 10)) AS "calc_sort_order",
 "v_base_address_search"."address_base_id",
 "v_base_address_search"."base_address_prefix",
 "v_base_address_search"."base_address_num",
 "v_base_address_search"."base_address_suffix",
 "v_base_address_search"."create_tms",
 "v_base_address_search"."street_name",
 "v_base_address_search"."street_type",
 "v_base_address_search"."street_category",
 "v_base_address_search"."street_prefix_direction",
 "v_base_address_search"."street_suffix_direction",
 "v_base_address_search"."full_street_name",
 "v_base_address_search"."zipcode",
 "v_base_address_search"."jurisdiction",
 "v_base_address_search"."summary_of_units",
 "v_base_address_search"."count_of_units",
 "v_base_address_search"."disposition_min",
 "v_base_address_search"."disposition_max",
 "v_base_address_search"."geometry"
FROM "v_base_address_search"
WHERE street_name like 'S%'
ORDER BY "calc_sort_order" ASC, "v_base_address_search"."base_address_num" ASC, "v_base_address_search"."street_name" ASC
LIMIT 20


ALTER TABLE change_requests
  ADD CONSTRAINT "cr_change_request_X_change_request" FOREIGN KEY (cr_change_request_id)
      REFERENCES cr_change_requests (change_request_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;


-- duplciates

SELECT
	ab1.address_base_id,
	ab1.base_address_prefix,
	ab1.base_address_num,
	ab1.base_address_suffix,
	ss1.st_name,
    ss1.st_type
FROM
	address_base ab1,
	street_segments ss1
WHERE 1 = 1
AND ab1.street_segment_id = ss1.street_segment_id
and ab1.address_base_id = 230520
and ab1.create_tms <> '2009-09-01 16:39:13.599'
and exists (
    SELECT 1
    FROM
        address_base ab2,
        street_segments ss2
    WHERE ab2.street_segment_id = ss2.street_segment_id
    and ab2.address_base_id <> ab1.address_base_id
	and COALESCE(ab1.base_address_prefix, '') = COALESCE(ab2.base_address_prefix, '')
	and ab1.base_address_num = ab2.base_address_num
	and COALESCE(ab1.base_address_suffix, '') = COALESCE(ab2.base_address_suffix, '')
    and ss1.st_name = ss2.st_name
    and COALESCE(ss1.st_type, '') = COALESCE(ss2.st_type, '')
    and ab2.retire_tms is null
)
order by
    ab1.base_address_prefix,
	ab1.base_address_num,
	ab1.base_address_suffix,
	ss1.st_name,
	ss1.st_type;


truncate TABLE basemap_bay_area_cities_staging;
truncate TABLE basemap_citylots_base_staging;
truncate TABLE basemap_citylots_staging;
truncate TABLE basemap_county_line_staging;
truncate TABLE basemap_ocean_bay_labels_staging;
truncate TABLE basemap_poi_staging;
truncate TABLE basemap_sfmaps_coast_staging;
truncate TABLE basemap_stclines_arterials_staging;
truncate TABLE basemap_stclines_city_staging;
truncate TABLE basemap_stclines_freeway_staging;
truncate TABLE basemap_stclines_highway_staging;
truncate TABLE basemap_stclines_major_staging;
truncate TABLE basemap_stclines_minor_staging;
truncate TABLE basemap_stclines_non_paper_staging;
truncate TABLE basemap_stclines_other_staging;
truncate TABLE basemap_stclines_ramp_staging;



-- DUPLIOCATES
--- http://code.google.com/p/eas/issues/detail?id=246&q=duplicate

SELECT
	ab1.address_base_id,
	ab1.base_address_prefix,
	ab1.base_address_num,
	ab1.base_address_suffix,
	ss1.st_name,
    ss1.st_type,
    z1.zipcode
FROM
	address_base ab1,
	street_segments ss1,
    zones z1
WHERE 1 = 1
AND ab1.street_segment_id = ss1.street_segment_id
--and ab1.unq_adds_id = 206137
and ab1.create_tms = '2009-09-01 16:39:13.599'
and ab1.zone_id = z1.zone_id
and exists (
    SELECT 1
    FROM
        address_base ab2,
        street_segments ss2,
        zones z2
    WHERE ab2.street_segment_id = ss2.street_segment_id
    and ab2.address_base_id <> ab1.address_base_id
    and z2.zone_id = ab2.zone_id
	and COALESCE(ab1.base_address_prefix, '') = COALESCE(ab2.base_address_prefix, '')
	and ab1.base_address_num = ab2.base_address_num
	and COALESCE(ab1.base_address_suffix, '') = COALESCE(ab2.base_address_suffix, '')
    and ss1.st_name = ss2.st_name
    and COALESCE(ss1.st_type, '') = COALESCE(ss2.st_type, '')
    and z1.zone_id = z2.zone_id
    and ab2.retire_tms is null
)
order by
    ab1.base_address_prefix,
	ab1.base_address_num,
	ab1.base_address_suffix,
	ss1.st_name,
	ss1.st_type;



SELECT
    ab.address_base_id,
    ab.base_address_prefix,
    ab.base_address_num,
    ab.base_address_suffix,
    st_transform(ab.geometry, 900913) AS geometry,
    ST_AsEWKT(
        st_transform(
            ST_line_interpolate_point(
                ST_GeometryN(
                    ST_SymDifference(
                        p.geometry,
                        ST_MakeLine(
                            ST_PointOnSurface(p.geometry),                                                                                          -- centroid - works properly for odd shapes too
                            ST_line_interpolate_point(ST_GeometryN(ss.geometry,1), ST_line_locate_point(ST_GeometryN(ss.geometry,1), ab.geometry))  -- this is the point on the street segment that is nearest the parcel centroid
                        )                   -- this is the line from the parcel centroid to the point on the street segment that is nearest the parcel centroid
                    ),                      -- diff of the line and the parcel - keep the part of the line that lines outside the parcel
                    1
                ),
                0.50                        -- the midpoint of the remaining line segment
            ),
            900913
        )
    ) as dpw_geometry,
    sn.full_street_name,
    z.zipcode,
    z.jurisdiction
FROM
    address_base ab,
    streetnames sn,
    street_segments ss,
    zones z,
    parcels p
WHERE ab.retire_tms IS NULL
AND ab.zone_id = z.zone_id
AND ab.street_segment_id = sn.street_segment_id
AND sn.category::text = 'MAP'::text
AND ss.street_segment_id = ab.street_segment_id
and st_contains(p.geometry, ab.geometry)
and p.map_blk_lot = p.blk_lot
limit 10;
