﻿
-- changes in base addresses
SELECT
	ab.base_address_num,
	ab.base_address_suffix,
	sn.base_street_name,
	sn.street_type,
	a.create_tms as a_create_tms,
	a.retire_tms as a_retire_tms
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
WHERE 1 = 1
and sn.category = 'MAP'
and a.address_base_flg = True
and (a.create_tms > '2012-02-15'::date or a.retire_tms > '2012-02-15'::date)
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;



-- changes in unit addresses
SELECT
	ab.base_address_num,
	ab.base_address_suffix,
	sn.base_street_name,
	sn.street_type,
	a.unit_num,
	a.create_tms as a_create_tms,
	a.retire_tms as a_retire_tms
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
WHERE 1 = 1
and sn.category = 'MAP'
and a.address_base_flg = False
and (a.create_tms > '2012-02-15'::date or a.retire_tms > '2012-02-15'::date)
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;



-- changes in DBI adresses
SELECT
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num,
	sn.base_street_name,
	sn.street_type,
	p.blk_lot,
	axp.create_tms as axp_create_tms,
	axp.retire_tms as axp_retire_tms
FROM address_base ab
inner join addresses a on (ab.address_base_id = a.address_base_id)
inner join streetnames sn on (ab.street_segment_id = sn.street_segment_id)
inner join street_segments ss on (ab.street_segment_id = ss.street_segment_id)
left outer join address_x_parcels axp on (axp.address_id = a.address_id)
left outer join parcels p on (axp.parcel_id = p.parcel_id) 
WHERE 1 = 1
and sn.category = 'MAP'
and (axp.create_tms > '2012-02-15'::date or axp.retire_tms > '2012-02-15'::date)
order by
	ss.st_name,
	ss.st_type,
	ab.base_address_num,
	ab.base_address_suffix,
	a.unit_num;
