
-- View: vw_streets_for_new_address

-- DROP VIEW vw_streets_for_new_address;

CREATE OR REPLACE VIEW vw_streets_for_new_address AS
SELECT
    ss.street_segment_id,
    sn.full_street_name 
        || 
        ' (' 
        ||     
        least(sar.right_from_address, sar.right_to_address, sar.left_from_address, sar.left_to_address)
        || 
        ' - '
        ||
	greatest(sar.right_from_address, sar.right_to_address, sar.left_from_address, sar.left_to_address)
	||
	')' as description,
    ss.geometry
FROM
    street_segments ss,
    streetnames sn,
    street_address_ranges sar
WHERE ss.street_segment_id = sn.street_segment_id
AND ss.date_dropped IS NULL
AND ss.street_segment_id = sar.street_segment_id
AND sn.category::text = 'MAP'::text;

ALTER TABLE vw_streets_for_new_address OWNER TO postgres;
