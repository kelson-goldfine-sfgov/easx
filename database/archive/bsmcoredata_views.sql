create view [dbo].[vw_sfmad1_pname_x_seg] as
SELECT dbo.StreetNames.StreetNameID, dbo.AddressRange.CNN, dbo.RangeNames.Category
FROM (dbo.StreetNames INNER JOIN dbo.RangeNames ON dbo.StreetNames.StreetNameID = dbo.RangeNames.StreetNameID) 
INNER JOIN dbo.AddressRange ON dbo.RangeNames.AddRangeID = dbo.AddressRange.AddRangeID
GROUP BY dbo.StreetNames.StreetNameID, dbo.AddressRange.CNN, dbo.RangeNames.Category
HAVING (((dbo.RangeNames.Category)='MAP'));

--------------------------------

create view [dbo].[vw_sfmad2_aname_x_seg] as
SELECT dbo.StreetNames.StreetNameID, dbo.AddressRange.CNN, dbo.RangeNames.Category
FROM (dbo.StreetNames INNER JOIN dbo.RangeNames ON dbo.StreetNames.StreetNameID = dbo.RangeNames.StreetNameID) 
		INNER JOIN dbo.AddressRange ON dbo.RangeNames.AddRangeID = dbo.AddressRange.AddRangeID
GROUP BY dbo.StreetNames.StreetNameID, dbo.AddressRange.CNN, dbo.RangeNames.Category
HAVING (((dbo.RangeNames.Category)='ALIAS'));

---------------------------------

create view [dbo].[vw_sfmad3_streetnames_all] as 
SELECT
a.cnn,
b.StreetNameID,
a.Category,
b.StreetName,
b.StreetType ,
rtrim(b.StreetName +' '+ isnull(b.StreetType,' ')) as full_streetname

FROM 

(
SELECT  * from dbo.vw_sfmad1_pname_x_seg 
UNION

SELECT 
	vw_sfmad2_aname_x_seg.StreetNameID, 
	vw_sfmad2_aname_x_seg.CNN, 
	vw_sfmad2_aname_x_seg.Category
 FROM 
	vw_sfmad2_aname_x_seg 
		LEFT JOIN vw_sfmad1_pname_x_seg ON (vw_sfmad2_aname_x_seg.CNN = vw_sfmad1_pname_x_seg.CNN) AND 
			(vw_sfmad2_aname_x_seg.StreetNameID = vw_sfmad1_pname_x_seg.StreetNameID)
 WHERE (((vw_sfmad1_pname_x_seg.StreetNameID) Is Null) AND ((vw_sfmad1_pname_x_seg.CNN) Is Null))
	) a

INNER JOIN dbo.StreetNames b ON a.StreetNameID = b.StreetNameID;