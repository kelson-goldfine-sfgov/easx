Ext.define('MAD.widgets.AddressReport', {
    extend: 'Ext.panel.Panel',
    requires: ['Ext.ux.grid.FiltersFeature'],

    layout: 'card',
    addressId: '',
    addressJson: '',
    user: null,
    showHistoricalData: false,
    renderers: {
        disposition: function(value, p, record, rowIndex) {
            var index = MAD.app.dispositionStore.find('disposition_code', value);
            if (index > -1) {
                return MAD.app.dispositionStore.getAt(index).get('disposition_description');
            } else {
                return value;
            }

        },
        unitType: function(value, p, record, rowIndex) {
            var index = MAD.app.unitTypeStore.find('unit_type_id', value);
            if (index > -1) {
                return MAD.app.unitTypeStore.getAt(index).get('unit_type_description');
            } else {
                return value;
            }

        },
        isBase: function(value, p, record, rowIndex) {
            if (value) {
                return 'base';
            } else {
                return 'unit';
            }

        },
        unitInfo: function(value, p, record, rowIndex) {
            var num = record.get("unit_num") !== null ? record.get("unit_num") : "";
            return Ext.util.Format.trim(num);
        }
    },

    initComponent: function() {
        if (!this.user) {
            this.user = MAD.model.User;
        }

        this.user.on('logout', this.updateHeaderContent, this);
        this.user.on('login', this.updateHeaderContent, this);

        this.on('beforedestroy', function () {
            this.user.un('logout', this.updateHeaderContent, this);
            this.user.un('login', this.updateHeaderContent, this);
        }, this);

        this.addEvents({
            'addressReportPopulated': true,
            'editButtonClicked': true,
            'showmapdatatoggled': true,
            'showaddressclicked': true
        });
    
        var disableControls = true;
        if (this.user.isRequestor()) {
            disableControls = false;
        }

        var updateGridToolbar = function (grid) {
            var toolbars = grid.query('toolbar');
            if (toolbars.length > 0) {
                toolbars[0].update({ count: grid.store.count() });
            }
        };

        this.parcelStore = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                url: MAD.config.Urls.ParcelsForAddressReport + this.addressId,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'results'
                }
            },
            listeners: {
                'beforeload': function () {
                    this.parcelGridPanel.setLoading(true);
                },
                'load': function () {
                    this.parcelGridPanel.setLoading(false);
                    if (!this.showHistoricalData) {
                        this.cacheHistoricalData(this.parcelStore, 'address_x_parcel_retire_tms');
                    }
                },
                'datachanged': function () {
                    updateGridToolbar(this.parcelGridPanel);
                },
                scope: this
            },
            model: 'MAD.model.UnitAddressReportRecord'
        });

        var filtersCfg = {
            ftype: 'filters',
            autoReload: false, //don't reload automatically
            local: true //only filter locally
        };

        var gridToolbarCfg = {
            xtype: 'toolbar',
            style: 'font-size:10px;',
            tpl: '{count} row(s)',
            layout: 'fit',
            height: 18,
            dock: 'bottom'
        };

        this.parcelGridPanel = new Ext.grid.GridPanel({
            features : [filtersCfg],
            border: true,
            loadMask: true,
            store: this.parcelStore,
            columns: {
                defaults: { filterable: true },
                items: [
                    { header: "APN", dataIndex: 'blk_lot', flex: 5,
                        renderer: function (value) {
                            return '<a class="property-information-link" href="#">' + value + '</a>';
                        }
                    },
                    { header: "Base/Unit", dataIndex: 'address_base_flg', flex: 4, renderer: this.renderers.isBase },
                    { header: "Unit #", dataIndex: 'unit_num', flex: 4, renderer: this.renderers.unitInfo },
                    { header: "Type", dataIndex: 'unit_type_id', flex: 4, renderer: this.renderers.unitType, hidden: true },
                    { header: "Status", dataIndex: 'disposition_code', flex: 4, renderer: this.renderers.disposition, hidden: true },
                    { header: "Linked", dataIndex: 'address_x_parcel_create_tms', flex: 7,
                        renderer: function (value) {
                            var displayValue;
                            if (value === "1970-01-01 00:00:00") {
                                displayValue = 'initial load';
                            } else {
                                displayValue = '<a class="lineage-create-link" href="#">' + value + '</a>';
                            }
                            return displayValue;
                        }
                    },
                    { header: "Unlinked", dataIndex: 'address_x_parcel_retire_tms', flex: 7,
                        renderer: function (value) {
                            return value ? '<a class="lineage-retire-link" href="#">' + value + '</a>' : '';
                        }
                    }
                ]
            },
            title: 'Parcels',
            forceFit: true,
            viewConfig: {
                emptyText: '<div class="x-grid-empty">No records found.</div>',
                deferEmptyText: true
            },
            dockedItems: [gridToolbarCfg],
            listeners: {
                'itemclick': function (view, record, item, index, e) {
                    var className = e.getTarget().className;
                    if (className === "property-information-link") {
                        // include retired
                        this.fireEvent("searchitemselected", "ParcelSearch", record.get('blk_lot'), true);
                    } else if (className === 'lineage-create-link' || className === 'lineage-retire-link') {
                        this.fireEvent("lineagelinkclicked", record.get('address_x_parcel_id'), className==='lineage-create-link'?'create':'retire');
                    }
                },
                scope: this
            }
        });

        this.unitAddressStore = Ext.create('Ext.data.Store', {
            proxy: {
                type: 'ajax',
                url: MAD.config.Urls.UnitsForAddressReport + this.addressId,
                method: 'GET',
                reader: {
                    type: 'json',
                    root: 'results'
                }
            },
            listeners: {
                'beforeload': function () {
                    this.unitsGridPanel.setLoading(true);
                },
                'load': function () {
                    this.unitsGridPanel.setLoading(false);
                    if (!this.showHistoricalData) {
                        this.cacheHistoricalData(this.unitAddressStore, 'retire_tms');
                    }
                },
                'datachanged': function () {
                    updateGridToolbar(this.unitsGridPanel);
                },
                scope: this
            },
            model: 'MAD.model.UnitAddressReportRecord'
        });

        this.unitsGridPanel = new Ext.grid.GridPanel({
            features : [filtersCfg],
            border: true,
            loadMask: true,
            store: this.unitAddressStore,
            columns: {
                defaults: { filterable: true },
                items: [
                    { header: "Base/Unit", dataIndex: 'address_base_flg', flex: 4, renderer: this.renderers.isBase },
                    { header: "Unit #", dataIndex: 'unit_num', flex: 4, renderer: this.renderers.unitInfo },
                    { header: "Type", dataIndex: 'unit_type_id', flex: 4, renderer: this.renderers.unitType },
                    { header: "Status", dataIndex: 'disposition_code', flex: 4, renderer: this.renderers.disposition },
                    { header: "Created", dataIndex: 'create_tms', flex: 5 },
                    { header: "Retired", dataIndex: 'retire_tms', flex: 5 }
                ]
            },
            title: 'Units',
            forceFit: true,
            viewConfig: {
                emptyText: '<div class="x-grid-empty">No records found.</div>',
                deferEmptyText: true
            },
            dockedItems: [gridToolbarCfg]
        });

        this.unitAddressStore.sort([{
            property : 'unit_num',
            direction: 'ASC'
        }]);

        this.summary = new Ext.Panel({
            title: 'More',
            layout: 'fit',
            border: false,
            id: 'addr-report-summary',
            autoScroll: true,
            html: ''
        });

        this.headerContent = Ext.create('Ext.container.Container', {
            layout: 'fit',
            flex: 1,
            html: ''
        });

        this.linkButton = new Ext.Button({
            text: 'Link'
            ,height: 30
            ,width: 60
            ,iconCls: 'silk-link'
            ,style: 'margin: 2px 0px 8px 28px;'
            ,handler: function() {
                this.fireEvent('showaddressclicked', this.addressId);                
            }
            ,scope: this
        });

        this.showHistoryCheckbox = Ext.create('Ext.form.field.Checkbox', {
            checked: this.showHistoricalData,
            boxLabel: 'historical',
            style: 'margin-left: 8px;',
            name: 'historicdata',
            listeners: {
                change: function (checkbox, value) {
                    if (value !== this.showHistoricalData) {
                        this.toggleHistoricalData(value);
                    }
                },
                scope: this
            }
        });

        this.showMapData = Ext.create('Ext.form.field.Checkbox', {
            checked: true,
            boxLabel: 'on map',
            style: 'margin-left: 8px;',
            name: 'mapdata',
            listeners: {
                change: function (checkbox, value) {
                    this.fireEvent('showmapdatatoggled', value);
                },
                scope: this
            }
        });

        this.contentPanel = new Ext.Panel({
            layout: 'fit',
            border: false,
            dockedItems: [
                new Ext.Panel({
                    dock: 'top',
                    bodyStyle: 'padding:5px;',
                    border: false,
                    layout: {
                        type: 'hbox',
                        align: 'bottom'
                    },
                    items: [
                        this.headerContent, 
                        Ext.create('Ext.container.Container', {
                            layout: {
                                type: 'vbox',
                                align: 'right'
                            },
                            items: [
                                this.linkButton,
                                {
                                    xtype:'fieldset',
                                    title: 'Show',
                                    style: 'padding-top:3px;padding-left:3px;',
                                    layout: {
                                        type: 'vbox',
                                        align: 'left'
                                    },
                                    items :[
                                        this.showMapData,
                                        this.showHistoryCheckbox
                                    ]
                                }
                            ]
                        })
                    ]
                })
            ],
            items: [new Ext.tab.Panel({
                deferredRender: true,
                border: false,
                items: [
                    this.parcelGridPanel,
                    this.unitsGridPanel,
                    this.summary
                ]
            })]
        });

        this.messagePanel = new Ext.Panel({
            html: '<div class="address-report-loading">Loading</div>',
            layout: 'fit',
            border: false
        });

        Ext.apply(this, {
            cls: 'address-report',
            border: false,
            items: [
                this.messagePanel,
                this.contentPanel
            ]
        });

        Ext.Ajax.request({
            url: MAD.config.Urls.AddressInfo + this.addressId,
            disableCaching: true,
            success: this.populateAddressReport,
            scope: this
        });

        this.callParent(arguments);
    },

    updateHeaderContent: function() {
        this.addressJson.isRequestor = this.user.isRequestor();
        this.headerContent.update(MAD.config.Tpls.addressDetailsSearchResults.apply(this.addressJson));
    },

    populateAddressReport: function(result) {
        if (!this.isDestroyed) {
            var response = Ext.JSON.decode(result.responseText);

            var addressJson = response.returnObj;

            this.addressJson = addressJson;

            if (this.isRetired()) {
                this.showHistoricalData = true;
                this.showHistoryCheckbox.setValue(true);
            }

            this.summary.update(MAD.config.Tpls.addressReportSummary.apply(addressJson));

            this.updateHeaderContent();

            this.layout.setActiveItem(this.contentPanel);

            this.parcelStore.load();
            this.unitAddressStore.load();

            this.fireEvent('populateAddressReport', [addressJson.streetSegmentGeometry, addressJson.parcelFootprintGeometry]);
        }
    },

    toggleHistoricalData: function(showData) {
        var grids = [
            { grid: this.parcelGridPanel, retireField: 'address_x_parcel_retire_tms' },
            { grid: this.unitsGridPanel, retireField: 'retire_tms' }
        ];
        if (typeof showData === "undefined") {
            showData = !this.showHistoricalData;
        }

        Ext.each(grids, function(gridObj) {
            // Ugly code to ensure that filters stay in sync with content
            // simply removes all filters from the store and unchecks all
            // FiltersFeature menu items... please refactor
            if (gridObj.grid.filters) {
                gridObj.grid.store.clearFilter();
                gridObj.grid.filters.clearFilters();
            }

            if (showData) {
                if (gridObj.grid.store.historicalData) {
                    gridObj.grid.store.add(gridObj.grid.store.historicalData);
                }
                gridObj.grid.store.historicalData = [];
            } else {
                this.cacheHistoricalData(gridObj.grid.store, gridObj.retireField);
            }
        }, this);

        this.showHistoricalData = showData;

        // ensure checkbox is in sync with data
        if (this.showHistoryCheckbox.getValue() !== showData) {
            this.showHistoryCheckbox.setValue(showData);
        }
    },

    cacheHistoricalData: function (store, retireField) {
        var activeData = [];
        store.historicalData = [];

        store.each(function (record) {
            if (record.get(retireField)) {
                store.historicalData.push(record);
            } else {
                activeData.push(record);
            }
        }, this);

        // Here we remove all then readd the active records because
        // removing the historical data is very slow when there are 1000+
        // records, while removeAll is always fast.  Adding records in 
        // this volume is not nearly as slow
        store.removeAll();
        store.add(activeData);
    },

    // one more of these and we should push into a proper model
    isRetired: function () {
        if (this.addressJson.retire_tms) {
            return true;
        }
        return false;
    }

});