Ext.define('MAD.widgets.ApnGridPanel', {
    extend: 'Ext.grid.GridPanel',
    alias: 'widget.apnGridPanel',
    requires: ['Ext.ux.CheckColumn'],

    // Note: this widget has history of performance issues.
    autoScroll: true,
    forceFit: true,
    deferRowRender: false,

    // This store backs the UI and is of type parcelLinkChangeStore.
    store: {},

    // The parcel links that the user wants to change; the store is part of "The Model" and has a change listener.
    parcelLinkChangeStore: {},

    // The parcels that are linked to the address
    linkedParcelStore: {},

    // The universe of parcels from which the user selects; may contain retired parcels.
    parcelPickStore: {},

    // If this is an unofficial address like "0 UNKNOWN", we should not allow the creation of new parcel links but we do allow unlinking.
    allowNewParcelLinking: true,

    checkColumnsEnabled: true,

    initComponent: function () {

        this.initializeStore();

        var notifyParcelPickChanged = Ext.bind(function () {
            // When there is a change inform subscribers of the APNs that are linked or will be linked.
            var apns = this.getNetParcelPicks();
            this.fireEvent('parcelPickChange', apns);
        }, this);

        this.parcelLinkChangeStore.on('add', notifyParcelPickChanged);
        this.parcelLinkChangeStore.on('remove', notifyParcelPickChanged);
        this.on('beforedestroy', function (apnGridPanel) {
            apnGridPanel.parcelLinkChangeStore.un('add', notifyParcelPickChanged);
            apnGridPanel.parcelLinkChangeStore.un('remove', notifyParcelPickChanged);
        });

        ///// http://code.google.com/p/eas/issues/detail?id=450
        // A sort can be slow for a large data set (say 1000 rows).
        // Therefore we put a mask up while this happens.
        // There is still a problem though...the masking operation is still a little slow in coming up.
        // We should probably adopt paging here.
        // Paging would improve masking performance and rendering performance in general.
        var storeSort = this.store.sort;
        this.store.sort = Ext.bind(function (fieldName, dir) {
            this.el.mask('Sorting...', 'x-mask-loading div');
            // Use defer to get the mask on the screen before the sort drains the life out of the CPU.
            Ext.defer(storeSort, 100, this.store, [fieldName, dir]);
        }, this);
        this.store.addListener('refresh', function () {
            this.el.unmask();
        }, this);
        ///

        var that = this;

        // {header: 'APN', sortable: false, dataIndex: 'apn', width: 65, menuDisabled: true},
        // Using a renderer here is fine most of the time but in a few cases we have 1000+ rows where it may not be terribly efficient.
        // In some of these cases it appears to perform satisfactorily.
        var apnColumn = {
            header: 'APN',
            sortable: true,
            dataIndex: 'apn',
            width: 75,
            menuDisabled: true,
            renderer: function (value) {
                return '<a class="property-information-link" href="#">' + value + '</a>';
            }
        };

        var checkBoxRenderer = function (value, record, disabled) {
            var cssPrefix = Ext.baseCSSPrefix,
            cls = [cssPrefix + 'grid-checkcolumn'];

            if (value) {
                cls.push(cssPrefix + 'grid-checkcolumn-checked');
            }

            if (disabled) {
                cls.push('x-item-disabled');
            }

            return '<div class="' + cls.join(' ') + '">&#160;</div>';
        };

        // Note that the menuDisabled at the column default level seems to have no effect so set it on each column. 
        Ext.apply(this, {
            border: false
            , name: 'apn_grid_panel'
            , viewConfig: {
                listeners: {
                    refresh: function(gridview) {
                        if (!this.checkColumnsEnabled) {
                            this.disableCheckColumns();
                        }
                    },
                    scope: this
                }
            }
            , enableColumnHide: false
            , columns: [
                apnColumn,
                { header: 'Retired', sortable: true, dataIndex: 'retire_tms', width: 70, menuDisabled: true,
                    renderer: function (value, metaData, record, rowIndex, colIndex, store) {
                        return Ext.util.Format.date(value, "m/d/Y");
                    }
                },
                {         
                    xtype: 'checkcolumn',
                    header: 'Link',
                    dataIndex: 'link',
                    width: 40,
                    sortable: true,
                    menuDisabled: true,
                    tdCls: 'apn-check-column',
                    listeners: {
                        'beforecheckchange': function (column, index, checked) {
                            var record = this.store.getAt(index);
                            var disabled = this.isLinkCellDisabled(record);
                            return !this.isCheckColumnDisabled(disabled);
                        },
                        'checkchange': this.checkChange,
                        scope: this
                    },
                    renderer: function (value, metaData, record) {
                        return checkBoxRenderer(value, record, that.isLinkCellDisabled(record));
                    }
                },
                {         
                    xtype: 'checkcolumn',
                    header: 'Unlink',
                    dataIndex: 'unlink',
                    width: 40,
                    sortable: true,
                    menuDisabled: true,
                    tdCls: 'apn-check-column',
                    listeners: {
                        'beforecheckchange': function (column, index, checked) {
                            var record = this.store.getAt(index);                            
                            var disabled = this.isUnlinkCellDisabled(record);
                            return !this.isCheckColumnDisabled(disabled);
                        },
                        'checkchange': this.checkChange,
                        scope: this
                    },
                    renderer: function (value, metaData, record) {
                        return checkBoxRenderer(value, record, that.isUnlinkCellDisabled(record));
                    }
                },
                { header: 'Distance', sortable: true, dataIndex: 'distance', width: 70, menuDisabled: true }
            ],
            selModel: {
                mode: 'SINGLE'
            }
        });

        this.callParent(arguments);

        this.on({
            'itemclick': function (view, record, item, index, e) {
                if (e.getTarget().className === "property-information-link") {
                    // include retired
                    this.fireEvent("searchitemselected", "ParcelSearch", record.get('apn'), true);
                }
            },
            scope: this
        });

        this.selModel.on('selectionchange', function( selectionModel, selectedRecords, eOpts){

            // http://code.google.com/p/eas/issues/detail?id=679

            // A single row should be selected at all times.
            if (selectedRecords.length < 1) {
                selectionModel.select(selectionModel.getLastSelected());
            }

        }, this);

        this.getView().on('itemkeydown', function( gridView, record, htmlElement, rowIndex, e, eOpts ){
            // hack for Internet Exploder...
            // fixes issue with key nav where wrong record gets checkbox changed
            // http://code.google.com/p/eas/issues/detail?id=679
            if (Ext.isIE) {
                record = this.getSelectionModel().getSelection()[0];
            }
            
            if (e.getKey() !== Ext.EventObject.SPACE   ) {
                return;
            }

            if (!record) {
                return;
            }

            // Get the right field.
            var fieldName;
            if (record.get('address_x_parcel_id')) {
                fieldName = 'unlink';
            } else {
                fieldName = 'link';
            }

            // update model
            record.set(fieldName, !record.get(fieldName));
            this.synchronizeSelection(record);
            record.commit();

            // hack for Internet Exploder...
            // fixes issue where focus is lost after specific sequence of mouse/keyboard actions
            // http://code.google.com/p/eas/issues/detail?id=679
            if (Ext.isIE) {
                this.getView().focus();
            }

        }, this);

    },

    checkChange: function (column, index, checked) {
        var record = this.store.getAt(index);
        this.synchronizeSelection(record);
    },

    isLinkCellDisabled: function (record) {
        return (record.get('address_x_parcel_id') || !this.allowNewParcelLinking);
    },

    isUnlinkCellDisabled: function (record) {
        return !record.get('address_x_parcel_id');
    },

    isCheckColumnDisabled: function (disabled) {
        if (!this.checkColumnsEnabled) {
            disabled = true;
        }
        return disabled;
    },

    enableCheckColumns: function (enableCheckColumns) {
        this.checkColumnsEnabled = enableCheckColumns;
        if (this.rendered) {
            //this.getView().refresh();
            this.selModel.clearSelections();
        }
        if (enableCheckColumns) {
            this.selModel.setLocked(false);
            Ext.each(Ext.query('.apn-check-column .x-grid-cell-inner'), function(htmlEl) {
                var cell = Ext.get(htmlEl);
                // Apply background style in IE instead of using mask() method
                // because performance on mask against many rows in IE is terrible!
                if (Ext.isIE) {
                    cell.applyStyles('background-color: transparent;');
                } else {
                    cell.unmask();
                }
            });
        } else {
            this.disableCheckColumns();
        }
    },

    disableCheckColumns: function () {
        this.selModel.setLocked(true);
        Ext.each(Ext.query('.apn-check-column .x-grid-cell-inner'), function(htmlEl) {
            var cell = Ext.get(htmlEl);
            // Apply background style in IE instead of using mask() method
            // because performance on mask against many rows in IE is terrible!
            if (Ext.isIE) {
                cell.applyStyles('background-color: #f5f5f5;');
            } else {
                cell.mask();
            }
        });
    },

    // This class occasionally needs some debugging; this should help.
    consoleLogRecord: function (record, fields) {
        var logString = '';
        var i, f;
        for (i = 0; i < fields.length; i++) {
            f = fields[i];
            logString += ' ' + f + ': ' + record.get(f);
        }
        logString += '\n';
        console.log(logString);
    },

    initializeStore: function () {

        // Performance is important.

        this.store = new MAD.model.ParcelLinkChangeStore();

        // 1) initially populate from parcelPickStore
        //console.log("----- initially populate from parcelPickStore ------");
        this.parcelPickStore.each(function (parcelPickRecord) {
            var newParcelLinkChangeRecord = this.store.addDefaultRecord();
            newParcelLinkChangeRecord.set('apn', parcelPickRecord.get('apn'));
            newParcelLinkChangeRecord.set('retire_tms', parcelPickRecord.get('retire_tms'));
            newParcelLinkChangeRecord.set('distance', parcelPickRecord.get('distance'));
            //this.consoleLogRecord(parcelPickRecord, ['apn', 'retire_tms', 'distance']);
        }, this);

        // 2) synchronize with linkedParcelStore
        //console.log("----- synchronize with linkedParcelStore -----");
        this.linkedParcelStore.each(function (linkedParcelRecord) {
            var index = this.store.findBy(function (record, id) {
                return record.get('apn') === linkedParcelRecord.get('apn');
            }, this);
            if (index !== -1) {
                var record = this.store.getAt(index);
                record.set('link', true);
                record.set('address_x_parcel_id', linkedParcelRecord.get('address_x_parcel_id'));
                //this.consoleLogRecord(record, ['apn', 'link', 'address_x_parcel_id']);
            }
        }, this);

        // 3) synchronize with parcelLinkChangeStore
        //console.log("----- synchronize with parcelLinkChangeStore -----");
        this.parcelLinkChangeStore.each(function (parcelLinkChangeRecord) {
            var index = this.store.findBy(function (record, id) {
                return record.get('apn') === parcelLinkChangeRecord.get('apn');
            }, this);
            if (index !== -1) {
                var record = this.store.getAt(index);
                record.set('link', parcelLinkChangeRecord.get('link'));
                record.set('unlink', parcelLinkChangeRecord.get('unlink'));
                record.set('address_x_parcel_id', parcelLinkChangeRecord.get('address_x_parcel_id'));
                //this.consoleLogRecord(record, ['apn', 'link', 'unlink', 'address_x_parcel_id']);
            }
        }, this);

        // 4) remove rows where the parcel is retired and it is not linked
        //console.log("----- remove rows where the parcel is retired and it is not linked -----");
        var recordsToRemove = new Array();
        this.store.each(function (record) {
            if (record.get('retire_tms') !== null && record.get('link') !== true) {
                recordsToRemove.push(record);
                //this.consoleLogRecord(record, ['apn', 'retire_tms', 'link']);
            }
        }, this);
        this.store.remove(recordsToRemove);

        this.store.each(function(record){
            record.commit();
        });
        this.store.sort('distance', 'ASC');

    },

    getNetParcelPicks: function () {
        var apns = this.linkedParcelStore.collect('apn');

        this.parcelLinkChangeStore.each(function (parcelLinkChangeRecord) {
            if (parcelLinkChangeRecord.get('link') && parcelLinkChangeRecord.get('unlink')) {
                Ext.Array.remove(apns, parcelLinkChangeRecord.get('apn'));
            } else if (parcelLinkChangeRecord.get('link') && !parcelLinkChangeRecord.get('unlink')) {
                apns.push(parcelLinkChangeRecord.get('apn'));
            }
        });

        return apns;
    },

    synchronizeSelection: function (parcelLinkChangeRecord) {
        var addressXParcelId = parcelLinkChangeRecord.get('address_x_parcel_id');
        var apn = parcelLinkChangeRecord.get('apn');
        var index = this.parcelLinkChangeStore.findBy(function (record, id) {
            return record.get('apn') === apn;
        });
        var newParcelLinkChangeRecord;
        if (addressXParcelId) {
            // An existing link.
            if (index === -1) {
                if (parcelLinkChangeRecord.get('unlink') === true) {
                    this.parcelLinkChangeStore.add(parcelLinkChangeRecord.copy());
                }
            } else {
                this.parcelLinkChangeStore.remove(this.parcelLinkChangeStore.getAt(index));
            }
        } else {
            // A new link.
            if (index === -1) {
                if (parcelLinkChangeRecord.get('link') === true) {
                    this.parcelLinkChangeStore.add(parcelLinkChangeRecord.copy());
                }
            } else {
                this.parcelLinkChangeStore.remove(this.parcelLinkChangeStore.getAt(index));
            }
        }
        parcelLinkChangeRecord.commit();
        //console.log('parcel link change records');
        //this.parcelLinkChangeStore.each(function(parcelLinkChangeRecord){
        //    this.consoleLogRecord(parcelLinkChangeRecord, ['apn', 'link', 'unlink']);
        //});

    }
});
