Ext.define('MAD.widgets.UnitAddressGridPanel', {
    extend: 'Ext.grid.GridPanel',
    alias: 'widget.unitAddressGridPanel',
    cls: 'unit-address-panel',

    // todo - add conditional behavior and style based on row state (new, existing)

    initComponent: function () {
        var cols = [
            {
                xtype: 'actioncolumn',
                header: 'Remove',
                width: 60,
                sortable: false,
                dataIndex: 0,
                fixed: true,
                align: 'center',
                menuDisabled: true,
                items: [{
                    getClass: function(v, meta, record) {
                        if (record.getStatusString() == 'new') {
                            // this.items[0].tooltip = 'Remove Unit?';
                            return 'silk-delete action-icon';
                        } else {
                            return '';
                        } 
                    },
                    handler: function (grid, rowIndex, colIndex) {
                        var store = grid.getStore();
                        var record = store.getAt(rowIndex);
                        var selection = this.getSelectionModel().getSelection()[0];
                        if (record.getStatusString() == 'new') {
                            grid.getStore().remove(record);
                            this.fireEvent('unitAddressRemoved', grid, record, selection);
                        }

                    },
                    scope: this
                }]
            },
            { 
                header: "Number", 
                sortable: true, 
                dataIndex: 'address_num',
                menuDisabled: true
            },
            {
                header: "Edit/Retire/New",
                sortable: true,
                dataIndex: 'retire_flg',
                menuDisabled: true,
                renderer: function (v, p, record, rowIndex) {
                    return record.getStatusString();
                }
            },
            {
                header: "Validation",
                sortable: true,
                dataIndex: 'validation_message',
                tdCls: 'validation-column',
                menuDisabled: true,
                renderer: function (value, p, record) {
                    return Ext.String.format('<div class="{0}">{1}</div>', record.data.exception_css_class, record.data.exception_message);
                }
            }
        ];

        Ext.apply(this, {
            name: 'unit_address_grid',
            height: 250,
            border: false,
            autoScroll: true,
            columns: cols,
            selModel: {
                mode: 'SINGLE',
                // It is sad to disable key nav, but...
                // we so because the UI can't keep up with the updates on larger addresses like 1000 pine.
                // One day we will completely change the UI so this will not be a problem
                enableKeyNav: false
            },
            forceFit: true
        });

        this.addEvents('unitAddressSelected');
        this.addEvents('unitAddressRemoved');

        this.on({
            "selectionchange": function (selectionModel, records) {
                if (records.length >0) {
                    this.fireEvent('unitAddressSelected', this, records[0]);
                }
            },
            scope: this
        });

        this.callParent(arguments);
    }
});