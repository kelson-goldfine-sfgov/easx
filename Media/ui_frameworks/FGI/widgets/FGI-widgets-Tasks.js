﻿Ext.define('FGI.widgets.Tasks', {
    extend: 'Ext.panel.Panel',

    // Constructor Defaults, can be overridden by user's config object

    configItems: [],

    /**
    * Adds a new accordian panel to the Tasks panel
    * @param {Ext.Panel} panel: The panel to add
    */
    addPanel: function (panel) {
        if (panel.closable) {
            if (!panel.tools) {
                panel.tools = [];
            }
            panel.tools.push({
                id: 'close',
                handler: function (e, target, panel) {
                    parent = panel.ownerCt;
                    parent.remove(panel, true);
                    parent.doLayout(true);
                }
            });
        }
        this.add(panel);
        this.doLayout(true);
    },

    /**
    * Removes a panel from the Tasks panel
    * @param {String} panelId: The id of the panel to remove
    */
    closePanel: function (panelId) {
        panel = this.getComponent(panelId);
        panel.ownerCt.remove(panel, true);
        this.doLayout(true);
    },

    /**
    * Expands a panel
    * @param {String} panelId: The id of the panel to expand
    */
    expandPanel: function (panelId) {
        panel = this.getComponent(panelId);
        panel.expand();
    },

    /**
    * Collapses a panel
    * @param {String} panelId: The id of the panel to collapse
    */
    collapsePanel: function (panelId) {
        panel = this.getComponent(panelId);
        panel.collapse();
    },


    initComponent: function () {

        Ext.apply(this, {
            //non-overidable objects
            layout: 'accordion',
            border: false,
            items: this.configItems
        });

        // Adds panels to the Tasks panel based on the configItems array
        Ext.each(this.configItems, function (item, index, allItems) {
            if (item.closable) {
                if (!item.tools) {
                    item.tools = [];
                }
                item.tools.push({
                    id: 'close',
                    handler: function (e, target, panel) {
                        parent = panel.ownerCt;
                        parent.remove(panel, true);
                        parent.doLayout(true);
                    }
                });
            }
        });

        this.callParent(arguments);

    }

});
 