﻿Ext.define('FGI.widgets.LoginForm', {
    extend: 'Ext.form.Panel',

    url: '',

    frame: true,

    title: 'Sign In',

    usernameLabel: 'Login',

    passwordLabel: 'Password',

    passwordReminderText: 'I forgot my password',

    passwordErrorText: 'Please confirm your username and password',

    loginMaskText: 'Logging you in...',

    initComponent: function () {

        // Events for clicking on the "forgot password" link and a successful login
        this.addEvents({
            'passwordhelpclicked': true,
            'loginsucceeded': true
        });

        this.submitButton = Ext.create('Ext.button.Button', {
            text: 'Login',
            scope: this,
            handler: this.submit,
            formBind: true
        });

        this.cancelButton = Ext.create('Ext.button.Button', {
            text: 'Cancel',
            scope: this,
            handler: this.cancel
        });

        this.forgotPasswordButton = Ext.create('Ext.button.Button', {
            text: this.passwordReminderText,
            scope: this,
            handler: function () {
                this.fireEvent('passwordhelpclicked', this);
                this.ownerCt.hide();
            }
        });

        var textFieldListeners = {
            'keydown': function(textField, event, eventOptions) {
                if (event.getKey() === event.ESC) {
                    this.cancel();
                } else if (event.getKey() === event.RETURN) {
                    this.submit();
                }
            },
            scope: this
        };

        Ext.apply(this, {
            defaults: {
                msgTarget: 'side',
                enableKeyEvents: true,
                width: 300
            },
            defaultType: 'textfield',
            bodyStyle: 'padding:5px 5px 0',
            buttonAlign: 'left',
            buttons: [
                this.forgotPasswordButton,
                "->",
				this.submitButton,
                this.cancelButton
			],
            items: [
                {
                    fieldLabel: this.usernameLabel,
                    name: 'name',
                    allowBlank: false,
                    listeners: textFieldListeners
                }, {
                    fieldLabel: this.passwordLabel,
                    name: 'password',
                    allowBlank: false,
                    inputType: 'password',
                    listeners: textFieldListeners
                }
            ]
        });

        this.callParent(arguments);

    },

    cancel: function() {
        this.ownerCt.hide();
    },

    submit: function() {
        var mask = new Ext.LoadMask(this.ownerCt.getEl(), { msg: this.loginMaskText });
        mask.show();
        this.submitButton.disable();
        this.form.submit({
            url: this.url,
            method: 'POST',
            success: function (theForm, responseObj) {
                mask.hide();
                theForm.findField('password').setValue('');
                this.submitButton.enable();
                this.ownerCt.hide();
                this.fireEvent('loginsucceeded', responseObj);
            },
            failure: function (theForm, responseObj) {
                var window;
                mask.hide();
                var passwordField = theForm.findField('password');
                passwordField.setValue('');
                if (responseObj.failureType == 'client') {
                    window = Ext.MessageBox.alert('Authentication Unsuccessful', this.passwordErrorText);
                } else {
                    window = Ext.MessageBox.alert('Authentication Unsuccessful', responseObj.result.message);
                }

                window.on({
                    hide: function () {
                        passwordField.focus(true, 10);
                        theForm.clearInvalid();
                    },
                    scope: this,
                    single: true
                });

            },
            scope: this
        });
    }
});
