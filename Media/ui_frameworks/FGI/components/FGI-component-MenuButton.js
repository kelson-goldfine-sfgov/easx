﻿/**
* @class FGI.component.MenuButton
* @extends Ext.Button
* Simple Menu Button class that extends the Ext.Button class.  Allows you to create an Ext.Button 
* with a similar look to a menu item(using an Ext.Action for example)
* @cfg {String} text The menu text
* @cfg {Object} menuCfg accepts an object with any combination of the following three properties to use the
* default menu button template: icon, text, header.
* @cfg {String} icon The path to an image to display in the link (the image will be set as the background-image
* CSS property of the button by default, so if you want a mixed icon/text button, set cls:"x-btn-text-icon")
* @cfg {Function} handler A function called when the link is clicked (can be used instead of click event)
* @cfg {Object} scope The scope of the handler
* @cfg {Number} minWidth The minimum width for this link (used to give a set of links a common width)
* @cfg {String/Object} tooltip The tooltip for the link - can be a string or QuickTips config object
* @cfg {Boolean} hidden True to start hidden (defaults to false)
* @cfg {Boolean} disabled True to start disabled (defaults to false)
* @cfg {Boolean} pressed True to start pressed (only if enableToggle = true)
* @cfg {String} toggleGroup The group this toggle button is a member of (only 1 per group can be pressed, only
* applies if enableToggle = true)
* @cfg {Boolean/Object} repeat True to repeat fire the click event while the mouse is down. This can also be
an {@link Ext.util.ClickRepeater} config object (defaults to false).
* @constructor
* Create a new menu button
* @param {Object} config The config object
*/
Ext.define('FGI.component.MenuButton', {
    extend: 'Ext.button.Button',

    // anchor tag based template
    template: new Ext.Template('<table class="x-btn x-menu-btn-wrap"><tbody><tr><td><a class="x-btn x-menu-btn-text">{0}</a></td></tr></tbody></table>'),

    menuCfg: null,

    // private
    onRender: function(ct, position) {
        if (this.menuCfg == null) {
            var templateArgs = [this.text || '&#160;'];
        }
        else {
            if (this.menuCfg.icon != undefined && this.menuCfg.header != undefined && this.menuCfg.text != undefined) {
                var templateArgs = [this.menuCfg.icon, this.menuCfg.header, this.menuCfg.text];
                this.template = new Ext.Template('<table class="x-btn x-menu-btn-wrap"><tbody><tr><td><a class="x-btn x-menu-btn-text">',
                    '<table><tbody><tr><td style="width: 50px;"><img src="{0}"/></td>',
                    '<td valign="top" style="width:140px;">',
                    '<div style="font-weight: bolder; color: rgb(21, 66, 139);">{1}</div>',
                    '<div>{2}</div>',
                    '</td></tr></tbody></table>',
                    '</a></td></tr></tbody></table>');
            }
            else if (this.menuCfg.header != undefined && this.menuCfg.text != undefined) {
                var templateArgs = [this.menuCfg.header, this.menuCfg.text];
                this.template = new Ext.Template('<table class="x-btn x-menu-btn-wrap"><tbody><tr><td><a class="x-btn x-menu-btn-text">',
                    '<table><tbody><tr>',
                    '<td valign="top" style="width:140px;">',
                    '<div style="font-weight: bolder; color: rgb(21, 66, 139);">{0}</div>',
                    '<div>{1}</div>',
                    '</td></tr></tbody></table>',
                    '</a></td></tr></tbody></table>');
            }
            else if (this.menuCfg.icon != undefined && this.menuCfg.header != undefined) {
                var templateArgs = [this.menuCfg.icon, this.menuCfg.header];
                this.template = new Ext.Template('<table class="x-btn x-menu-btn-wrap"><tbody><tr><td><a class="x-btn x-menu-btn-text">',
                    '<table><tbody><tr><td style=""><img src="{0}"/></td>',
                    '<td valign="top" style="">',
                    '<div style="font-weight: bolder; color: rgb(21, 66, 139);">{1}</div>',
                    '</td></tr></tbody></table>',
                    '</a></td></tr></tbody></table>');
            }
            else if (this.menuCfg.icon != undefined && this.menuCfg.text != undefined) {
                var templateArgs = [this.menuCfg.icon, this.menuCfg.text];
                this.template = new Ext.Template('<table class="x-btn x-menu-btn-wrap"><tbody><tr><td><a class="x-btn x-menu-btn-text">',
                    '<table><tbody><tr><td style="width: 50px;"><img src="{0}"/></td>',
                    '<td valign="top" style="width:140px;">{1}</td></tr></tbody></table>',
                    '</a></td></tr></tbody></table>');
            }
            else if (this.menuCfg.icon != undefined) {
                var templateArgs = ['<img src="' + this.menuCfg.icon + '"/>'];
            }
            else if (this.menuCfg.text != undefined) {
                var templateArgs = [this.menuCfg.text];
            }
            else if (this.menuCfg.header != undefined) {
                var templateArgs = ['<span style="font-weight: bolder; color: rgb(21, 66, 139);">' + this.menuCfg.header + '</span>'];
            }
            else {
                var templateArgs = [this.text || '&#160;'];
            }
        }
        if (position) {
            var btn = this.template.insertBefore(position, templateArgs, true);
        } else {
            var btn = this.template.append(ct, templateArgs, true);
        }

        this.initButtonEl(btn);

        if (this.menu) {
            this.el.child(this.menuClassTarget).addClass("x-btn-with-menu");
        }
    },

    // private
    initButtonEl: function(btn) {

        this.el = btn;

        if (this.icon) {
            btn.setStyle('background-image', 'url(' + this.icon + ')');
        }
        if (this.iconCls) {
            btn.addClass(this.iconCls);
            if (!this.cls) {
                btn.addClass(this.text ? 'x-menu-btn-text-icon' : 'x-menu-btn-icon');
            }
        }
        if (this.tabIndex !== undefined) {
            btn.dom.tabIndex = this.tabIndex;
        }
        if (this.tooltip) {
            if (typeof this.tooltip == 'object') {
                Ext.QuickTips.register(Ext.apply({
                    target: btn.id
                }, this.tooltip));
            } else {
                btn.dom[this.tooltipType] = this.tooltip;
            }
        }

        if (this.pressed) {
            this.el.addClass("x-btn-pressed");
        }

        if (this.handleMouseEvents) {
            btn.on("mouseover", this.onMouseOver, this);
            // new functionality for monitoring on the document level
            //btn.on("mouseout", this.onMouseOut, this);
            btn.on("mousedown", this.onMouseDown, this);
        }

        if (this.menu) {
            this.menu.on("show", this.onMenuShow, this);
            this.menu.on("hide", this.onMenuHide, this);
        }

        if (this.id) {
            this.el.dom.id = this.el.id = this.id;
        }

        if (this.repeat) {
            var repeater = new Ext.util.ClickRepeater(btn,
                typeof this.repeat == "object" ? this.repeat : {}
            );
            repeater.on("click", this.onClick, this);
        }

        btn.on(this.clickEvent, this.onClick, this);
    }


});