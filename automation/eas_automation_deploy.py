import sys
import os
import subprocess
import copy
import shutil
import urllib
import tarfile
from StringIO import StringIO


def usage():
    print 'usage: %s localTargetPath' % sys.argv[0]
    exit(1)


def initVersionControlParms(description, versionControlDict, versionControlDictOrder):
    import getpass
    message = '\nPlease enter values for %s' % description
    print message
    print '-' * len(message)
    for k in versionControlDictOrder:
        if k == 'PASSWORD':

            # Apparently, the following function prompts the user with the
            # string "Password", which some may find annoying because the other
            # prompts are all upper-case.

            capturedValue = getpass.getpass()
        else:
            sys.stdout.write('%s (default = "%s"):' % (k,versionControlDict[k]))
            capturedValue = raw_input()
        if capturedValue:
            versionControlDict[k] = capturedValue
    print '\n'


def bitbucketExport(targetPath, team, project, branch, user, password):

    # Create deployment workaround for TLS 1.2 compatibility problem
    # https://bitbucket.org/sfgovdt/eas/issues/261
    #
    # All of the "debugging print points" are commented out with three
    # comment characters (###), and the "original" code (before the workaround)
    # is commented out with a single comment character (#).  The idea is that
    # we may want to easily test, or back-out, the workarounds later.
 
    ###print "In the function bitbucketExport()"
    ###print "  targetPath      = " + targetPath
    #url = 'https://%s:%s@bitbucket.org/%s/%s/get/%s.tar.gz' % (user, password, team, project, branch)
    tarFileName = '%s-%s-%s.tar.gz' % (team, project, branch)
    #tarFileName = '%s-%s-%s' % (team, project, branch)
    ###print "  tarFileName     = " + tarFileName
    tarFileFullPath = os.path.join(targetPath, tarFileName)
    ###print "  tarFileFullPath = " + tarFileFullPath
    ###print
    ###sys.exit()
    #tfile = tarfile.open(mode="r:gz",fileobj=StringIO(urllib.urlopen(url).read()))
    tfile = tarfile.open(name=tarFileFullPath, mode="r:gz")
    tfile.extractall(targetPath)
    tempSrcFolder = None
    for dirItemName in os.listdir(targetPath):
        ###print "  dirItemName     = " + dirItemName
        expectedDirPrefix = '%s-%s' % (team, project)
        if expectedDirPrefix in dirItemName:

            # We only care about one item in this directory, and it has to
            # have the right name prefix, and ...

            dirItemPath = os.path.join(targetPath, dirItemName)
            ###print "  dirItemPath     = " + dirItemPath
            dirItemIsDirectory = os.path.isdir(dirItemPath)

            if dirItemIsDirectory:
                # ... it has to be a directory.
                tempSrcFolder = os.path.join(targetPath, dirItemName)
                ###print "  tempSrcFolder   = " + tempSrcFolder
                break

    if tempSrcFolder is None:
        raise Exception('Unable to locate an extracted repository archive.')
    else:
        return tempSrcFolder


def main():
    if len(sys.argv) < 2:
        usage()
    elif len(sys.argv) == 2:
        LOCAL_TARGET_PATH = sys.argv[1]
    else:
        usage()

    EAS_BRANCH = 'EAS_BRANCH'
    CONFIG_BRANCH = 'CONFIG_BRANCH'

    bitbucketParms = {'USER': None, 'PASSWORD': None, 'TEAM': 'sfgovdt', EAS_BRANCH: 'master', CONFIG_BRANCH: 'master'}

    # Using this list to mimic an ordered dict as we are using Python 2.5 on the automation node currently.
    bitbucketParmsOrder = ['USER', 'PASSWORD', 'TEAM', EAS_BRANCH, CONFIG_BRANCH]
    initVersionControlParms('BITBUCKET', bitbucketParms, bitbucketParmsOrder)

    print "Extracting the " + EAS_BRANCH + " repository archive"
    easTempPath = bitbucketExport(os.path.join(LOCAL_TARGET_PATH, 'temp'), bitbucketParms['TEAM'], 'easx', bitbucketParms[EAS_BRANCH], bitbucketParms['USER'], bitbucketParms['PASSWORD'])
    ###print "easTempPath    = " + easTempPath

    print "Extracting the " + CONFIG_BRANCH + " repository archive"
    configTempPath = bitbucketExport(os.path.join(LOCAL_TARGET_PATH, 'temp'), bitbucketParms['TEAM'], 'easconfigxsf', bitbucketParms[CONFIG_BRANCH], bitbucketParms['USER'], bitbucketParms['PASSWORD'])
    ###print "configTempPath = " + configTempPath
    # sys.exit()

    print "Deploying code"
    shutil.move(os.path.join(easTempPath, 'database'), os.path.join(LOCAL_TARGET_PATH, 'database'))
    shutil.move(os.path.join(easTempPath, 'automation'), os.path.join(LOCAL_TARGET_PATH, 'automation'))
    shutil.move(os.path.join(configTempPath, 'automation/fme_workspaces/live'), os.path.join(LOCAL_TARGET_PATH, 'automation/fme_workspaces/live'))
    shutil.move(os.path.join(configTempPath, 'automation/src/config/live'), os.path.join(LOCAL_TARGET_PATH, 'automation/src/config/live'))
    
    print "Cleaning up"
    shutil.rmtree(os.path.join(LOCAL_TARGET_PATH, 'temp'))
    print "Done"

if __name__ == "__main__":
    main()

