@echo off

set ACTION=%1
set JOB=%2
set ENV=%3

set PYTHON_EXE="C:\python25\python.exe"
set PYTHON_SCRIPT="C:\apps\eas_automation\automation\src\job.py"
set LOG_FILE="C:\apps\eas_automation\app_data\logs\%JOB%_%ACTION%.out"

REM This cmd line idiom may be better for scheduled tasks.
REM See very bottom of:
REM    http://forums11.itrc.hp.com/service/forums/questionanswer.do?admit=109447626+1274726302016+28353475&threadId=986884
cmd.exe /C "%PYTHON_EXE% %PYTHON_SCRIPT% --env %ENV% --job %JOB% --action %ACTION% > %LOG_FILE% 2>&1"
