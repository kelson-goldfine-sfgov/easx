
import os
from job import *
from fme_server_helper import FmeServerHelper
import subprocess
from config.live.config_paths import *
from config.live.config_jobs import *
import sys
import datetime
import time
import psycopg2
import psycopg2.extensions
import simplejson as json
import re
import urllib
import httplib

# todo ?
# fix usability flaw described here
# Do not use the "job.env" parmameter in the "__init__" function - it will be None.
# It becomes DEV or QA or PROD once we are in the "execute" function.
#
# todo ?
# Should we need a better way to handle the notification data (emails), I would recommend adding an etl_command table
# where we would log the activites of each command. FK would be the job_uuid.
#
# Uses the command pattern.
# The command processor is in the Job class.
#
# Commands
#   - must implement execute
#
# Gateway Commands
#   - can stop command processing due to some condition they encountered
#
# XaCommands:
#  - are used to support a two phase commit
#  - names should end in XaCommand
#  - are transactional
#  - use the transaction resources stored in the Job
#  - use sqlalchemy for db work
#


class FmeCommand:


    # You can specify as many connectionGroups as you want - but you have to configure the fme workspace accordingly.
    # e.g. parms must be defined as follows:
    #    c1_host, c1_db, c1_port, c1_etc
    #    c2_host, c2_db, c2_port, c2_etc
    #    c3_etc ...
    #
    # This provides the FME developer the freedom to use FME as intended.


    def __init__(self, job=None, fmw=None, connectionGroups=(), useSdePorts=(), files=() ):

        self.type = 'FME Workspace Command'
        self.job = job
        self.fmw = fmw
        self.connectionGroups = connectionGroups
        self.files = files
        self.useSdePorts = useSdePorts
        self.fme_exe  = config_paths.getFmeExePath()
        self.fme_workspaceDir = config_paths.getFmeWorkspacePath()


    def __str__(self):
       return  'jobName: %s, workspace: %s, dbConnectionGroups: %s, files: %s, useSdePorts: %s' % (self.job.jobName, self.fmw, str(self.connectionGroups), str(self.files), self.useSdePorts)

    def createFmeParm(self, key, value):
        return ' --%s "%s" ' % (key, value)

    def execute(self):
        print str(datetime.datetime.now())
        sshClients = []
        dbConns = []
        for i, connectionGroup in enumerate(self.connectionGroups):
            sshDbPair = connectionGroup[self.job.env].sshDbPair
            sshClients.append(sshDbPair.getSshClient())
            dbConn = sshDbPair.getDbConnDef()
            dbConn.useSdePort = self.useSdePorts[i]
            dbConns.append(dbConn)

        try:
            # ssh clients may implement the "null object pattern"
            # connect and disconnect includes the tunnel
            for sshClient in sshClients:
                sshClient.connect()

            self.executeWorkspace(dbConns)
        except Exception, e:
            raise
        finally:
            for sshClient in sshClients:
                sshClient.disconnect()

    def executeWorkspace(self, dbConns):

        print 'executing workspace: %s' % self.fmw

        # The idea here is that each fme workspace will have granular paramterized connections.
        # Therefore, order is relavant. The order is established in the job_configs
        # e.g. c1_host, c1_port, ...
        fmeParmsString = ''
        for i, dbConn in enumerate(dbConns):
            infix = 'c' + str(i+1)
            fmeParmsString += dbConn.toFmeParms(infix=infix) + ' '

        for i, file in enumerate(self.files):
            if not os.path.exists(file):
                raise IOError("file not found: %s " % file)
            key = 'f' + str(i+1)
            fmeParmsString += self.createFmeParm(key, str(file))

        # Write out the FME parameters file - which follows this form:
        # fme_workspace.fmw --parm1_name parm1_value --parm2_name parm2_value ...

        # prepare output dir - this is a little tricky because we include the path (if any) from the workspace

        outputPath = config_paths.getFmeOutputPath() + self.fmw
        outputPath = os.path.dirname(outputPath)
        if not os.path.exists(outputPath):
            # todo - capture failure
            os.makedirs(outputPath)

        # strip the path off because we should have captured that above
        fmw_basename = os.path.basename(self.fmw)
        baseName = fmw_basename.partition(".")[0]

        # prepare parm file
        parmPathDirName = os.path.join(outputPath + '/parms/')
        if not os.path.exists(parmPathDirName):
            # todo - capture failure
            os.makedirs(parmPathDirName)
        parmFilename = os.path.join(parmPathDirName, baseName + ".par")
        if os.path.exists(parmFilename):
            os.remove(parmFilename)

        # prepare workspace file
        self.fmw = os.path.join(config_paths.getFmeWorkspacePath(), self.fmw)
        if not os.path.exists(self.fmw):
            raise IOError("file not found: %s " % self.fmw)


        # prepare logging dir
        logPathDirName = os.path.join(outputPath + '/logs/')
        if not os.path.exists(logPathDirName):
            # todo - capture failure
            os.makedirs(logPathDirName)

        # prepare log file
        logFilename = os.path.join(logPathDirName, baseName + ".log")
        if os.path.exists(logFilename):
            os.remove(logFilename)

        # write the parm file
        # todo  - capture failure
        parm_file = open(parmFilename, 'w')
        # Take some care to quote the paths properly in case of the "Program Files"
        parm_file_content = """ "%s" %s --LOG_FILE "%s" """ % (self.fmw, fmeParmsString, logFilename)

        parm_file.write(parm_file_content)
        parm_file.close()

        # run the command, capture the standard output, write the output to the screen
        # The raw windows command would look like this:
        # > "c:/program files/FME/fme.exe" PARAMETER_FILE "C:/parms/county_line_sfgisStg_to_sfmaps.par"
        command = [self.fme_exe, "PARAMETER_FILE", parmFilename]
        # debug only - prints credential info!
        #print "executing FME workspace as follows"
        #print command
        print 'FME logfile is here: %s' % logFilename
        process = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        # "wait" for the process to finish.
        # Using "process.wait()" may fill the PIPE buffer if there is too much stdout.
        # To avoid this we use "process.communicate()".
        process.communicate(input=None)

        (returncode, stdout, stderr) = (process.returncode, process.stdout, process.stderr)
        for line in stdout.readlines():
            print line
        for line in stderr.readlines():
            print line

        # Use this block for debugging. It just echos log file to stdout.
        # Its a too verbose for general use; you can get the data out of the FME log files.
        if self.job.verbose:
            logFile = open(logFilename, 'r')
            for line in logFile.readlines():
                print line

        #print "fme return code: " + str(returncode)
        if returncode != 0:
            print "***** fme workspace failure *****"
            sys.exit(-1)
        else:
            print "fme workspace success"


class SecureFtpCommand:
    def __init__(self, job=None, connectionGroup=None, sourcePath=None, targetPath=None, action=None):
        self.type = 'secure ftp command (get or put)'
        self.job = job
        self.connectionGroup = connectionGroup
        self.sourcePath = sourcePath
        self.targetPath = targetPath
        self.action = action
        if self.action not in ('GET', 'PUT'):
            raise Error("SecureFtpCommand: direction parameter was %s. The parameter must be GET or PUT" % self.action)

    def get(self, sftp):
        remoteDir = os.path.dirname(self.sourcePath)
        sftp.chdir(remoteDir)
        remoteFilename = os.path.basename(self.sourcePath)
        print 'GET'
        print 'from %s' % self.sourcePath
        print 'to %s' % self.targetPath
        sftp.get(remoteFilename, self.targetPath)

    def put(self, sftp):
        remoteDir = os.path.dirname(self.targetPath)
        sftp.chdir(remoteDir)
        remoteFilename = os.path.basename(self.targetPath)
        print 'PUT'
        print 'from %s' % self.sourcePath
        print 'to %s' % self.targetPath
        sftp.put(self.sourcePath, remoteFilename)

    def execute(self):
        import paramiko
        ftpConnectionDef = self.connectionGroup[self.job.env]
        try:
            print 'logging into ' + ftpConnectionDef['host']
            transport = paramiko.Transport((ftpConnectionDef['host'], ftpConnectionDef['port']))
            transport.connect(username = ftpConnectionDef['username'], password = ftpConnectionDef['password'])
            sftp = paramiko.SFTPClient.from_transport(transport)
            if self.action == "GET":
                self.get(sftp)
            elif self.action == "PUT":
                self.put(sftp)
            print 'secure ftp complete'
        except Exception, e:
            print e
            raise
        finally:
            sftp.close()
            transport.close()


class FtpCommand:

    def __init__(self, job=None, connectionGroup=None, sourcePath=None, targetPath=None, direction=None):
        self.type = 'ftp command'
        self.job = job
        self.connectionGroup = connectionGroup
        self.sourcePath = sourcePath
        self.targetPath = targetPath
        self.direction = direction
        if self.direction not in ('GET', 'PUT'):
            raise Error("FtpCommand: direction parameter was %s. The parameter must be GET or PUT" % self.direction)

    def get(self, ftpConnection, localFileHandle):
        remoteDir = os.path.dirname(self.sourcePath)
        ftpConnection.cwd(remoteDir)
        remoteFilename = os.path.basename(self.sourcePath)
        localFileHandle = open(self.targetPath, 'wb')
        def callback(block):
            localFileHandle.write(block)
            print ".",
        print 'get'
        print 'from %s' % self.sourcePath
        print 'to %s' % self.targetPath
        ftpConnection.retrbinary('RETR ' + remoteFilename, callback)

    def put(self, ftpConnection, localFileHandle):
        targetDir = os.path.dirname(self.targetPath)
        ftpConnection.cwd(targetDir)
        print 'put'
        print 'from %s' % self.sourcePath
        print 'to %s' % self.targetPath
        ftpConnection.storbinary("stor %s" % self.targetPath, localFileHandle)

    def execute(self):
        from ftplib import FTP
        print str(datetime.datetime.now())
        ftpConnectionDef = self.connectionGroup[self.job.env]
        try:
            host = ftpConnectionDef['host']
            print 'logging in to ' + host
            ftpConnection = FTP(host)
            ftpConnection.login(ftpConnectionDef['username'], ftpConnectionDef['password'])
            if self.direction == 'GET':
                localFileHandle = open(self.targetPath, 'wb')
                self.get(ftpConnection, localFileHandle)
            else:
                localFileHandle = open(self.sourcePath, 'rb')
                self.put(ftpConnection, localFileHandle)
        except Exception, e:
            print e
            raise
        finally:
            ftpConnection.close()
            localFileHandle.close()
            print 'ftp complete'



class UnzipCommand:

    def __init__(self, job=None, sourcePath=None, targetDir=None):
        self.type = 'unzip command'
        self.job = job
        self.sourcePath = sourcePath
        self.targetDir = targetDir

    def execute(self):
        import zipfile, os, os.path
        print 'unzipping ' + self.sourcePath + ' to ' + self.targetDir
        if not os.path.exists(self.targetDir):
            os.mkdir(self.targetDir)
        zipfileObject = zipfile.ZipFile(self.sourcePath)
        for name in zipfileObject.namelist():
            try:
                outfile = None
                if name.endswith('/'):
                    os.mkdir(os.path.join(self.targetDir, name))
                else:
                    outfile = open(os.path.join(self.targetDir, name), 'wb')
                    print 'writing ' + outfile.name
                    outfile.write(zipfileObject.read(name))
            except Exception, e:
                print e
                raise
            finally:
                try: outfile.close()
                except: pass

        print 'unzip complete'


class ZipCommand:

    # archives just the files within the specified directory

    def __init__(self, job=None, sourceDir=None, targetFile=None):
        self.type = 'Zip Command'
        self.job = job
        self.sourceDir = sourceDir
        self.targetFile = targetFile

    def execute(self):
        print str(datetime.datetime.now())
        import zipfile, os, os.path
        print 'zipping \n\t%s \ninto \n\t%s' % (self.sourceDir, self.targetFile)
        try:
            origDir = os.getcwd()
            zipfileObject = zipfile.ZipFile(self.targetFile, 'w', zipfile.ZIP_DEFLATED)
            os.chdir(self.sourceDir)
            fileList = [f for f in os.listdir(self.sourceDir) if os.path.isfile(f)]
            for f in fileList:
                zipfileObject.write(f)
        except Exception, e:
            print e
            raise
        finally:
            try:
                os.chdir(origDir)
                zipfileObject.close()
            except: pass

        print 'zip complete'


class SqlCommand:

    def __init__(self, job=None, sqlStatement=None, connectionGroup=None, useTransaction=True ):

        self.type = 'SQL Command'
        self.job = job
        self.sqlStatement = sqlStatement
        self.connectionGroup = connectionGroup
        self.useTransaction = useTransaction

    def execute(self):
        print str(datetime.datetime.now())

        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()
        try:
            sshClient.connect()
            connection = dbConnDef.getConnection()
            if not self.useTransaction:
                connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
            cursor = connection.cursor()
            print 'executing sql: %s' % self.sqlStatement
            cursor.execute(self.sqlStatement)
            if self.useTransaction:
                connection.commit()
        except Exception, e:
            print e
            if self.useTransaction:
                connection.rollback()
            raise
        finally:
            cursor.close()
            connection.close()
            sshClient.disconnect()


class SqlStatementsCommand:

    # submits the contents of a file one statement at a time - separated by a semicolon

    def __init__(self, job=None, connectionGroup=None, sqlFile=None, errorCodesToIgnore=()):
        self.type = 'Sql Statements Command'
        self.job = job
        self.connectionGroup = connectionGroup
        self.sqlFile = sqlFile
        self.errorCodesToIgnore=errorCodesToIgnore

    def getSqlStatements(self):
        # Takes a SQL file and splits out separate statements.
        # SQL comment syntax allowed: --
        # SQL comment syntax not allowed: /**/
        # All of the regex lines in this function were taken from the South project
        #     http://south.aeracode.org/browser/south/db/generic.py
        # Make sure you really know regexes before trying to edit these!

        if not os.path.exists(self.sqlFile):
            raise IOError("file not found: %s " % self.sqlFile)

        # remove blank lines
        sql = ''
        sqlFile = open(self.sqlFile)
        for line in sqlFile.readlines():
            if line.strip():
                sql += line

        sqlStatements = []
        regex=r"(?mx) ([^';]* (?:'[^']*'[^';]*)*)"
        comment_regex=r"(?mx) (?:^\s*$)|(?:--.*$)"
        # strip comments
        commentFreeSql = "\n".join([x.strip().replace("%", "%%") for x in re.split(comment_regex, sql) if x.strip()])
        # grab each statement
        for sqlStatement in re.split(regex, commentFreeSql)[1:][::2]:
            sqlStatements.append(sqlStatement)
        return sqlStatements

    def execute(self):

        import string
        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()

        try:
            sshClient.connect()
            # split file into a collection of statements
            print 'processing %s' % self.sqlFile
            sqlStatements = self.getSqlStatements()
            # execute statements one at a time

            for sqlStatement in sqlStatements:
                exceptionForPropagation = None
                try:
                    # Obtain new connection for every statement because some connector vendors invalidate connection when you hit an error; in specified cases we ignore errors.
                    connection = dbConnDef.getConnection()
                    cursor = connection.cursor()
                    print "\n\nexecuting sql"
                    for line in sqlStatement.split('\n'):
                        print '\t' + line
                    dbConnDef.executeSqlStatement(cursor, sqlStatement)
                    print 'rowcount: %s' % str(cursor.rowcount)
                    connection.commit()
                    cursor.close()
                except Exception, e:
                    # ignore errors as specified by client
                    exceptionForPropagation = e
                    standardException = dbConnDef.standardizeException(e)
                    for errorCodeToIgnore in self.errorCodesToIgnore:
                        if str(standardException['code']) == errorCodeToIgnore:
                            print 'ignoring error as per ignore list: %s, %s' % (standardException['code'], standardException['message'])
                            exceptionForPropagation = None
                            break

                    if exceptionForPropagation:
                        connection.rollback()
                        raise exceptionForPropagation
                finally:
                    connection.close()

        finally:
            sshClient.disconnect()


class SqlFileCommand:

    # Submits the entire contents of the file to the DB in one call.
    # Discretion is required because some DB vendors do not allow multiple "objects" per file.
    # For example, with oracle, I have to put a proc in one file, and a trigger in another, etc, otherwise I get an ora-XXX error.
    # However, postgres is not particular here.

    def __init__(self, job=None, connectionGroup=None, sqlFile=None, errorCodesToIgnore=()):
        self.type = 'Sql File Command'
        self.job = job
        self.connectionGroup = connectionGroup
        self.sqlFile = sqlFile
        self.errorCodesToIgnore=errorCodesToIgnore

    def getSql(self):
        if not os.path.exists(self.sqlFile):
            raise IOError("file not found: %s " % self.sqlFile)
        sqlFile = open(self.sqlFile)
        sql = sqlFile.read()
        return sql

    def execute(self):

        import string
        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()
        connection = None

        try:
            sshClient.connect()
            connection = dbConnDef.getConnection()
            cursor = connection.cursor()
            try:
                print 'processing %s' % self.sqlFile
                sql = self.getSql()
                print "\n\nexecuting SQL"
                print sql
                dbConnDef.executeSqlStatement(cursor, sql)
            except Exception, e:
                ignoreException = False
                # ignore errors as specified by client
                for errorCodeToIgnore in self.errorCodesToIgnore:
                    if string.find(str(e.message), errorCodeToIgnore) > -1:
                        print 'ignoring error as per ignore list: %s' % e.message
                        ignoreException = True
                        break

                if not ignoreException:
                    raise

            connection.commit()
            cursor.close()
        except Exception, e:
            if connection:
                connection.rollback()
            raise
        finally:
            if connection:
                connection.close()
            if sshClient:
                sshClient.disconnect()


class SqlForStopJobCommand:

    def __init__(self, job=None, connectionGroup=None, sql=None):
        self.type = 'SqlForStopJobCommand'
        self.job = job
        self.connectionGroup = connectionGroup
        self.sql = sql

    def execute(self):
        print str(datetime.datetime.now())
        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()
        cursor = None
        connection = None
        try:
            sshClient.connect()
            connection = dbConnDef.getConnection()
            cursor = connection.cursor()
            print 'executing sql: %s' % self.sql
            cursor.execute(self.sql)
            if not cursor.rowcount:
                raise StopJob('no rows - stopping job')
            else:
                print 'rowcount is %s so job will proceed' % cursor.rowcount
        except Exception, e:
            print e
            raise
        finally:
            if cursor:
                cursor.close()
            if connection:
                connection.close()
            sshClient.disconnect()


class StagingStartCommand:

    # inserts a row into etl_jobs
    # captures the job id from that row

    def __init__(self, job=None, tableMappings=None, fmeJobDict=None, fmeWebConnectionGroup=None):
        self.type = 'Staging Start Command'
        self.job=job
        self.fmeJobDict=fmeJobDict
        self.tableMappings=tableMappings
        self.fmeWebConnectionGroup = fmeWebConnectionGroup

    def execute(self):
        fmeServerHelper = FmeServerHelper(host=self.fmeWebConnectionGroup[self.job.env]['host'], user=self.fmeWebConnectionGroup[self.job.env]['user'], password=self.fmeWebConnectionGroup[self.job.env]['password'])
        fmeServerJob = fmeServerHelper.getLatestCompletedJob(self.fmeJobDict)
        fmeServerJobId = fmeServerHelper.getJobId(fmeServerJob)
        for tableMapping in self.tableMappings.values():
            connectionGroup = tableMapping.connectionGroup
            connectionPair = connectionGroup[self.job.env].sshDbPair
            sshClient = connectionPair.getSshClient()
            dbConnDef = connectionPair.getDbConnDef()
            try:
                sshClient.connect()
                self.insertJobRow(dbConnDef=dbConnDef, fmeServerJobId=fmeServerJobId)
            except Exception, e:
                print e
                raise
            finally:
                sshClient.disconnect()

    def insertJobRow(self, dbConnDef=None, fmeServerJobId=None):
        dbConnection = None
        try:
            dbConnection = dbConnDef.getConnection()
            cursor = dbConnection.cursor()
            sql = """
                insert into etl_jobs (uuid, job_name, fme_server_job_id)
                select '%s', '%s', %s
            """ % (self.job.id, self.job.name, fmeServerJobId)
            #print 'executing sql'
            #print sql
            cursor.execute(sql)            
            dbConnection.commit()
        except Exception, e:
            print e
            raise
        finally:
            if dbConnection:
                dbConnection.close()


class StagingCompleteCommand:

    def __init__(self, job=None, tableMappings=None):

        self.type = 'Staging Complete Command'
        self.job=job
        self.tableMappings=tableMappings

    def execute(self):

        for tableMapping in self.tableMappings.values():
            connectionGroup = tableMapping.connectionGroup
            connectionPair = connectionGroup[self.job.env].sshDbPair
            sshClient = connectionPair.getSshClient()
            dbConnDef = connectionPair.getDbConnDef()
            tableNames = tableMapping.getStagingTableNames()
            try:
                sshClient.connect()
                self.executeGrants(dbConnDef, tableNames)
                self.updateJobRow(dbConnDef=dbConnDef)
                self.insertTableNameRows(dbConnDef=dbConnDef, tableMapping=tableMapping)
            except Exception, e:
                print e
                raise
            finally:
                sshClient.disconnect()

    def executeGrants(self, dbConnDef, tableNames):
        for tableName in tableNames:
            grantTablePermissions(dbConnDef, tableName)


    def insertTableNameRows(self, dbConnDef=None, tableMapping=None):
        print 'inserting table name rows'
        try:
            dbConnection = dbConnDef.getConnection()
            cursor = dbConnection.cursor()
            now = datetime.datetime.now()
            for stagingTableName, targetTableName in tableMapping.getTableTuples():
                sql = """
                    insert into etl_tables (job_uuid, staging_table_name, target_table_name, staged_tms)
                    select '%s', '%s', '%s', '%s'
                """ % (self.job.id, stagingTableName, targetTableName, now)
                #print 'executing sql'
                #print sql
                cursor.execute(sql)
            dbConnection.commit()
        except Exception, e:
            print e
            raise
        finally:
            dbConnection.close()

    def updateJobRow(self, dbConnDef=None):
        print 'updating job row'
        try:
            dbConnection = dbConnDef.getConnection()
            cursor = dbConnection.cursor()
            now = datetime.datetime.now()
            sql = """
                update etl_jobs
                set staged_tms = '%s'
                where uuid = '%s'
             """ % (now, self.job.id)
            #print 'executing sql'
            #print sql
            cursor.execute(sql)
            dbConnection.commit()
        except Exception, e:
            print e
            raise
        finally:
            dbConnection.close()

class PostgresPreFmeCommand:

    # todo
    # We need to make it more clear that this command is only for staging tables that have no FK constraints that prevent table drop.
    # Because we use "deleteGeometryColumnRow" this currently works only fme workspaces that "drop table".
    # This is to insure that FME inserts the right geometry column row.

    def __init__(self, job=None, connectionGroup=None, tableNames=None):

        self.type = 'Postgres Pre Fme Command'
        self.job=job
        self.connectionGroup=connectionGroup
        self.tableNames=tableNames

    def execute(self):

        print 'preparing target DB for FME load'
        #print 'making sure spatial reference system is registered in target DB'

        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()

        try:
            sshClient.connect()
            self.deleteGeometryColumnRow(dbConnDef)
        except Exception, e:
            print e
            raise
        finally:
            sshClient.disconnect()


    def deleteGeometryColumnRow(self, dbConnDef):

        for tableName in self.tableNames:
            sql = getSqlForDeleteGeometryColumnRow(tableName)
            dbConnDef.executeSql(sql)



class CopyTablesXaCommand:

    def __init__(self, job=None, tableMappings=None):

        self.type = 'Copy Tables Xa Command'
        self.job=job
        self.tableMappings=tableMappings

    def execute(self):

        # todo: separate reporting from copying

        self.job.message += '\n'
        self.job.message += '%s\n' % self.type

        for tableMapping in self.tableMappings.values():
            key = tableMapping.connectionGroup[self.job.env].name
            dbConn = self.job.commitResources[key]['DB_CONNECTION']
            try:
                for tableMappingItem in tableMapping.tableMappingItems:
                    if tableMappingItem.copyOnCommit:
                        self.truncateTable(dbConn=dbConn, table=tableMappingItem.targetTable)
                        self.copyTable(dbConn=dbConn, sourceTable=tableMappingItem.stagingTable, targetTable=tableMappingItem.targetTable)
                        self.job.message += self.getReportRow(key=key, dbConn=dbConn, table=tableMappingItem.targetTable)
            except Exception, e:
                print e
                raise


    def getReportRow(self, key, dbConn, table):
        try:
            sql = """
                select count(*) from %s;
            """ % table
            results = dbConn.execute(sql)
            rowCount = results.fetchall()[0][0]
            reportRow = '\t# rows in %s.%s: %s\n' % (key.lower(), table, rowCount)
            return reportRow
        except Exception, e:
            print e
            raise
        finally:
            results.close()


    def truncateTable(self, dbConn=None, table=None):
        try:
            sql = 'truncate table %s;' % table
            results = dbConn.execute(sql)
        except Exception, e:
            print e
            raise
        finally:
            results.close()


    def copyTable(self, dbConn=None, sourceTable=None, targetTable=None):

        pgDumpOutputPath = config_paths.getPgDumpOutputPath()
        if not os.path.exists(pgDumpOutputPath):
            os.makedirs(pgDumpOutputPath)

        file = os.path.join(pgDumpOutputPath, sourceTable + ".copy")

        sqlStatements = []
        sqlStatements.append( "copy %s to '%s';" % (sourceTable, file) )
        sqlStatements.append( "copy %s from '%s';" % (targetTable, file) )
        try:
            for sql in sqlStatements:
                results = dbConn.execute(sql)
                results.close()
        except Exception, e:
            print e
            raise
        finally:
            pass



class GetJobIdXaCommand:

    # transactional (uses job level resources)
    # uses sqlalchemy

    def __init__(self, job=None, tableMappings=None):
        self.type = 'Get Job Id Xa Command'
        self.job=job
        self.tableMappings = tableMappings

    def execute(self):
        id = None
        for tableMapping in self.tableMappings.values():
            connectionGroup = tableMapping.connectionGroup[self.job.env]
            key = connectionGroup.name
            resourceDict = self.job.commitResources[key]
            dbConn = resourceDict['DB_CONNECTION']
            try:
                if id is None:
                    id = self.getStagedJobId(dbConn, self.job.name)
                    if id is None:
                        raise StopJob('job %s is not staged on %s database in %s' % (self.job.name, key, self.job.env))
                else:
                    id2 = self.getStagedJobId(dbConn, self.job.name)
                    assert id == id2, 'inconsistent job UUID across databases - this probably means there is a flaw in design or code; assert %s == %s fails' % (id, id2)
            except Exception, e:
                raise
            finally:
                pass

        self.job.id = id

    def getStagedJobId(self, dbConn, jobName):
        try:
            sql = """
                select uuid
                from etl_jobs
                where job_name = '%s'
                and committed_tms is null
                and staged_tms = (
                    select max(staged_tms)
                    from etl_jobs
                    where job_name = '%s'
                )
                limit 1;
            """ % (self.job.name, self.job.name)

            #print 'executing sql'
            #print sql

            results = dbConn.execute(sql)
            rows = results.fetchall()
            jobId = None
            if len(rows) == 1:
                jobId = rows[0][0]
            return jobId

        except Exception, e:
            print e
            raise
        finally:
            results.close()




class SetJobCommitedXaCommand:

    # transactional (uses job level resources)
    # gateway command (can stop command processing)
    # uses sqlalchemy

    def __init__(self, job=None, tableMappings=None):
        self.type = 'Update Job Xa Command'
        self.job=job
        self.tableMappings = tableMappings

    def execute(self):
        try:
            for tableMapping in self.tableMappings.values():
                key = tableMapping.connectionGroup[self.job.env].name
                dbConn = self.job.commitResources[key]['DB_CONNECTION']
                self.updateJobRow(dbConn)
        except Exception, e:
            print e
            raise


    def updateJobRow(self, dbConn):
        print 'updating job row - setting to committed'
        try:
            now = datetime.datetime.now()
            sql = """
                update etl_jobs
                set committed_tms = '%s'
                where uuid = '%s';
            """ % (now, self.job.id)

            #print 'executing sql'
            #print sql
            results = dbConn.execute(sql)
            # todo - assert that we updated one row
        except Exception, e:
            print e
            raise
        finally:
            results.close()



class SendEmailXaCommand:

    # non transactional - but still uses job level resources
    # todo - refactor as suggested by the following criticism
    # We are now seeing warts in the design because the name of the class here is FooXaCommand, but it is not transactional.
    # Rather it shares the behavior of how it obtains the database connections - which is from it's job.
    # uses sqlalchemy

    def __init__(self, job=None):
        self.type = 'Send Email Xa Command'
        self.job=job

    def execute(self):
        
        exceptionReportText = None
        try:
            for key, resourceDict in self.job.commitResources.iteritems():
                dbConn = resourceDict['DB_CONNECTION']
                jobSummaryReportText = self.job.message
                jobExceptionReportText = self.getJobExceptionReport(dbConn)
        except Exception, e:
            print e
            raise

        # send email notification
        emailSubject = '%s Load Report ' % self.job.name.title()
        if jobExceptionReportText:
            emailSubject += ' (exceptions)'
        emailText = emailSubject + '\n'
        emailText += jobSummaryReportText + '\n'
        if exceptionReportText:
            emailText += str(exceptionReportText) + '\n'

        import utils
        utils.sendEmail(self.job.env, emailSubject, emailText)

    def getJobExceptionReport(self, dbConn):
        print 'getting job exception report'
        try:
            sql = """
                select message
                from etl_exceptions
                where job_uuid = '%s';
            """ % (self.job.id)
            #print 'executing sql'
            #print sql
            results = dbConn.execute(sql)
            rows = results.fetchall()
            reportString = ''
            for row in rows:
                reportString += row[0] + '\n'
            return reportString
        except Exception, e:
            print e
            raise
        finally:
            results.close()


# keep for a post commit
#def executeVacuumAnalyze(self, dbConnDef, tableNames):
#    # VACUUM ANALYZE cannot be run like plain old sql - must be run without a transaction
#    print 'running db vacuum analyze'
#    try:
#        connection = dbConnDef.getConnection()
#        connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
#        cursor = connection.cursor()
#        for tableName in tableNames:
#            sql = "VACUUM ANALYZE %s" % tableName
#            #print "executing sql"
#            #print sql
#            cursor.execute(sql)
#
#        connection.close()
#
#    except Exception, e:
#        print 'exception while trying to vacuum analyze'
#        print e
#        raise
            


#####
# Keep this code here for now - we'll use it for db backup script.
#            
# If we use the BackupDatabaseCommand, we have a deploy headache because this package must be deployed on the DB
# server.... whcih means that the config file has to fussed with, python versions installed , etc.
# Better I think to simply back up the db, record the date of the backup in the db.
# Then, before we run the bigger etl jobs, we check to see if the back up has occurred.         
#class BackupDatabaseCommand:
#
#    def __init__(self, job=None, oncePerDay=False, connectionGroup=None):
#        self.type = 'Backup Database Command'
#        self.job=job
#        self.oncePerDay = oncePerDay
#        self.connectionGroup=connectionGroup
#
#    def execute(self):
#
#        # This will be running on the database server itself, so we won't need ssh.
#        dbConnDef = self.connectionGroup[self.job.env].sshDbPair.getDbConnDef()
#        timestampString = time.strftime( "%Y%m%d_%H%M%S")
#        dateString = time.strftime( "%Y%m%d")
#        pgDumpOutputPath = config_paths.getPgDumpOutputPath()
#
#        # First we see if an etl triggered db dump already exists.
#        if self.oncePerDay:
#            globString = os.path.join(pgDumpOutputPath, dbConnDef.db + '_'+ dateString + '*.backup')
#            import glob
#            filelist = glob.glob(globString)
#            if len(filelist) > 0:
#                message = ''
#                message += 'job %s \n' % self.job.name
#                message += 'is skipping the db backup for %s\n' % dbConnDef.forLogging()
#                message += 'because a backup already exists for the date of %s\n' % dateString
#                print message
#                return
#
#        pgDumpExe = config_paths.getPgDumpExePath()
#        outputFile = os.path.join(pgDumpOutputPath, dbConnDef.db + '_'+ timestampString + '.backup')
#
#
#        # pg_dump --host localhost --port 5432 --username postgres --format custom --blobs --verbose --file "/mnt/data/sams_fargeo_image_01.backup" sams_fargeo_image
#        commandLine = pgDumpExe
#        commandLine += ' --host %s ' % dbConnDef.host
#        commandLine += ' --username %s ' % dbConnDef.user
#        commandLine += ' --format custom '
#        commandLine += ' --blobs '
#        commandLine += ' --verbose '
#        commandLine += ' --file %s ' % outputFile
#        commandLine += ' %s ' % dbConnDef.db
#
#        print "command line:" + commandLine
#
#        # todo - check for disk space
#
#        try:
#            process = subprocess.Popen(commandLine, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
#            # "wait" for the process to finish.
#            # Using "process.wait()" may fill the PIPE buffer if there is too much stdout.
#            # To avoid this we use "process.communicate()".
#            process.communicate(input=None)
#
#            (returncode, stdout, stderr) = (process.returncode, process.stdout, process.stderr)
#            for line in stdout.readlines():
#                print line
#            for line in stderr.readlines():
#                print line
#
#            if returncode != 0:
#                print '***** db backup failed *****'
#                sys.exit(-1)
#            else:
#                print 'db backup succeeded'
#
#        except Exception, e:
#            print e
#            print 'exception during db backup'
#            raise


class AreDatabasesBackedUpXaCommand:

    # transcational (uses job level resource)
    # gateway command (can stop command processing)
    # uses sqlalchemy

    def __init__(self, job=None):
        self.type = 'Are Databases Backed Up Gateway Xa Command'
        self.job=job

    def execute(self):
        try:
            for key, resourceDict in self.job.commitResources.iteritems():
                dbConn = resourceDict['DB_CONNECTION']
                if not self.isDbBackedUp(dbConn):
                    raise StopJob('database %s is not backed up in %s' % (key, self.job.env))
        except Exception, e:
            raise

    def isDbBackedUp(self, dbConn):
        try:
            sql = """
                select 1
                from etl_db_backups
                where etl_db_backups.dt = '%s'
            """ % str(datetime.date.today())
            #print 'executing sql'
            #print sql
            results = dbConn.execute(sql)
            rows  = results.fetchall()
            return len(rows) >= 1
        except Exception, e:
            print e
            raise
        finally:
            results.close()




class ProcessParcelsXaCommand:

    def __init__(self, job=None, connectionKey=None):
        self.type = 'Process Parcels Xa Command'
        self.job = job
        self.connectionKey = connectionKey

    def execute(self):

        dbConn = self.job.commitResources[self.connectionKey]['DB_CONNECTION']
        sql="""
            select *
            from _sfmad_etl_parcels('%s');
        """ % self.job.id
        results = None
        try:
            results = dbConn.execute(sql)
            row = results.fetchall()[0]
            success = row[0]
            messageText = row[1]
            if not success:
                raise Exception, messageText
            else:
                print messageText

        except Exception, e:
            print e
            raise
        finally:
            if results:
                results.close()




class ProcessStreetsXaCommand:

    def __init__(self, job=None, connectionKey=None):
        self.type = 'Process Streets Xa Command'
        self.job = job
        self.connectionKey = connectionKey

    def execute(self):

        # calls db function
        # function signature:
        #    job_id_in char(36),
        #    out success boolean,
        #    out messageText text
        # For now we are not using all output parameters; they are here for simplicity, consistency.

        dbConn = self.job.commitResources[self.connectionKey]['DB_CONNECTION']

        sql="""
            select *
            from _sfmad_etl_streets('%s');
        """ % self.job.id

        results = None
        try:
            results = dbConn.execute(sql)
            row = results.fetchall()[0]
            success = row[0]
            messageText = row[1]
            self.job.message += '\n'
            self.job.message += '%s - %s\n' % (self.type, self.connectionKey)
            if not success:
                raise Exception, messageText
            else:
                print messageText
        except Exception, e:
            # todo - figure out how to handle this in the Job.
            print e
            raise
        finally:
            if results:
                try:
                    results.close()
                except Exception, e:
                    pass




class AbstractMapCacheCommand:

    def __init__(self, **kwargs):
        self.job = kwargs['job']
        self.connectionGroup = kwargs['connectionGroup']
        self.geoserverConnections = kwargs['geoserverConnections']
        self.layerName = kwargs['layerName']
        self.mimeType = kwargs['mimeType']
        # we cannot initialize these here because we only know once we start "execute"
        self.connectionPair = None
        self.geoserverParms = None
        self.zoomStart = kwargs['zoomStart']
        self.zoomStop = kwargs['zoomStop']
        self.pollingIntervalInSeconds = kwargs['pollingIntervalInSeconds']

    def isSeedingInProgress(self):
        import base64
        auth = base64.encodestring("%s:%s" % (self.geoserverParms['user'], self.geoserverParms['password']))
        headers = {"Authorization" : "Basic %s" % auth}
        connection = httplib.HTTPConnection(self.getGeoWebCacheUrl())
        connection.request("GET", self.getSeedUrl(), headers=headers)
        response = connection.getresponse()
        if response.status != 200:
            message = 'geoserver request failure - response status was ' + str(response.status) + ' - message was ' + str(response.msg)
            print message
            raise IOError(message)
        stringResponse = response.read()
        responseJson = json.loads(stringResponse)
        responseArray = responseJson['long-array-array']
        timestamp = datetime.datetime.now().strftime("%H:%M:%S")
        if not len(responseArray):
            print "no map cache processing as of %s" % timestamp
            return False
        else:
            print "map cache processing is active as of %s" % timestamp
            return True

    def getGeoWebCacheUrl(self):
        return '%s:%s' % (self.geoserverParms['host'], self.geoserverParms['port'])


    def getSeedUrl(self):
        return '%s/%s.xml' % (self.geoserverParms['seedPath'], self.layerName)


    def getSqlForSelect(self):
        return """
            select
                id,
                xmin(st_transform(geometry, 900913)) as llx,
                ymin(st_transform(geometry, 900913)) as lly,
                xmax(st_transform(geometry, 900913)) as urx,
                ymax(st_transform(geometry, 900913)) as ury
            from etl_bboxes
            where map_cache_refresh_tms is null;
        """


    def getSqlForUpdate(self, id, tms):
        return """
            update etl_bboxes
            set map_cache_refresh_tms = '%s'
            where id = %s
        """ % (tms, id)


    def getXml(self, bbox):
        # We have had trouble if the coordinates were not floats; i.e. the seed request would do nothing.
        # For seedType, we can use "truncate", "reseed" or "seed".
        #   seed: generate missing tiles
        #   reseed: regenerate tiles
        #   truncate: remove existing tiles (truncate cache)
        # Without the strip() the response status will be 500.
        print "\tlayerName: %s\n\tbbox: %s\n\tzoomStart: %d\n\tzoomStop: %d\n\tmimeType: %s\n\tseedType: %s" % (self.layerName, bbox, self.zoomStart, self.zoomStop, self.mimeType, self.seedType)

        bboxXmlFragment = ""
        if bbox:
            bboxXmlFragment = """
                <bounds>
                    <coords>
                        <double>%f</double>
                        <double>%f</double>
                        <double>%f</double>
                        <double>%f</double>
                    </coords>
                </bounds>
            """ % (bbox['xmin'], bbox['xmax'], bbox['ymin'], bbox['ymax'])

        return """
            <?xml version="1.0" encoding="UTF-8"?>
            <seedRequest>
                <name>%s</name>
                %s
                <srs>
                    <number>900913</number>
                </srs>
                <zoomStart>%d</zoomStart>
                <zoomStop>%d</zoomStop>
                <format>%s</format>
                <type>%s</type>
                <threadCount>1</threadCount>
            </seedRequest>
        """.strip() % (self.layerName, bboxXmlFragment, self.zoomStart, self.zoomStop, self.mimeType, self.seedType)



    def submitSeedRequest(self, bbox):
        print 'submitting request for %s' % self.seedType
        import httplib
        import base64
        import string
        # "Basic" authentication encodes userid:password in base64. Note
        # that base64.encodestring adds some extra newlines/carriage-returns
        # to the end of the result. string.strip is a simple way to remove
        # these characters.
        auth = 'Basic ' + string.strip(base64.encodestring(self.geoserverParms['user'] + ':' + self.geoserverParms['password']))
        seedUrl = self.getSeedUrl()
        xml = self.getXml(bbox)

        connection = httplib.HTTPConnection(self.getGeoWebCacheUrl())
        connection.putrequest("POST", seedUrl)
        connection.putheader("Host", self.geoserverParms['host'])
        connection.putheader("User-Agent", "Python post")
        connection.putheader("Content-type", "text/xml; charset=\"UTF-8\"")
        connection.putheader("Content-length", "%d" % len(xml))
        connection.putheader('Authorization', auth )
        connection.endheaders()
        connection.send(xml)
        # get response
        response = connection.getresponse()
        if response.status != 200:
            message = 'geoserver request failure - response status was ' + str(response.status) + ' - message was ' + str(response.msg)
            print message
            raise IOError(message)




    def pollUntilSeedingComplete(self):
        # Poll geoserver until seeding is complete.
        # Testing suggests this will be about 10 seconds per request.
        # As far as I know, we can only detect seeding work in general, not seeding due to our specific layer or request.
        # It may take a few seconds for seeding to initialize - so we wait a few seconds.
        time.sleep(10)
        while True:
            if not self.isSeedingInProgress():
                break
            time.sleep(self.pollingIntervalInSeconds)

    def execute(self, doExecute=None):
        self.geoserverConnection = self.geoserverConnections[self.job.env]
        sshClient = self.geoserverConnection['SSH']
        self.geoserverParms = self.geoserverConnection['GEOSERVER']
        try:
            sshClient.connect()
            if self.isSeedingInProgress():
                raise Exception("Sorry but seeding is already in progress:-(\nRunning multiple seeds is not supported yet.\nYou can kill the seed process using the geoserver admin console.")
            else:
                doExecute()

        except Exception, e:
            print e
            raise
        finally:
            sshClient.disconnect()

class MapCacheEtlReseedCommand(AbstractMapCacheCommand):

    def __init__(self, **kwargs):
        AbstractMapCacheCommand.__init__(self, **kwargs)
        self.type = 'Map Cache ETL Reseed'
        self.seedType = 'reseed'
        self.pollingIntervalInSeconds = 15 if self.pollingIntervalInSeconds == None else self.pollingIntervalInSeconds

    def execute(self):
        AbstractMapCacheCommand.execute(self, self.doExecute)

    def doExecute(self):
        print 'running etl reseed'
        self.connectionPair = self.connectionGroup[self.job.env].sshDbPair
        dbConnDef = self.connectionPair.getDbConnDef()
        # A reseed request for a parcel takes about 15 seconds
        pollingIntervalInSeconds = 15 if self.pollingIntervalInSeconds == None else self.pollingIntervalInSeconds
        try:
            dbConn = dbConnDef.getConnection()
            cursor = dbConn.cursor()
            sql = self.getSqlForSelect()
            cursor.execute(sql)
            # assume rowCount < 1000
            # assume rowCount < 1000
            rows = cursor.fetchall()
            for row in rows:
                # assign variables for clarity
                primaryKey = row[0]
                bbox = {'xmin': row[1], 'xmax': row[2], 'ymin': row[3], 'ymax': row[4]}
                self.submitSeedRequest(bbox)
                self.pollUntilSeedingComplete()
                cursor.execute(self.getSqlForUpdate(primaryKey, datetime.datetime.now()))
                dbConn.commit()
            cursor.close()
        except Exception, e:
            print e
            raise
        finally:
            dbConn.close()




class MapCacheCommand(AbstractMapCacheCommand):

    def __init__(self, **kwargs):
        AbstractMapCacheCommand.__init__(self, **kwargs)
        self.type = 'Map Cache Command'
        self.seedType = kwargs['cacheCommandType']
        self.bbox=None
        try:
            self.bbox=kwargs['bbox']
        except KeyError:
            pass
        self.pollingIntervalInSeconds = 30 if self.pollingIntervalInSeconds == None else self.pollingIntervalInSeconds

    def execute(self):
        AbstractMapCacheCommand.execute(self, self.doExecute)

    def doExecute(self):
        print 'running %s for %s' % (self.seedType, 'maximum extent' if not self.bbox else self.bbox)
        self.submitSeedRequest(self.bbox)
        self.pollUntilSeedingComplete()


class CompareEtlJobIdsCommand:

    def __init__(self, job=None, easDbConnectionGroup=(), fmeWebConnectionGroup=(), fmeJobDict=None):
        self.type = "CompareEtlJobIdsCommand"
        self.job = job
        self.fmeJobDict = fmeJobDict
        self.easDbConnectionGroup = easDbConnectionGroup
        self.fmeWebConnectionGroup = fmeWebConnectionGroup

    def execute(self):
        fmeServerHelper = FmeServerHelper(host=self.fmeWebConnectionGroup[self.job.env]['host'], user=self.fmeWebConnectionGroup[self.job.env]['user'], password=self.fmeWebConnectionGroup[self.job.env]['password'])
        lastCompletedFmeJob = fmeServerHelper.getLatestCompletedJob(self.fmeJobDict)
        lastCompletedFmeJobId = fmeServerHelper.getJobId(lastCompletedFmeJob)
        latestFmeJobIdFromEas = self.getLatestFmeJobIdFromEas()
        print 'lastCompletedFmeJobId: ', lastCompletedFmeJobId
        print 'latestFmeJobIdFromEas: ', latestFmeJobIdFromEas

        if latestFmeJobIdFromEas is None:
            pass # This is the path we take the first time we run after the initial deploy.
        elif lastCompletedFmeJobId < latestFmeJobIdFromEas:
            raise StopJob('The latest SFGIS ETL for %s has a job id of %s. This is less than the most recent one loaded into EAS which is %s. This should never happen so we are halting EAS staging.' % (self.job.name, lastCompletedFmeJobId, latestFmeJobIdFromEas))
        elif lastCompletedFmeJobId > latestFmeJobIdFromEas:
            pass # This is the nominal path.
        else:
            raise StopJob('The latest SFGIS ETL for %s (job id %s) has already been loaded into EAS - halting EAS staging of %s.' % (self.job.name, lastCompletedFmeJobId, self.job.name) )

    def getLatestFmeJobIdFromEas(self):
        fmeServerJobId = None
        dbConn = None
        sshClient = None
        try:
            sql = """
                select fme_server_job_id
                from etl_jobs
                where job_name = '%s'
                and committed_tms is not null
                order by committed_tms desc
                limit 1
            """ % self.job.name
            #print sql
            sshDbPair = self.easDbConnectionGroup[self.job.env].sshDbPair
            sshClient = sshDbPair.getSshClient()
            dbConnDef = sshDbPair.getDbConnDef()
            sshClient.connect()
            dbConn = dbConnDef.getConnection()
            cursor = dbConn.cursor()
            cursor.execute(sql)
            rows = cursor.fetchall()
            cursor.close()
            fmeServerJobId = rows[0][0]
        except Exception, e:
            print e
            raise
        finally:
            try:
                if dbConn: dbConn.close()
            except Exception, e:
                pass
            try:
                if sshClient: sshClient.disconnect()
            except Exception, e:
                pass
        return fmeServerJobId


class CheckForGreenLightToRunStagingCommand:

    def __init__(self, job=None, easDbConnectionGroup=(), fmeWebConnectionGroup=(), fmeJobDicts=None):
        self.type = "CheckForGreenLightToRunStagingCommand"
        self.job = job
        self.fmeJobDicts = fmeJobDicts
        self.easDbConnectionGroup = easDbConnectionGroup
        self.fmeWebConnectionGroup = fmeWebConnectionGroup

    def execute(self):
        for fmeJobDict in self.fmeJobDicts:
            print 'running checks for %s ' % fmeJobDict
            self.runChecks(fmeJobDict)

    def runChecks(self, fmeJobDict):
        fmeServerHelper = FmeServerHelper(host=self.fmeWebConnectionGroup[self.job.env]['host'], user=self.fmeWebConnectionGroup[self.job.env]['user'], password=self.fmeWebConnectionGroup[self.job.env]['password'])
        fmeJobExists = fmeServerHelper.doesJobExist(fmeJobDict)
        fmeJobIsRunning = fmeServerHelper.isJobRunning(fmeJobDict)
        lastCompletedFmeJob = fmeServerHelper.getLatestCompletedJob(fmeJobDict)
        lastCompletedFmeJobStatus = fmeServerHelper.getJobStatus(lastCompletedFmeJob)
        jobIsQueued = fmeServerHelper.isJobQueued(fmeJobDict)
        print 'fmeJobExists: ', fmeJobExists
        print 'fmeJobIsRunning: ', fmeJobIsRunning
        print 'jobIsQueued: ', jobIsQueued
        print 'lastCompletedFmeJobStatus: ', lastCompletedFmeJobStatus

        if not fmeJobExists:
            raise StopJob('SFGIS ETL fme workspace %s does not exist - halting EAS staging.' % fmeJobDict['workspace'])

        if fmeJobIsRunning:
            raise StopJob('SFGIS ETL for %s is currently running - halting EAS staging.' % self.job.name)

        if jobIsQueued:
            raise StopJob('SFGIS ETL for %s is queued - halting EAS staging' % self.job.name)

        if lastCompletedFmeJobStatus is None:
            raise StopJob('No records found for SFGIS %s ETL - halting EAS staging. Rerun the %s job on SFGIS.' % (self.job.name, self.job.name))

        if lastCompletedFmeJobStatus != 'SUCCESS':
            raise StopJob('Last SFGIS ETL for %s did not complete successfully - halting EAS staging.' % self.job.name)


class BeginTransactionXaCommand:

    def __init__(self, job=None):
        self.type = 'Begin Transaction Xa Command'
        self.job=job

    def execute(self):
        self.job.beginTransaction()


class EndTransactionXaCommand:

    def __init__(self, job=None):
        self.type = 'Commit Transaction Xa Command'
        self.job=job

    def execute(self):
        self.job.endTransaction()


class InitDbCommand:

    # For tables that have no RI (such as geoserver tables),
    # after data is staged, we can initialize or reinitialize the DB for a given job.
    # We do it this because:
    # 1) during the "commit" we want the fastest 

    def __init__(self, job=None, tableMappings=None):
        self.type = 'Init Db Command'
        self.job=job
        self.tableMappings=tableMappings

        
    def execute(self):
        try:
            for tableMapping in self.tableMappings.values():
                connectionGroup = tableMapping.connectionGroup
                connectionPair = connectionGroup[self.job.env].sshDbPair
                sshClient = connectionPair.getSshClient()
                dbConnDef = connectionPair.getDbConnDef()

                sshClient.connect()

                self.insertSpatialReferenceSystem(dbConnDef)

                for tableMappingItem in tableMapping.tableMappingItems:
                    if tableMappingItem.copyOnCommit:
                        self.checkSourceTableExist(dbConnDef, tableMappingItem.stagingTable)
                        self.dropTable(dbConnDef, tableMappingItem.targetTable)
                        self.createTable(dbConnDef, tableMappingItem.stagingTable, tableMappingItem.targetTable)
                        self.createGistIndex(dbConnDef, tableMappingItem.targetTable)
                        grantTablePermissions(dbConnDef, tableMappingItem.targetTable)
                        self.deleteGeometryColumnRow(dbConnDef, tableMappingItem.targetTable)
                        self.insertGeometryColumnRow(dbConnDef, tableMappingItem.stagingTable, tableMappingItem.targetTable)
        except Exception, e:
            print e
            raise
        finally:
            sshClient.disconnect()


    def checkSourceTableExist(self, dbConnDef, sourceTable):
        sql = """
            select 1
            from pg_tables
            where schemaname = 'public'
            and tablename = '%s';
        """ % sourceTable
        dbConn = dbConnDef.getConnection()
        cursor = dbConn.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()
        if len(rows) == 0:
            raise Exception, 'staging table "%s" was not found; you must run STAGE before INIT' % sourceTable


    def createGistIndex(self, dbConnDef, targetTable):
        indexName = targetTable + "_gist" 
        sql = """
            CREATE INDEX %s
            ON %s
            USING gist (geometry);
        """ % (indexName, targetTable)
        dbConnDef.executeSql(sql)

    def createTable(self, dbConnDef, sourceTableName, targetTableName):
        sql = """
            select *
            into %s
            from %s
            where 1=2;
        """ % (targetTableName, sourceTableName)
        dbConnDef.executeSql(sql)


    def dropTable(self, dbConnDef, tableName):
        sql = """
            drop table if exists %s;
        """ % tableName
        dbConnDef.executeSql(sql)


    def deleteGeometryColumnRow(self, dbConnDef, tableName):
        sql = getSqlForDeleteGeometryColumnRow(tableName)
        dbConnDef.executeSql(sql)


    def insertGeometryColumnRow(self, dbConnDef, sourceTable, targetTable):
        try:
            sql  = """
                insert into geometry_columns(f_table_catalog, f_table_schema, f_table_name, f_geometry_column, coord_dimension, srid, "type")
                select f_table_catalog, f_table_schema, '%s', f_geometry_column, coord_dimension, srid, "type"
                from geometry_columns where f_table_schema = '%s' and f_table_name = '%s';
            """ % (targetTable, 'public', sourceTable)
            dbConnDef.executeSql(sql)
        except Exception, e:
            raise

    def insertSpatialReferenceSystem(self, dbConnDef):
        sql = """
            INSERT into spatial_ref_sys (srid, auth_name, auth_srid, proj4text, srtext)
            select
                900913,
                'sr-org',
                900913,
                '+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs',
                'PROJCS["unnamed",GEOGCS["unnamed ellipse",DATUM["unknown",SPHEROID["unnamed",6378137,0]],PRIMEM["Greenwich",0],UNIT["degree",0.0174532925199433]],PROJECTION["Mercator_2SP"],PARAMETER["standard_parallel_1",0],PARAMETER["central_meridian",0],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["Meter",1],EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"]]'
            where not exists (
                select 1
                from spatial_ref_sys
                where srid = 900913
            );
        """
        dbConnDef.executeSql(sql)


class RemoveDirectoryTreeCommand:
    def __init__(self, job=None, targetDir=None):
        self.type = 'Remove Directory Tree Command'
        self.job = job
        self.targetDir = targetDir
    def execute(self):
        print str(datetime.datetime.now())
        import shutil
        try:
            if os.path.exists(self.targetDir):
                print 'removing directory %s' % self.targetDir
                shutil.rmtree(self.targetDir)
        except Exception, e:
            print e
            raise

class DumpTablesCommand:

    # deprecated - use SqlToCsvCommand instead

    def __init__(self, job=None, connectionGroup=None, tableNames=[], targetDir=getDataFilePath(), separator=','):
        self.type = 'Dump Tables Command'
        self.job = job
        self.connectionGroup = connectionGroup
        self.tableNames = tableNames
        self.targetDir = targetDir
        self.separator = separator

    def execute(self):
        print str(datetime.datetime.now())

        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()

        try:
            if not os.path.exists(self.targetDir):
                os.makedirs(self.targetDir)
            sshClient.connect()
            connection = dbConnDef.getConnection()
            cursor = connection.cursor()
            for tableName in self.tableNames:
                fileName = os.path.join(self.targetDir, tableName + '.csv')
                print 'dumping %s to %s' % (tableName, fileName)
                try:
                    outFile = open(fileName, 'w')
                    # Syntax of the COPY is arcane and can be fussy.
                    # Use of "select * from ..." allows us to pass in views.
                    # Computed columns must have datatype defined otherwise the column will not show.
                    copySyntax = "COPY (select * from %s) TO STDOUT WITH CSV HEADER" % tableName
                    cursor.copy_expert(copySyntax, outFile)

                except Exception, e:
                    raise
                finally:
                    outFile.close()

            cursor.close()

        except Exception, e:
            print e
            raise
        finally:
            connection.close()
            sshClient.disconnect()


class SqlToCsvCommand:

    # example usage
    # SqlToCsvCommand(job=job, connectionGroup=self.connectionGroups['MAD_ETL'], sql="select * from etl_exceptions where job_uuid = '%s'" % job.id, outFile=os.path.join(reportDir, 'exceptions.csv')),

    def __init__(self, job=None, connectionGroup=None, sql=None, outFile=None, separator=','):
        self.type = 'SqlToCsvCommand'
        self.job = job
        self.connectionGroup = connectionGroup
        self.sql = sql.rstrip().rstrip(';')
        self.outFile = outFile
        self.separator = separator

    def execute(self):
        print str(datetime.datetime.now())

        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()

        connection = None
        try:
            reportDirName = os.path.dirname(self.outFile)
            if not os.path.exists(reportDirName):
                os.makedirs(reportDirName)
            sshClient.connect()
            connection = dbConnDef.getConnection()
            cursor = connection.cursor()
            print 'creating file %s' % self.outFile
            print 'from sql %s' % self.sql
            outFile = None
            try:
                outFile = open(self.outFile, 'w')
                # Syntax of the COPY is arcane and can be fussy.
                # Use of "select * from ..." allows us to pass in views.
                # Computed columns must have datatype defined otherwise the column will not show.
                copySyntax = "COPY (%s) TO STDOUT WITH CSV HEADER" % self.sql
                cursor.copy_expert(copySyntax, outFile)

            except Exception, e:
                raise
            finally:
                if outFile:
                    outFile.close()

            cursor.close()

        except Exception, e:
            print e
            raise
        finally:
            if connection:
                connection.close()
            if sshClient:
                sshClient.disconnect()


class EmailCommand:
    def __init__(self, job=None, attachFiles=(), subject=None, text=None, html=None, htmlList=(), attachFilePath=getDataFilePath(), fromKey=None, toKey=None):
        self.type = 'Email Command'
        self.job = job
        self.attachFilePath = attachFilePath
        self.attachFiles = attachFiles
        self.subject = subject
        self.text = text
        self.html = html
        self.fromKey = fromKey
        self.toKey = toKey
        self.htmlList = htmlList

    def execute(self):
        print str(datetime.datetime.now())
        from config.live import config_notifications
        notificationsOrganizer = config_notifications.NotificationsOrganizer()
        recipientList = notificationsOrganizer.getEmailList(self.toKey, self.job.env)
        sender = notificationsOrganizer.getEmailList(self.fromKey, self.job.env)
        try:
            attachFiles = []
            for attachFile in self.attachFiles:
                attachFiles.append(os.path.join(self.attachFilePath, attachFile))
            import utils
            html = ""
            if self.html:
                html = self.html
            if self.htmlList:
                html = ''.join(self.htmlList)
            utils.sendEmail(env=self.job.env, subject=self.subject, text=self.text, html=html, attachFiles=attachFiles, sender=sender, recipientList=recipientList)
        except:
            raise

class FileToListCommand:

    def __init__(self, job=None, inFileName=None, outList=[]):
        self.type = 'FileToListCommand'
        self.job = job
        self.inFileName = inFileName
        self.outList = outList

    def execute(self):
        inFile = open(self.inFileName, 'rb')
        for row in inFile:
            self.outList.append(row)
        try: inFile.close()
        except Exception, e: pass


class CsvToHtmlCommand:

    def __init__(self, job=None, csvFileName=None, htmlFileName=None):
        self.type = 'CsvToHtmlCommand'
        self.job = job
        self.csvFileName = csvFileName
        self.htmlFileName = htmlFileName

    def execute(self):
        import csv
        print str(datetime.datetime.now())
        csvFile = open(self.csvFileName, 'rb')
        htmlFile = open(self.htmlFileName, 'wb')
        try:
            csvReader = csv.reader(csvFile)
            rownum = 0
            htmlFile.write('<table>')
            for csvRow in csvReader:
                htmlFile.write('<tr>')
                if rownum == 0:
                    for columnHeader in csvRow:
                        htmlFile.write('<th>' + columnHeader + '</th>')
                else:
                    for column in csvRow:
                        htmlFile.write('<td>' + column + '</td>')
                htmlFile.write('</tr>'+os.linesep)
                rownum += 1
            htmlFile.write('</table>')
        finally:
            try: csvFile.close()
            except Exception, e: pass
            try: htmlFile.close()
            except Exception, e: pass


class CsvToSqlCommand:

    def __init__(self, job=None, csvFileName=None, sqlFileName=None, tableName=None):

        self.type = 'CsvToSqlCommand'
        self.job = job
        self.csvFileName = csvFileName
        self.sqlFileName = sqlFileName
        self.tableName = tableName

    def execute(self):
        import csv
        print str(datetime.datetime.now())
        csvFile = open(self.csvFileName, 'rb')
        sqlFile = open(self.sqlFileName, 'wb')
        try:
            csvReader = csv.reader(csvFile)
            header = ""
            rownum = 0
            for csvRow in csvReader:
                if rownum == 0:
                    header = ",".join(csvRow)
                else:
                    data = "'" + "','".join(csvRow) + "'"
                    sql = "INSERT INTO %s (%s) VALUES (%s);\n" % (self.tableName, header, data)
                    sqlFile.write(sql)
                rownum += 1

        finally:
            try: csvFile.close()
            except Exception, e: pass
            try: sqlFile.close()
            except Exception, e: pass


class AbstractDbAdminCommand:

    def __init__(self, job=None, connectionGroup=None, role=None, schema=None):
        self.job = job
        self.connectionGroup = connectionGroup
        self.role = role
        self.schema = schema

    def doExecute(self, cursor):
        raise Exception('subclass must implement this method')

    def buildGrantStatementsForSequences(self, cursor, sequences, privileges):
        grantStatements = []
        for sequence in sequences:
            grantSql = "GRANT ALL PRIVILEGES ON SEQUENCE %s TO %s;" % (sequence, self.role)
            grantStatements.append(grantSql)
        return grantStatements

    def buildGrantStatementsForTables(self, cursor, tables, privileges):
        grantStatements = []
        for table in tables:
            for privilege in privileges:
                grantSql = 'GRANT %s ON TABLE %s TO %s;' % (privilege, table, self.role)
                grantStatements.append(grantSql)
        return grantStatements

    def buildList(self, cursor, schema, sql, concatenateSchema=True):
        cursor.execute(sql % schema)
        rows = cursor.fetchall()
        resultList = []
        for row in rows:
            if concatenateSchema:
                resultList.append('%s.%s' % (schema, row[0]))
            else:
                resultList.append('%s' % row[0])
        return resultList

    def getAllTables(self, cursor, schema):
        return self.buildList(cursor, schema, "select table_name from information_schema.tables where table_schema = '%s';")

    def getAllSequences(self, cursor, schema):
        return self.buildList(cursor, schema, "select sequence_name from information_schema.sequences where sequence_schema = '%s';")

    def buildFunctionSignature(self, cursor, schema, specificFunctionName):
        # todo - replace this function with pg_get_functiondef() upon pg upgrade to 8.X?
        sql = "select routine_name from information_schema.routines where routine_schema = '%s' and specific_name = '%s';" % (schema, specificFunctionName)
        cursor.execute(sql)
        rows = cursor.fetchall()
        functionName = rows[0][0]
        sql = "select udt_name, parameter_mode from information_schema.parameters where specific_schema = '%s' and specific_name = '%s' order by ordinal_position asc" % (schema, specificFunctionName)
        cursor.execute(sql)
        rows = cursor.fetchall()
        parameterList = []
        for row in rows:
            parameterType = row[0]
            parameterMode = row[1]
            if parameterMode == 'IN':
                parameterList.append(parameterType)
        return '%s(%s)' % (functionName, ', '.join(parameterList))


    def buildAlterOwnerStatementsForFunctions(self, cursor):
        sql = "select specific_name, routine_definition from information_schema.routines where routine_schema = '%s';" % self.schema
        cursor.execute(sql)
        rows = cursor.fetchall()
        alterStatements = []
        for row in rows:
            specificName = row[0]
            functionType = 'AGGREGATE' if row[1] == 'aggregate_dummy' else 'FUNCTION'
            functionSignature = self.buildFunctionSignature(cursor, self.schema, specificName )
            alterStatements.append('ALTER %s %s.%s OWNER TO %s;' % (functionType, self.schema, functionSignature, self.role))
        return alterStatements


    def buildAlterOwnerStatementsForTables(self, cursor, tables):
        alterStatements = []
        for table in tables:
            alterStatements.append('ALTER TABLE %s OWNER TO %s;' % (table, self.role))
        return alterStatements


    def execute(self):
        connectionPair = self.connectionGroup[self.job.env].sshDbPair
        sshClient = connectionPair.getSshClient()
        dbConnDef = connectionPair.getDbConnDef()
        connection = None
        cursor = None
        try:
            sshClient.connect()
            connection = dbConnDef.getConnection()
            cursor = connection.cursor()
            self.doExecute(cursor)
            connection.commit()
        except Exception, e:
            if connection:
                connection.rollback()
            raise
        finally:
            if cursor:
                cursor.close()
            if connection:
                connection.close()
            if sshClient:
                sshClient.disconnect()


class GrantPrivilegesOnTablesCommand(AbstractDbAdminCommand):

    def __init__(self, job=None, connectionGroup=None, role=None, schema=None, privileges=(), tables=[]):
        AbstractDbAdminCommand.__init__(self, job=job, connectionGroup=connectionGroup, role=role, schema=schema)
        self.privileges = privileges
        self.tables = tables

    def execute(self):
        AbstractDbAdminCommand.execute(self)

    def doExecute(self, cursor):
        if not self.tables:
            self.tables = self.getAllTables(cursor, self.schema)
        grantStatements = self.buildGrantStatementsForTables(cursor, self.tables, self.privileges)
        for grantStatement in grantStatements:
            print grantStatement
            cursor.execute(grantStatement)



class GrantPrivilegesOnSequencesCommand(AbstractDbAdminCommand):

    def __init__(self, job=None, connectionGroup=None, role=None, schema=None, privileges=(), sequences=[]):
        AbstractDbAdminCommand.__init__(self, job=job, connectionGroup=connectionGroup, role=role, schema=schema)
        self.privileges = privileges
        self.sequences = sequences

    def execute(self):
        AbstractDbAdminCommand.execute(self)

    def doExecute(self, cursor):
        if not self.sequences:
            self.sequences = self.getAllSequences(cursor, self.schema)
        grantStatements = self.buildGrantStatementsForSequences(cursor, self.sequences, self.privileges)
        for grantStatement in grantStatements:
            print grantStatement
            cursor.execute(grantStatement)



class SetOwnerOnFunctionsCommand(AbstractDbAdminCommand):

    def __init__(self, job=None, connectionGroup=None, role=None, schema=None):
        AbstractDbAdminCommand.__init__(self, job=job, connectionGroup=connectionGroup, role=role, schema=schema)

    def execute(self):
        AbstractDbAdminCommand.execute(self)

    def doExecute(self, cursor):
        alterStatements = self.buildAlterOwnerStatementsForFunctions(cursor)
        for alterStatement in alterStatements:
            print alterStatement
            cursor.execute(alterStatement)



class SetOwnerOnTablesCommand(AbstractDbAdminCommand):

    def __init__(self, job=None, connectionGroup=None, role=None, schema=None):
        AbstractDbAdminCommand.__init__(self, job=job, connectionGroup=connectionGroup, role=role, schema=schema)

    def execute(self):
        AbstractDbAdminCommand.execute(self)

    def doExecute(self, cursor):
        # To own a table you must own the sequence as well.
        # A view is equivilent to a table.
        objectList = []
        objectList.extend(self.getAllTables(cursor, self.schema))
        objectList.extend(self.getAllSequences(cursor, self.schema))
        alterStatements = self.buildAlterOwnerStatementsForTables(cursor, objectList)
        for alterStatement in alterStatements:
            print alterStatement
            cursor.execute(alterStatement)



class SetOwnerOnDbCommand(AbstractDbAdminCommand):

    def __init__(self, job=None, connectionGroup=None, role=None, schema=None):
        AbstractDbAdminCommand.__init__(self, job=job, connectionGroup=connectionGroup, role=role, schema=schema)

    def execute(self):
        AbstractDbAdminCommand.execute(self)

    def doExecute(self, cursor):
        cursor.execute('select current_database();')
        rows = cursor.fetchall()
        dbName = rows[0][0]
        sql = 'ALTER DATABASE %s OWNER TO %s;' % (dbName, self.role)
        print sql
        cursor.execute(sql)


##### BEGIN - shared functions and classes

def getSqlForDeleteGeometryColumnRow(tableName):
        return """
            delete
            from geometry_columns
            where f_table_schema = 'public'
            and f_table_name  = '%s'
        """ % tableName


def grantTablePermissions(dbConnDef, tableName):
    print 'granting table permissions'
    try:
        connection = dbConnDef.getConnection()
        connection.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_READ_COMMITTED)
        cursor = connection.cursor()
        sql = "GRANT SELECT ON %s TO PUBLIC;" % tableName
        #print 'executing sql'
        #print sql
        cursor.execute(sql)
        connection.commit()
    except Exception, e:
        print 'exception while trying to grant table permsssions'
        print e
        raise
    finally:
        connection.close()

##### END - shared functions and classes
