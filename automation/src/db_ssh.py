
# Classes that help support db connectivity with ssh

class ConnectionGroup:
    def __init__(self, name=None, env=None, sshDbPair=None):
        self.name = name
        self.env = env
        self.sshDbPair = sshDbPair

    def __str__(self):
       return  self.name +"\n"+ str(self.sshDbPair)


class SshDbConnectionPair:
    def __init__(self, dbConnDef=None, sshConnDef=None):
        self.dbConnDef = dbConnDef
        self.sshConnDef = sshConnDef

    def __str__(self):
        return  ('dbConnDef: %s \nsshConnDef: %s' ) % ( str(self.dbConnDef), str(self.sshConnDef) )

    def getSshClient(self):
        return self.sshConnDef

    def getDbConnDef(self):
        return self.dbConnDef
