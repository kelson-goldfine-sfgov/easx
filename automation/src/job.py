
from config.live.config_jobs import *
from config.live.config_jobs import *
from config.live import config_paths
import db
import os
import sys
import subprocess
import time
from optparse import OptionParser
import sqlalchemy
import uuid


class Job:

    
    # command pattern


    def __init__(self, name=None):
        self.id = uuid.uuid4()
        self.name = name
        self.commitConnGroups = []
        self.commitResources = {}
        self.verbose = False
        self.env = None
        self.message = ''


    def init(self, verbose=False):
        self.executeCommands(self.initCommands)


    def stage(self, verbose=False):
        self.executeCommands(self.stagingCommands)


    # supports 2 phase commit
    def commit(self, verbose=False):
        try:
            self.initTransactionalConnections()
            for index, command in enumerate(self.commitCommands):
                self.logDescription(index, command)
                command.execute()
        except StopJob, e:
            print e.message
        except Exception, e:
            print e
            self.rollbackTransaction()
            raise
        finally:
            self.closeTransactionalConnections()


    def execute(self, verbose=False):
        self.executeCommands(self.commands)


    # use for staging; auto commit is true
    def executeCommands(self, commands):
        if not commands:
            return
        try:
            for i, command in enumerate(commands):
                self.logDescription(i, command)
                command.execute()
        except StopJob, e:
            print e.message
        except Exception, e:
            print e
            raise
        finally:
            pass

    def rollbackTransaction(self):
        # todo - do not rollback a transaction that does not exist
        print 'rolling back'
        for resourceDict in self.commitResources.values():
            transaction = resourceDict['TRANSACTION']
            transaction.rollback()


    def endTransaction(self):

        for resourceDict in self.commitResources.values():
            transaction = resourceDict['TRANSACTION']
            transaction.prepare()

        for resourceDict in self.commitResources.values():
            transaction = resourceDict['TRANSACTION']
            transaction.commit()


    def beginTransaction(self):
        for key, resourceDict in self.commitResources.iteritems():
            dbConn = resourceDict['DB_CONNECTION']
            transaction = dbConn.begin_twophase()
            resourceDict['TRANSACTION'] = transaction


    def initTransactionalConnections(self):

        self.commitResources = {}
        
        for connectionGroup in self.commitConnGroups:
            key = connectionGroup[self.env].name
            sshDbPair = connectionGroup[self.env].sshDbPair
            sshClient = sshDbPair.getSshClient()
            dbConnDef = sshDbPair.getDbConnDef()
            # we use sqlalchemy because it supports two-phase commit
            connectionString = dbConnDef.toSqlAlchemy()

            sshClient.connect()
            engine = sqlalchemy.create_engine(connectionString)
            conn = engine.connect()

            self.commitResources[key] = {}
            self.commitResources[key]['DB_CONNECTION'] = conn
            self.commitResources[key]['SSH_CLIENT'] = sshClient
            # we will create the transaction when we begin it
            self.commitResources[key]['TRANSACTION'] = None 


    def closeTransactionalConnections(self):
        try:
            for resourceDict in self.commitResources.values():
                resourceDict['DB_CONNECTION'].close()
        except:
            pass

        try:
            for resourceDict in self.commitResources.values():
                resourceDict['SSH_CLIENT'].disconnect()
        except:
            pass


    def logDescription(self, i, command):
        print '\ncommand number: ' + str(i+1)
        # todo - move away from the use of the type field because it increases the maintenance burden
        try:
            print 'command: %s' % command.type
        except AttributeError:
            print 'command: %s' % command.__class__.__name__

    def setCommitConnGroups(self, connectionGroups=None, connGroupKeys=None):
        self.commitConnGroups = []
        for key in connGroupKeys:
            self.commitConnGroups.append(connectionGroups[key])


class StopJob(Exception):
    def __init__(self, message):
        self.message = message
    def __str__(self):
        return repr(self.message)

                
class Jobs:

    def addJob(self, job):
        self.jobDict[job.name] = job

    def getJob(self, jobName):
        return self.jobDict[jobName]

    def getJobList(self):
        jobList = []
        for key, value in self.jobDict.iteritems():
            jobList.append(value)
        return jobList

    def __init__(self):
        self.jobDict = {}


class TableMapping:

    def __init__(self, connectionGroup=None, tableMappingItems=None):
        self.connectionGroup = connectionGroup
        self.tableMappingItems = tableMappingItems

    def getStagingTableNames(self):
        stagingTableNames = []
        for tableMappingItem in self.tableMappingItems:
            stagingTableNames.append(tableMappingItem.stagingTable)
        return stagingTableNames

    def getTargetTableNames(self):
        targetTableNames = []
        for tableMappingItem in self.tableMappingItems:
            targetTableNames.append(tableMappingItem.targetTable)
        return targetTableNames

    def getTableTuples(self):
        tableTupleList = []
        for tableMappingItem in self.tableMappingItems:
            tableTupleList.append((tableMappingItem.stagingTable, tableMappingItem.targetTable, ))
        return tableTupleList



class TableMappingItem:

    def __init__(self, stagingTable=None, targetTable=None, copyOnCommit=False):
        self.stagingTable = stagingTable
        self.targetTable = targetTable
        self.copyOnCommit = copyOnCommit


def main():

    # todo - make smarter - pass in a job organizer instance

    jobOrganizer = JobOrganizer()
    jobKeys = sorted(jobOrganizer.jobs.jobDict.keys())
    environments = jobOrganizer.connectionOrganizer.environments


    usage = "\npython %prog --env environment --job jobname --action action --v verbose\n"
    usage += '\nenvironments:'
    for env in environments:
        usage += '\n\t%s' % env
    usage += '\njobs:\n'

    leftJustLength = len(max(jobKeys, key=len)) + 1
    for jobKey in jobKeys:
        usage += '\t' + jobKey.ljust(leftJustLength, ' ') + 'actions: '
        job = jobOrganizer.jobs.jobDict[jobKey]

        actions = []

        try:
            x = job.initCommands
            actions.append('INIT')
        except:
            pass

        try:
            x = job.stagingCommands
            actions.append('STAGE')
        except:
            pass

        try:
            x = job.commitCommands
            actions.append('COMMIT')
        except:
            pass

        try:
            x = job.commands
            actions.append('EXECUTE')
        except:
            pass

        usage += ' | '.join(actions)
        usage += '\n'

    usage += """
Description


An INIT may be run exactly one time.
EXECUTE, STAGE, and COMMIT actions may be run many times.

DO NOT run an INIT a second time without a DB refresh.
If a job has an INIT, you must run INIT before using any other job action.

However, the first time you run a STAGE-COMMIT job, you must run through this sequence:
    STAGE
    INIT
    COMMIT
INIT will initialize the table structure in the RDBMS.
When you change a staging table definition in an FME workspace,
you must run through the same sequence to reinitialize the table structure.
Thereafter, you simply run:
    STAGE
    COMMIT

See the source code README for details.

"""


    parser = OptionParser(usage)
    parser.add_option("--job", dest="job", help="specify job")
    parser.add_option("--env", dest="env", help="specify environment")
    parser.add_option("--action", dest="action", help="specify action")
    parser.add_option("--verbose", action="store_true", dest="verbose", default=False, help="print lots of output to stdout")

    (options, args) = parser.parse_args()
    if options.job == None:
        parser.error("Please specify a job.")
    if options.job not in jobKeys:
        parser.error("No such job: " + options.job)
    if options.env == None:
        parser.error("Please specify an env.")
    if options.action == None:
        parser.error("Please specify an action.")
    if options.env not in environments:
        parser.error("No such env: " + options.env)
    if options.action not in ('INIT', 'STAGE', 'COMMIT', 'EXECUTE'):
        parser.error("No such action: " + options.action)

    job = jobOrganizer.getJob(options.job, options.env, options.verbose)

    print "running job: %s" % job.name
    print "env: %s" % options.env
    print "action: %s" % options.action
    print "verbose: %s" % str(job.verbose)

    if (options.action == 'INIT'):
        job.init()
    elif (options.action == 'STAGE'):
        job.stage()
    elif (options.action == 'COMMIT'):
        job.commit()
    elif (options.action == 'EXECUTE'):
        job.execute()
    else:
        parser.error("No such action: " + options.action)

if __name__ == "__main__":
    main()

